
import main.tp1.BinarySearch;
import struct.impl.BinarySearchTree;
import struct.impl.BinaryTree;

public class Varela {
    BinarySearchTree<String> resultadoA;
    BinarySearchTree<String> resultadoB;

    public Varela(){
        resultadoA = new BinarySearchTree<>();
        resultadoB = new BinarySearchTree<>();
    }

    /**
     * Recibe un arbol con elementos de la sucursal A y la sucursal B
     * Agarra los elementos pertenecientes a la sucursal A
     * Y los inserta en un arbol binario de busqueda en el cual estan ordenados y separados de los
     * datos de la sucursal B
     * @param tree
     * El arbol que contiene los elementos de las dos sucursales que queremos separar
     * @return
     * El arbol binario de busqueda con los elementos de la sucursal A
     */

    public BinarySearchTree<String> getTreeSucursalA(BinaryTree<String> tree){

        if(tree.getRoot().contains("A")){
            resultadoA.insert(tree.getRoot());
        }
        if(!tree.getLeft().isEmpty()){
            getTreeSucursalA(tree.getLeft());
        }
        if(!tree.getRight().isEmpty()){
            getTreeSucursalA(tree.getRight());
        }
        return resultadoA;
    }

    /**
     * Recibe un arbol con elementos de la sucursal A y la sucursal B
     * Agarra los elementos pertenecientes a la sucursal B
     * Y los inserta en un arbol binario de busqueda en el cual estan ordenados y separados de los
     * datos de la sucursal A
     * @param tree
     * El arbol que contiene los elementos de las dos sucursales que queremos separar
     * @return
     * El arbol binario de busqueda con los elementos de la sucursal B
     */

    public BinarySearchTree<String> getTreeSucursalB(BinaryTree<String> tree){
        if(tree.getRoot().contains("B")){
            resultadoB.insert(tree.getRoot());
        }
        if(!tree.getLeft().isEmpty()){
            getTreeSucursalB(tree.getLeft());
        }
        if(!tree.getRight().isEmpty()){
            getTreeSucursalB(tree.getRight());
        }
        return resultadoB;
    }




    public void inOrder(BinarySearchTree<String> tree){
        if (tree.isEmpty()) return;
        inOrder(tree.getLeft());
        System.out.print(tree.getRoot() + "  ");
        inOrder(tree.getRight());
    }


    /**
     * Realiza una prueba del programa creando un arbol binario con datos de ambas sucursales
     * y se consiguen dos arboles binarios de busqueda cada uno correspondiente a una sucursal
     * @param args
     */
    public static void main(String[] args) {

        Varela ejercicio4 = new Varela();
        BinaryTree<String> tree1 = new BinaryTree<>("A1");
        BinaryTree<String> tree2 = new BinaryTree<>("B7");
        BinaryTree<String> tree3 = new BinaryTree<>("A8");
        BinaryTree<String> tree4 = new BinaryTree<>("A20", tree1, tree2);
        BinaryTree<String> tree5 = new BinaryTree<>("B3", tree3, new BinaryTree<>("B2"));
        BinaryTree<String> completed = new BinaryTree<>("A13", tree4, tree5);

        BinarySearchTree<String> arbolAOrdenado = ejercicio4.getTreeSucursalA(completed);
        BinarySearchTree<String> arbolBOrdenado = ejercicio4.getTreeSucursalB(completed);

        ejercicio4.inOrder(arbolAOrdenado);
        System.out.println("");
        ejercicio4.inOrder(arbolBOrdenado);






    }
}
