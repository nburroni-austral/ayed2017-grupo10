package struct.impl;

public class BinarySearchTree<T extends Comparable<T>> {
	// Implementacion de un arbol binario de busqueda no balanceado
	// Autor Alicia Gioia
	// Modificada para usar generics y excepciones y traducida al ingles por Tomas Varela e Ignacio Gilardoni
	private DoubleNode<T> root;

	public BinarySearchTree() {
		root = null;
	}

	public boolean isEmpty() {
		return (root == null);
	}

	public T getRoot() {
		if (isEmpty()) throw new NullPointerException("The tree is empty");
		return root.getElement();
	}

	public BinarySearchTree<T> getLeft() throws NullPointerException {
		BinarySearchTree<T> t = new BinarySearchTree<>();
		t.root = root.left;
		return t;
	}

	public BinarySearchTree<T> getRight() throws NullPointerException {
		BinarySearchTree<T> t = new BinarySearchTree<>();
		t.root = root.right;
		return t;
	}

	public boolean exists(T x) {
		return exists(root, x);
	}

	public T getMin() {
		return getMin(root).getElement();
	}

	public T getMax() {
		return getMax(root).getElement();
	}

	public T search(T x)  throws NullPointerException {
		return search(root, x).getElement();
	}

	public void insert(T x) {
		root = insert(root, x);
	}

	public void delete(T x)  throws NullPointerException {
		root = delete(root, x);
	}

	private boolean exists(DoubleNode<T> t, T x) {
		if (t == null)
			return false;
		if (x.compareTo(t.getElement()) == 0)
			return true;
		else if (x.compareTo(t.getElement()) < 0)
			return exists(t.getLeft(), x);
		else
			return exists(t.getRight(), x);
	}

	private DoubleNode<T> getMin(DoubleNode<T> t) {
		try {
			t.getLeft();
		} catch (Exception e) {
			return t;
		}
			return getMin(t.getLeft());
	}

	private DoubleNode<T> getMax(DoubleNode<T> t) {
		try {
			t.getRight();
		} catch (NullPointerException e) {
			return t;
		}
		return getMax(t.getRight());
	}

	private DoubleNode<T> search(DoubleNode<T> t, T x) {
		try {
		if (x.compareTo(t.getElement()) == 0)
			return t;
		else if (x.compareTo(t.getElement()) < 0)
			return search(t.getLeft(), x);
		else
			return search(t.getRight(), x);
	}catch(NullPointerException e){
		throw new NullPointerException("item not found");		
	}
		}

	private DoubleNode<T> insert(DoubleNode<T> t, T x) {
		if (t == null) {
			t = new DoubleNode<T>(x);
		}
		else if (x.compareTo(t.getElement()) < 0)
			t.left = insert(t.left, x);
		else if (x.compareTo(t.getElement()) > 0)
			t.right = insert(t.right, x);
		else throw new IllegalArgumentException(x.getClass().getSimpleName()+" already added");
		return t;
	}

	private DoubleNode<T> delete(DoubleNode<T> t, T x) {
		if (x.compareTo(t.getElement()) < 0)
			t.left = delete(t.getLeft(), x);
		else if (x.compareTo(t.element) > 0)
			t.right = delete(t.getRight(), x);
		else if (t.left != null && t.right != null) {
			t.element = getMin(t.getRight()).getElement();
			t.right = deleteMin(t.getRight());
		} else if (t.left != null)
			t = t.getLeft();
		else if (t.right != null)
			t = t.getRight();
		else t=null;
			
		return t;
	}

	private DoubleNode<T> deleteMin(DoubleNode<T> t) {
		if (t.left != null)
			t.left = deleteMin(t.getLeft());
		else
			t = t.getRight();
		return t;
	}

	private class DoubleNode<T> {
		private T element;
		private DoubleNode<T> right;
		private DoubleNode<T> left;

		public DoubleNode() {
			element = null;
		}
		public DoubleNode(T element) {
			this.element = element;
		}
		public T getElement() {
			if (element==null) throw new NullPointerException("Element is empty");
			return element;
		}
		public DoubleNode<T> getLeft() {
			if (left==null) throw new NullPointerException("Left child is empty");
			return left;
		}
		public DoubleNode<T> getRight() {
			if (right==null) throw new NullPointerException("Right child is empty");
			return right;
		}
	}
}