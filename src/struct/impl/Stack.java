package struct.impl;

public class Stack<T> implements struct.istruct.Stack<T>{
private Node<T> head;
private int size;
public Stack() {
  size = 0;
  head = new Node<>(null);
}
	@Override
	public void push(T t) {
		Node<T> newNode = new Node<>(t, head);
		head = newNode;
		size++;
	}

	@Override
	public void pop() {
		if(!isEmpty()) {
		head = head.previousElement;
	size--;
		}
	}

	@Override
	public T peek() {
		return head.element;
	}

	@Override
	public boolean isEmpty() {
		if (size != 0)	return false;
		else return true;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void empty() {
		size = 0;		
	}
private class Node<T>{
	private T element;
	private Node<T> previousElement;
	public Node(T o) {
		previousElement=null;
		element = o;
	}
	public Node(T o, Node<T> previousNode) {
		previousElement=previousNode;
		element = o;
	}
}
}
