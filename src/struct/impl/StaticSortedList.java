package struct.impl;

import struct.istruct.list.GeneralList;
import struct.istruct.list.SortedList;

public class StaticSortedList<L extends Comparable<L>> implements SortedList<L>{
    private static final int DEFAULT_CAPACITY = 10;
    private Object[] data;
    private int window;
    private int size;
    private final int capacity;
    private boolean isSorted;
    public StaticSortedList() {
    	this(DEFAULT_CAPACITY);
	}
    public StaticSortedList(int capacity) {
        this.data = new Object[capacity];
        this.capacity = capacity;
        this.window = 0;
        this.size = 0;
    }
    private StaticSortedList(int window, int size, int capacity, Object[] data) {
        this.window = window;
        this.size = size;
        this.capacity = capacity;
        this.data = data;
    }
	@Override
	public void remove() {
		for (int i = window; i < data.length - 1; i++) {
            data[i] = data[i + 1];
        }
        size--;
        if (window >= size) window = size - 1;
	}

	@Override
	public void goNext() {
        if (window == size - 1) throw new IndexOutOfBoundsException("Reached the end of the list");
        window++;		
	}

	@Override
	public void goPrev() {
		if (window == 0) throw new IndexOutOfBoundsException("Reached the beginning of the list");
        window--; 
	}

	@Override
	public void goTo(int n) {
		if (n < 0 || n >= data.length)
            throw new IndexOutOfBoundsException("There is no such index in this list");
        window = n;
	}

	@Override
	public L getActual() {
		if (isVoid()) throw new NullPointerException("The list is empty");
		if (!isSorted) {
			insertionSort();
			isSorted=true;
		}
        return (L) data[window];
	}

	@Override
	public int getActualPosition() {
		return window;
	}

	@Override
	public int size() {
        return size;
	}

	@Override
	public boolean isVoid() {
		 return data[0] == null;
	}

	@Override
	public boolean endList() {
		return window == data.length - 1;
	}
	 private void insertionSort (){
	        Object temp;
	        for (int i = 1; i < data.length; i++) {
	            for(int j = i ; j > 0 ; j--){
	            	L firstObject=(L)data[j];
	            	L secondObject=(L)data[j-1];
	                if(firstObject.compareTo(secondObject) < 0){
	                    temp = data[j];
	                    data[j] = data[j-1];
	                    data[j-1] = temp;
	                }
	            }
	        }
	    }
	@Override
	public GeneralList<L> clone() {
		if(!isSorted) {
			insertionSort();
			isSorted=true;
		}
		Object[] cloned = new Object[data.length];
        for (int i = 0; i < data.length; i++) cloned[i] = data[i];
        return new StaticSortedList<L>(window, size, capacity, cloned);
	}

	@Override
	public void insert(L obj) {
        if (size == data.length) enlarge();
        for (int i = data.length - 1; i > window; i--) data[i] = data[i - 1];
        data[window] = obj;
        isSorted=false;
        size++;
	}
	private void enlarge() {
        Object[] tempObjects = new Object[data.length + DEFAULT_CAPACITY];
        for (int i = 0; i < data.length; i++) tempObjects[i] = data[i];
        data = tempObjects;
    }

}
