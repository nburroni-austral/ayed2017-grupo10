package struct.impl;

public class StaticStack<T> implements struct.istruct.Stack<T>{
	private int size;
	private Object[] elements;
 	public StaticStack () {
	size=0;
	elements = new Object[1];
}
	@Override
	public void push(T t) {
		Object[] newStack = new Object[size+1];
		for (int i = 0; i < size; i++) {
			newStack[i]=elements[i];
		}
		newStack[size]=t;
		elements=newStack;
		size++;
	}

	@Override
	public void pop() {
		if(!isEmpty()) {
		elements[size-1]=null;
		size--;
		}
	}

	@Override
	public T peek() {
		if(isEmpty()) return null;
		return (T) elements[size-1];
	}

	@Override
	public boolean isEmpty() {
		if (size != 0) return false;
		else return true;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void empty() {
		size=0;
		
	}

}
