package struct.impl;

public class BinaryTree<T> implements struct.istruct.BinaryTree<T> {
	private DoubleNode<T> root;
	
	public BinaryTree() {
	root=null;
}
	public BinaryTree(T element){
		 root = new DoubleNode <>(element);
		 }
		public BinaryTree(T element, BinaryTree<T> leftTree, BinaryTree<T> rightTree){
		 
		 if(leftTree==null)root = new DoubleNode<T>(element,null, rightTree.root);
		 else if(rightTree==null)root = new DoubleNode<T>(element,leftTree.root, null);	
		 else root = new DoubleNode<T>(element,leftTree.root, rightTree.root);
		 }
		
	@Override
	public boolean isEmpty() {
		return root==null;
	}

	@Override
	public T getRoot() {
		if(!isEmpty())return root.element;
		return null;
	}

	@Override
	public BinaryTree<T> getLeft() {
		if(!isEmpty() && root.left!=null) {
			BinaryTree<T> returnTree = new BinaryTree<>();
			returnTree.root = root.left;
			 return returnTree;
		}
		return new BinaryTree<>();
	}

	@Override
	public BinaryTree<T> getRight() {
		if(!isEmpty()&& root.right!=null) {
			BinaryTree<T> returnTree = new BinaryTree<>();
			returnTree.root = root.right;
			 return returnTree;
		}
		return new BinaryTree<>();
	}
	
	private class DoubleNode <T>{
		 private T element;
		 private DoubleNode <T> right;
		 private DoubleNode <T> left;
		public DoubleNode(T o){
		 element = o;
		 }

		public DoubleNode(T o, DoubleNode<T> left, DoubleNode<T> right){
		 element = o;
		 this.right = right;
		 this.left = left;
		 }
		}
}
