package struct.impl;
import java.util.ArrayList;
public class BinaryTreeApi<T> {

public BinaryTreeApi() {

}
/**
 * returns the size of a tree
 * @param tree
 * the tree whose size needs to be known
 * @return
 * the size of that tree
 */
public int size (BinaryTree<T> tree){
 if(tree.isEmpty())
 return 0;
 else
 return 1 + size(tree.getLeft())+size(tree.getRight());
}

 /**
  * returns the nodes that are complete of a binary tree, 
  * a complete node has two childs 
 * @param tree
 * the tree
 * @return
 * the number of complete nodes
 */
public int completeNodes(BinaryTree<T> tree){
 if(tree.isEmpty())
 return 0;
 if (tree.getLeft().isEmpty())
 return completeNodes(tree.getRight());
 if (tree.getRight().isEmpty())
 return completeNodes(tree.getLeft());
 return 1+completeNodes(tree.getRight())+completeNodes(tree.getLeft());
 }

 /**
  * returns the number of times an object is in a tree
 * @param tree
 * the tree
 * @param o
 * the object to search within that tree
 * @return
 */
public int ocurrencies(BinaryTree<T> tree, T o){
 if(tree.isEmpty())
 return 0;
 if(tree.getRoot().equals(o))
 return 1 + ocurrencies(tree.getLeft(),o)+ocurrencies(tree.getRight(),o);
 else
 return ocurrencies(tree.getLeft(),o)+ocurrencies(tree.getRight(),o);
 }
 /**
  * print a binary tree in inorder on the console
 * @param tree
 * the tree to be printed
 */
 public void inorder(BinaryTree<T> tree){
 if(!tree.isEmpty()){
 if(tree.getLeft()!=null)inorder(tree.getLeft());
 System.out.println(tree.getRoot());
 if(tree.getRight()!=null)inorder(tree.getRight());
 }
 }

 /**
  * add binary tree to a list in inorder
 * @param tree
 * the tree to be added to the list
 * @param treeList
 * the list in which the tree will be added
 */
public void inorder(BinaryTree<T> tree, ArrayList<T> treeList){
	if(!tree.isEmpty()){
		if(tree.getLeft()!=null)inorder(tree.getLeft(),treeList);
		treeList.add(tree.getRoot());
		if(tree.getRight()!=null)inorder(tree.getRight(),treeList);
 }
 }
/**
 * print a binary tree in preorder on the console
* @param tree
* the tree to be printed
*/
public void preorder(BinaryTree<T> tree){
	if(!tree.isEmpty()){
		System.out.println(tree.getRoot());
		if(tree.getLeft()!=null)preorder(tree.getLeft());
		if(tree.getRight()!=null)preorder(tree.getRight());
}
}

/**
 * add binary tree to a list in preorder
* @param tree
* the tree to be added to the list
* @param treeList
* the list in which the tree will be added
*/
public void preorder(BinaryTree<T> tree, ArrayList<T> treeList){
	if(!tree.isEmpty()){
		if(tree.getLeft()!=null)preorder(tree.getLeft(),treeList);
		treeList.add(tree.getRoot());
		if(tree.getRight()!=null)preorder(tree.getRight(),treeList);
}
}

/**
 * print a binary tree in postorder on the console
* @param tree
* the tree to be printed
*/
public void postorder(BinaryTree<T> tree){
	if(!tree.isEmpty()){
		if(tree.getLeft()!=null)preorder(tree.getLeft());
		if(tree.getRight()!=null)preorder(tree.getRight());
		System.out.println(tree.getRoot());
}
}

/**
 * add binary tree to a list in postorder
* @param tree
* the tree to be added to the list
* @param treeList
* the list in which the tree will be added
*/
public void postorder(BinaryTree<T> tree, ArrayList<T> treeList){
if(!tree.isEmpty()){
	 if(tree.getLeft()!=null)preorder(tree.getLeft(),treeList);
	 if(tree.getRight()!=null)preorder(tree.getRight(),treeList);
	 treeList.add(tree.getRoot());
	 }
}
} 