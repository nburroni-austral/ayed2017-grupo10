package struct.impl;
import struct.istruct.list.GeneralList;

public class SortedList<L extends Comparable<L>> implements struct.istruct.list.SortedList<L> {
	private Node<L> head, window, sentinel;
    private int size;
    private boolean sorted;
    public SortedList() {
    	sorted = true;
    	 head = new Node<>();
         sentinel = new Node<>();
         head.next = sentinel;
         window = head;
         size = 0;
    }
	@Override
	public void remove() {
		if(isVoid()) throw new NullPointerException("This List is empty");
        goBack();
        window.next = window.next.next;
        window = window.next;
        if(window == sentinel) goBack();
        size--; 
	}
	
	@Override
	public void goNext() {
		if(window.next == sentinel) throw new IndexOutOfBoundsException("Reached the end of this List");
        window = window.next;
	}

	@Override
	public void goPrev() {
		if(window == head.next) throw new IndexOutOfBoundsException("Reached the beginning of this List");
        goBack(); 
        }
    private void goBack(){
        Node<L> aux = head;
        while(window != aux.next){
            aux = aux.next;
        }
        window = aux;
	}

	@Override
	public void goTo(int n) {
		window = head.next;
        for(int i = 0; i < n; i++){
            window = window.next;
        }
	}
private  void bubbleSort() {
	boolean changed = true;
	while (changed) {
		changed = false;
	for(int i=0; i<size-1;i++){
		goTo(i);
		if (window.obj.compareTo(window.next.obj)>0) {
				L tmpElement = window.obj;
				window.obj = window.next.obj;
				window.next.obj=tmpElement;
				changed = true;
	}
		}
	
	}sorted=true;
}

	@Override
	public L getActual() {
		int pos = getActualPosition();
		if(!sorted)bubbleSort();
		goTo(pos);
		 return window.obj;
	}

	@Override
	public int getActualPosition() {
		int pos = 0;
        if (!isVoid()) {
            Node<L> aux = head;
            for (; aux.next != window; pos++) aux = aux.next;
        }
        return pos; 
	}

	@Override
	public int size() {
return size;
	}

	@Override
	public boolean isVoid() {
        return head.next == sentinel;
	}

	@Override
	public boolean endList() {
		 return window.next == sentinel;
	}

	@Override
	public GeneralList<L> clone() {
		 return null;
	}

	@Override
	public void insert(L obj) {
		sorted=false;
        window.next = new Node<>(obj, window.next);
        window = window.next;
        size++;
	}
	private static class Node<E extends Comparable<E>> {
        E obj;
        Node<E> next;
        Node() {
            obj = null;
            next = null; 
            }
        Node(E o) {
            obj = o;
            next = null; 
            }
        Node(E o, Node<E> next) {
            this(o);
            this.next = next;
        }
        boolean hasNoObj() {
            return obj == null;
        }
    }
}
