package struct.impl;

import struct.istruct.Queue;

public class StaticQueue<Q> implements Queue<Q> {
private Object[] elements;
/**
 * the newest item on the queue
 */
private int back;
/**
 * the  oldest item on the queue
 */
private int front;
/**
 * the amount of items on the queue
 */
private int quantity;
/**
 * the maximum amount of items on the queue
 */
private int size;
public StaticQueue(int size) {
	this.size=size;
	quantity=0;
	elements = new Object[size];
	front = 0;
	back=0;
	
}
/*
 * primero chequear que el arreglo no este lleno, despues que back no sea
 * el final del arreglo, si back es igual al final del arreglo, entonces back=0;
 * 
 */
	@Override
	public void enqueue(Q q) {
		if(quantity==size) {
			grow();
			enqueue(q);
		}
		else {
			if (back==size) back=0;
			elements[back++]=q;
			quantity++;
		}
	}

	@Override
	public Q dequeue() {
		if(!isEmpty()) {
		Object returnValue = elements[front];
		if(front == size-1){ front =0;}
		else {front++;}
		quantity--;
		return (Q) returnValue;
		} else return null;
	}

	@Override
	public boolean isEmpty() {
		if(quantity==0) return true;
		return false;
	}

	@Override
	public int length() {
		return quantity;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void empty(){
		elements=new Object[size];
		quantity=0;
		front = 0;
		back=0;
			}
	private void grow() {
		size+=10;
		Object[] newElements=new Object[size];
		for (int i = 0; i < elements.length; i++) {
			newElements[i] = dequeue();
			quantity++;
		}
		front = 0;
		back = quantity;
		elements = newElements;
	}
}
