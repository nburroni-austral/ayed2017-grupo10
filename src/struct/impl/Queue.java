package struct.impl;


public class Queue<Q> implements struct.istruct.Queue<Q>{
	private Node<Q> back;
	private Node<Q> front;
	private int quantity;
	
	public Queue() {
	back = new Node<>(null);
	front = new Node<>(null);
	quantity=0;
	}
	@Override
	public void enqueue(Q q) {
		Node<Q> newNode = new Node<>(q);
		if(quantity==0) {
			front=newNode;
			back=newNode;
		}else {
		back.nextElement=newNode;
		back=newNode;
		}
		quantity++;
		}

	@Override
	public Q dequeue() {
		if (quantity==1) {
			Node<Q> returnNode = front;
			empty();
			return returnNode.element;
		}
		if(quantity==0) return null;
		Node<Q> returnNode = front;
		front=returnNode.nextElement;
		quantity--;
		return returnNode.element;
	}

	@Override
	public boolean isEmpty() {
		return quantity==0;
	}

	@Override
	public int length() {
		return quantity;
	}

	@Override
	public int size() {
		return quantity;
	}

	@Override
	public void empty() {
		back = new Node<>(null);
		front = new Node<>(null);
		quantity=0;
	}
	
	private class Node<Q>{
		private Q element;
		private Node<Q> nextElement;
		
		public Node(Q o) {
			nextElement=null;
			element = o;
		}
	}
}
