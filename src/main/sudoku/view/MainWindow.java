package main.sudoku.view;


import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import java.awt.event.ActionListener;


public class MainWindow extends JFrame {
	private JButton exitButton;
	private JButton solveButton;
	private JPanel contentPane;
	private JTextField[][] textFields;
	
	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setTitle("Sudoku");
		textFields = new JTextField[9][9];
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridLayout gbl_contentPane = new GridLayout(10,10,-1,-1);
		contentPane.setLayout(gbl_contentPane);
		
			for(int y = 0; y<textFields.length;y++) {
				for (int x = 0; x <textFields[y].length; x++) {
			textFields[x][y] = new JTextField();
			textFields[x][y].setColumns(2);
			textFields[x][y].setDocument (new JTextFieldLimit(1));
			contentPane.add(textFields[x][y]);
			int topBorder = 1;
			int leftBorder=1;
			int bottomBorder=1;
			int rightBorder=1;
			
			if (y%3 == 0) topBorder=3;
			else if (y%3 == 2) bottomBorder = 3;
			if (x%3 == 0) leftBorder=3;
			else if (x%3 == 2) rightBorder = 3;		
			if(y==0) topBorder = 5;
			else if(y==textFields[x].length-1) bottomBorder = 5;
			if (x == 0) leftBorder = 5;
			else if(x==textFields.length-1) rightBorder = 5;
			textFields[x][y].setBorder(BorderFactory.createMatteBorder(topBorder, 
                    leftBorder, 
                    bottomBorder, 
                    rightBorder, 
                    Color.BLACK));
		textFields[x][y].setHorizontalAlignment(JTextField.CENTER);
		textFields[x][y].setText("");
		}
			}
			
			exitButton = new JButton("Exit");
			solveButton = new JButton("Solve");
			contentPane.add(new JLabel()); //in grid layout we need to fill the empty grid spaces with something
			contentPane.add(new JLabel());
			contentPane.add(new JLabel());
			contentPane.add(new JLabel());
			contentPane.add(new JLabel());
			contentPane.add(new JLabel());
			contentPane.add(new JLabel());
			contentPane.add(solveButton);
			contentPane.add(exitButton);
			solveButton.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
			
			getRootPane().setDefaultButton(solveButton);
			
		setLocationRelativeTo(null);
		pack();
		setVisible(true);
	}
	/**Method to add a listener to the exit button
	 * @param exitButtonAction
	 * an ActionListener to link to this button
	 */
	public void addExitListener(ActionListener exitButtonAction) {
		exitButton.addActionListener(exitButtonAction);
	}
	/**Method to add a listener to the solve button
	 * @param solveButtonAction
	 * an ActionListener to link to this button
	 */
	public void addSolveButtonListener(ActionListener solveButtonAction) {
		solveButton.addActionListener(solveButtonAction);
	}
	/**
	 * method for reading the data that the user entered in the board
	 * @return
	 * the values  of all the cells in the board
	 */
	public String[][] getBoardValues(){
		String[][] returnValue = new String[textFields.length][textFields[0].length];
		for(int y = 0; y<textFields.length;y++) {
			for (int x = 0; x <textFields[y].length; x++) {
				returnValue[x][y]= textFields[x][y].getText();
			}
			} return returnValue;
	}
	/**
	 * method to update the value of one cell
	 * @param location
	 * the location of that cell in the board
	 * @param value
	 * the value to be put in that cell
	 */
	public void setBoardValue(int[] location, int value) {
		textFields[location[0]][location[1]].setText(String.valueOf(value));
		
	}
	/**
	 * method for locking the text fields, so the user can't input anything while the puzzle is being solved. 
	 */
	public void lockBoard() {
		for(int y = 0; y<textFields.length;y++) {
			for (int x = 0; x <textFields[y].length; x++) {
				textFields[x][y].setEnabled(false);
			}
			}
		solveButton.setEnabled(false);
	}
	/**
	 * method for unlocking the text fields, in case the user has to input something after the puzzle was solved. 
	 */
	public void unlockBoard() {
		for(int y = 0; y<textFields.length;y++) {
			for (int x = 0; x <textFields[y].length; x++) {
				textFields[x][y].setEnabled(true);
			}
			}
		solveButton.setEnabled(true);
	}
	/**
	 * private class to limit the amount of characters that the user can input
	 *
	 */
	private class JTextFieldLimit extends PlainDocument {
		  private int limit;

		  JTextFieldLimit(int limit) {
		   super();
		   this.limit = limit;
		   }

		  public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException {
		    if (str == null) return;

		    if ((getLength() + str.length()) <= limit) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
}
