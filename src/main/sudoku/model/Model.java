package main.sudoku.model;

import main.sudoku.controller.MainController;

public class Model {
	int[][] puzzleGrid;
	MainController controller;
	public Model(MainController controller) {
	this.controller = controller;
	}
/**
 * main method for solving the sudoku puzzle
 * @param incompletePuzzle
 * the raw input data from the board
 * @return
 * true if the puzzle could be solved, false if it has no solution
 * @throws IllegalArgumentException
 * when an invalid puzzle is entered (one that has letters or a number that is not between 0-9
 */
public boolean solveSudoku(String[][] incompletePuzzle) throws IllegalArgumentException{
	fillPuzzleGrid(incompletePuzzle);
	int[] start = {0, 0};
	return solveSudoku(start); 
}

/**
 * private method for the recursive calls when resolving the puzzle
 * @param cell
 * the current cell being solved
 * @return
 * true if the puzzle has a solution, false if the puzzle has no solution
 */
private boolean solveSudoku(int[] cell) {
	if (cell == null) return true;
	int x = cell[0];
	int y = cell[1];
	
	if(puzzleGrid[x][y]!=0) {
		int tempValue = puzzleGrid[x][y];
		puzzleGrid[x][y] = 0;
		if(!isLegalMove(cell, tempValue)) return false;
		puzzleGrid[x][y] = tempValue;
		return solveSudoku(getNextCell(cell));
	}
	for (int i = 1; i < 10; i++) { 
		   boolean valid = isLegalMove(cell, i);
		   if (!valid) 
		    continue;
		   puzzleGrid[x][y] = i;
		  controller.setBoardValue(cell, i);
		   boolean solved = solveSudoku(getNextCell(cell));
		   if (solved)
		    return true;
		   else
			   puzzleGrid[x][y] = 0;
		    controller.setBoardValue(cell, i);
		  }
return false;
		 }

/**
 * method for getting the next cell in the grid
 * @param cell
 * the cell whose next cell you want to get
 * @return
 * the next cell of the cell stated in the parameter
 */
private int[] getNextCell(int[] cell) {
	int[] newCell = {cell[0], cell[1]};
	if (newCell[0]==8) {
		newCell[0]=0;
		newCell[1]++;
	}else {
		newCell[0]++;
	}
	if (newCell[1]==9) return null;
	return newCell;

}

/**
 * method for checking that a number can be legally used to fill a cell
 * @param cell
 * the cell in which te value must go
 * @param value
 * the value to put in the cell
 * @return
 * true if that value can go in that cell without violating sudoku rules, 
 * false if that value violates a sudoku rule
 */
public boolean isLegalMove(int[] cell, int value) {
	int x=cell[0];
	int y=cell[1];
	for (int i = 0; i < puzzleGrid[x].length; i++) {
		if(puzzleGrid[x][i]==value)return false;		
	}
	for (int i = 0; i < puzzleGrid.length; i++) {
		if(puzzleGrid[i][y]==value)return false;		
	}
	int xOffset = (x / 3)*3;
    int yOffset = (y / 3)*3;
    for (int k = 0; k < 3; k++) {
        for (int m = 0; m < 3; m++) {
            if (puzzleGrid[xOffset+k][yOffset+m]==value) return false;
        }
        }
    return true;
}


/**
 * method for filling the internal grid with an incomplete puzzle
 * @param rawPuzzleGrid
 * the raw grid input from a user
 * @throws IllegalArgumentException
 * if the raw input contains letters, or numbers lower than 0 and bigger than 10
 */
public void fillPuzzleGrid(String[][] rawPuzzleGrid) throws IllegalArgumentException{
	puzzleGrid = new int[rawPuzzleGrid.length][rawPuzzleGrid[0].length];
	for (int y = 0; y < rawPuzzleGrid.length; y++) {
		for (int x = 0; x < rawPuzzleGrid[y].length; x++) {
			if (rawPuzzleGrid[x][y].equals("")) rawPuzzleGrid[x][y]="0";
			try
		      {
		         puzzleGrid[x][y] = Integer.parseInt(rawPuzzleGrid[x][y]);
		      }
		      catch (NumberFormatException e)
		      {
		        throw new IllegalArgumentException("please enter NUMBERS ONLY, thank you");
		      }
			if (puzzleGrid[x][y] < 0 || puzzleGrid[x][y] > 9) throw new IllegalArgumentException("Sudoku is played with numbers from 1 to 9...");
		}
	}
	
}
}
