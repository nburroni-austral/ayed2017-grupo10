package main.sudoku;
import javax.swing.*;

import main.sudoku.controller.MainController;
public class Main {
    /**
     * Main method that sets look and feel and initializes the application
     */
    public static void main(String[] args){
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); //tell Swing to display GUI components according to the OS style
			new MainController();
		} catch (Exception e) { //need to handle .setLookAndFeel exceptions
			e.printStackTrace();
		}
    }
}
