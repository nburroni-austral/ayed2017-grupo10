package main.sudoku.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.sudoku.model.Model;
import main.sudoku.view.ErrorWindow;
import main.sudoku.view.MainWindow;

public class MainController {
		private Model actions;
		private MainWindow display;
		Thread t;
	public MainController() {
		actions = new Model(this);
		display = new MainWindow();
		display.addExitListener(new ExitListener());
		display.addSolveButtonListener(new SolveButtonListener());
		display.setVisible(true);
	}
	public void setBoardValue(int[] location, int value) {
		display.setBoardValue(location, value);
		
	}
	
	/**
	 * Listener class for exit button
	 *
	 */
	class ExitListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent buttonPressed) {
			System.exit(0);
		}
	}
	/**
	 * Listener class for solve button
	 *
	 */
	class SolveButtonListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent buttonPressed) {
			boolean sudokuCompleted = false;
			try {
				sudokuCompleted = actions.solveSudoku(display.getBoardValues());
				if(!sudokuCompleted) new ErrorWindow("this board has no solution!", display);
				display.lockBoard();
			} catch (IllegalArgumentException e) {
				new ErrorWindow(e.getMessage(), display);
			}finally {
				if (!sudokuCompleted) display.unlockBoard();
			}
		}
	}
}
