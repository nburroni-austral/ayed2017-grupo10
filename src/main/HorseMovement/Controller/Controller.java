package main.HorseMovement.Controller;



import main.HorseMovement.View.ErrorWindow;
import main.HorseMovement.Model.HorseMovement;
import main.HorseMovement.View.MainWindow;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
    MainWindow display;
    HorseMovement logic;
    public Controller(){
        display = new MainWindow();
        logic = new HorseMovement(4);
        display.addNextListener(new NextButtonListener());
    }


    public class NextButtonListener implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
        	try {

				display.updateHorseLocation(logic.moveHorse());
			} catch (IllegalArgumentException e1) {
				new ErrorWindow(e1.getMessage(), display);
			}
        }
    }


}
