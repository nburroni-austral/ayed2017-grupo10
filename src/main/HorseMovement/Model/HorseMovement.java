package main.HorseMovement.Model;

import java.util.ArrayList;

import struct.impl.Stack;

public class HorseMovement {
    private int[][] board;
    private Point horseLocation;
    private ArrayList<Point> horsePreviousLocations;
    private ArrayList<Stack<Point>> listOfossibleMovements;
    private int maxMovements;
    private int doneMovements;
    private boolean returning;
    public HorseMovement(int maxMovements){
    	horsePreviousLocations=new ArrayList<>();
    	this.maxMovements=maxMovements-1;
    	doneMovements = 0;
    	returning=false;
        board = new int[8][8];
        horseLocation = new Point(0,0);
        listOfossibleMovements = new ArrayList<>();
        horsePreviousLocations.add(horseLocation);
        fillPossibleMovements(new Point(0, 0));
    }
    public Point moveHorse() {
    	try {
    	if(doneMovements<maxMovements) {
    		
    		horseLocation=possibleMovements(horseLocation).peek();
    		horsePreviousLocations.add(horseLocation);
    		fillPossibleMovements(horseLocation);
    		
    		doneMovements++;
    	}else {
    		if(returning) {
    			horseLocation=horsePreviousLocations.get(horsePreviousLocations.size()-1);
    			returning=false;
    		}else {
    			if(listOfossibleMovements.get(listOfossibleMovements.size()-1).size()==0) {
    				board[horseLocation.getX()][horseLocation.getY()]=-1;
    				horsePreviousLocations.remove(horsePreviousLocations.size()-1);
    				listOfossibleMovements.remove(listOfossibleMovements.size()-1);
    				doneMovements=listOfossibleMovements.size();
    				horseLocation=horsePreviousLocations.get(horsePreviousLocations.size()-1);
    				listOfossibleMovements.get(listOfossibleMovements.size()-1).pop();
    			}else {
    	horseLocation=listOfossibleMovements.get(listOfossibleMovements.size()-1).peek();
    	listOfossibleMovements.get(listOfossibleMovements.size()-1).pop();
    	returning=true;
    			}
    	}
    }
    	} catch (ArrayIndexOutOfBoundsException e) {
    		throw new ArrayIndexOutOfBoundsException("The horse can't keep on moving anywhere");
		}
    	/*if (possibleMovements.get(possibleMovements.size()-1).size()==0) possibleMovements.remove(possibleMovements.size()-1);
    	else  possibleMovements.get(possibleMovements.size()-1).pop();
    	horseLocation=possibleMovements.get(possibleMovements.size()-1).peek();*/
    	return horseLocation; 
    }
    public Stack<Point> possibleMovements(Point horseCoordinates){
        Stack<Point> listOfMovements = new Stack<>();

            if(canMove(horseCoordinates.getX() -1, horseCoordinates.getY() +2)){
                listOfMovements.push(new Point(horseCoordinates.getX() -1, horseCoordinates.getY() +2));
            }
            if(canMove(horseCoordinates.getX() - 2, horseCoordinates.getY() +1)){
                listOfMovements.push(new Point(horseCoordinates.getX() - 2, horseCoordinates.getY() +1));
            }
            if(canMove(horseCoordinates.getX() - 2, horseCoordinates.getY() -1)){
                listOfMovements.push(new Point(horseCoordinates.getX() - 2, horseCoordinates.getY() -1));
            }
            if(canMove(horseCoordinates.getX() -1 , horseCoordinates.getY() -2)){
                listOfMovements.push(new Point(horseCoordinates.getX() -1 , horseCoordinates.getY() -2));
            }
            if(canMove(horseCoordinates.getX() + 1, horseCoordinates.getY() -2)){
                listOfMovements.push(new Point(horseCoordinates.getX() + 1, horseCoordinates.getY() -2));
            }
            if(canMove(horseCoordinates.getX() + 2, horseCoordinates.getY() -1)){
                listOfMovements.push(new Point(horseCoordinates.getX() + 2, horseCoordinates.getY() -1));
            } if(canMove(horseCoordinates.getX() + 1, horseCoordinates.getY() +2)){
                listOfMovements.push(new Point(horseCoordinates.getX() + 1, horseCoordinates.getY() +2));
            }            if(canMove(horseCoordinates.getX() + 2, horseCoordinates.getY() +1)){
                listOfMovements.push(new Point(horseCoordinates.getX() + 2, horseCoordinates.getY() +1));
            }

        return listOfMovements;


    }
    public void fillPossibleMovements(Point aPoint) {
		Stack<Point> someMovements = possibleMovements(aPoint);
		listOfossibleMovements.add(someMovements);

}
    public boolean canMove(int row, int column) {
        if (row >= 0 && column >= 0 && row < 8 && column < 8 && board[row][column] != -1) {
            return true;
        }
        return false;
    }
}
