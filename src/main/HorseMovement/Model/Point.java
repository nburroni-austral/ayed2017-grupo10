package main.HorseMovement.Model;

public class Point {
private int x;
private int y;
public Point(int x, int y) {
	super();
	this.x = x;
	this.y = y;
}
public int getX() {
	return x;
}
public int getY() {
	return y;
}
public void setX(int x) {
	if(x>=0)this.x = x;
}
public void setY(int y) {
	
	if(y>=0)this.y = y;
}
}
