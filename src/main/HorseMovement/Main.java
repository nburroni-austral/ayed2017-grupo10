package main.HorseMovement;

import javax.swing.UIManager;

import main.HorseMovement.Controller.Controller;
import main.HorseMovement.Model.HorseMovement;

public class Main {
    public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); //tell Swing to display GUI components according to the OS style
			new Controller();
		} catch (Exception e) { //need to handle .setLookAndFeel exceptions
			e.printStackTrace();
		}
    }
}
