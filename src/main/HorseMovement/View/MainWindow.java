package main.HorseMovement.View;


import main.HorseMovement.Model.*;
import main.HorseMovement.Model.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class MainWindow extends JFrame{

    private JButton nextButton;
    private JPanel contentPane;
    private JPanel boardPanel;
    private JPanel[][] colours;
    private JPanel numbers;
    private JLabel[][] horseImages;
    private main.HorseMovement.Model.Point horseLocation;
    public MainWindow(){
    	horseLocation = new Point(0,0);

    	horseImages = new JLabel[8][8];
        setTitle("Horse Movement");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(500 ,500);
        setLocationRelativeTo(null);
        setResizable(false);
        setBackground(Color.LIGHT_GRAY);

        contentPane = new JPanel();
        contentPane.setLayout(new BorderLayout(5,5));
        contentPane.setBackground(Color.LIGHT_GRAY);
        contentPane.setVisible(true);
        boardPanel = new JPanel();
        boardPanel.setBackground(Color.LIGHT_GRAY);
        boardPanel.setLayout(new GridLayout(9,9));


        colours = new JPanel[9][9];
        for(int i= 0; i<8 ; i++){
            for(int j = 0 ; j<8 ; j++){
                if(i%2 ==0){
                    if(j%2 == 0){
                        colours[i][j] = new JPanel();
                        colours[i][j].setBackground(Color.WHITE);
                        boardPanel.add(colours[i][j]);
                    }
                    else {
                        colours[i][j] = new JPanel();
                        colours[i][j].setBackground(Color.BLACK);
                        boardPanel.add(colours[i][j]);

                    }

                }
                else{
                    if(j%2 == 0){
                        colours[i][j] = new JPanel();
                        colours[i][j].setBackground(Color.BLACK);
                        boardPanel.add(colours[i][j]);
                    }
                    else{
                        colours[i][j] = new JPanel();
                        colours[i][j].setBackground(Color.WHITE);
                        boardPanel.add(colours[i][j]);
                    }
                }
                horseImages[i][j] = new JLabel();
            	horseImages[i][j].setIcon(new ImageIcon(MainWindow.class.getResource("/main/HorseMovement/View/caballo.png")));
            	horseImages[i][j].setVisible(false);
                colours[i][j].add(horseImages[i][j]);
            }
        }
        String abc = "ABCDEFGH";
        for (int i = 0; i < 8; i++) {
        	JLabel letter = new JLabel(String.valueOf(abc.charAt(i)), SwingConstants.CENTER);
        	boardPanel.add(letter);
        }
        
        numbers = new JPanel();
        numbers.setLayout(new GridLayout(9,1));
        numbers.setBackground(Color.LIGHT_GRAY);
        for(int i = 1; i< 9; i++) {
        	JLabel number = new JLabel(String.valueOf(i));
        	numbers.add(number);
        }

        contentPane.add(numbers, BorderLayout.LINE_START);
        contentPane.add(boardPanel, BorderLayout.CENTER);

        nextButton = new JButton("Next");
        nextButton.setSize(50, 50);
        nextButton.setAlignmentX(CENTER_ALIGNMENT);
        contentPane.add(nextButton, BorderLayout.PAGE_END);


        horseImages[0][0].setVisible(true);
        getContentPane().add(contentPane);
        setVisible(true);



    }

    public void addNextListener(ActionListener nextButtonListener){
        nextButton.addActionListener(nextButtonListener);
    }
public void updateHorseLocation(Point newLocation) {
	horseImages[horseLocation.getX()][horseLocation.getY()].setVisible(false);
	horseImages[newLocation.getX()][newLocation.getY()].setVisible(true);
	horseLocation = newLocation;
}
}























