package main.HorseMovement.View;
import javax.swing.*;

public class ErrorWindow extends JOptionPane {
	/**
	 * Create the dialog.
	 */
	public ErrorWindow(String message, JFrame parent) {
		showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	}
