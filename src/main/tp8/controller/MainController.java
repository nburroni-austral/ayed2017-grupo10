package main.tp8.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.tp8.model.Database;
import main.tp8.model.Lamp;
import main.tp8.view.CreateALampWindow;
import main.tp8.view.ErrorWindow;
import main.tp8.view.MainWindow;
import main.tp8.view.ModifyLampWindow;
import main.tp8.view.ShowLamps;
import main.tp8.view.lampCodeInputWindow;
import struct.impl.List;

public class MainController {
	private MainWindow view;
	private Database database;
	
public MainController() {
	view = new MainWindow();
	view.setVisible(true);
	view.addShowStockListener(new ShowLampsListener());
	view.addNewLampListener(new CreateLampListener());
	view.addRemoveLampListener(new DeleteLampListener());
	view.addModifyLampListener(new ModifyLampListener());
	database = new Database();
}
class CreateLampListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent e) {
		Lamp lamp = new CreateALampWindow().showCreateALampWindow(view);
		try {
			if(lamp!=null)database.insertLamp(lamp); 
		} catch (Exception e1) {
			new ErrorWindow(e1.getMessage(), view);
		}
	}
	
}
class ModifyLampListener implements ActionListener{
	@Override
	public void actionPerformed(ActionEvent e) {
		String lampCode = new lampCodeInputWindow().showLampCodeInputWindow(view);
		Lamp lamp;
		if(lampCode!=null){
			try {
			lamp = database.searchLamp(lampCode);
			Lamp newLamp = new ModifyLampWindow().showModifyLampWindow(lamp, view);
			database.modifyLamp(newLamp);
		} catch (Exception e1) {
			new ErrorWindow(e1.getMessage(), view);
		}
	}}
	
}
class DeleteLampListener implements ActionListener{

	@Override
	public void actionPerformed(ActionEvent e) {
		String lampCode = new lampCodeInputWindow().showLampCodeInputWindow(view);
		if(lampCode!=null){try {
			database.deleteLamp(lampCode);
		} catch (Exception e1) {
			new ErrorWindow(e1.getMessage(), view);
		}}
	}
	
}
class ShowLampsListener implements ActionListener{
	@Override
	public void actionPerformed(ActionEvent buttonPressed) {
		List<Lamp> lamps = database.getLamps();
		String[] columnNames = {"Lamp code", "Watts", "Lamp kind", "Quantity in stock"};
		Object[][] data = new Object[lamps.size()][4];
		for (int x = 0; x < data.length; x++) {
			lamps.goTo(x);
			for (int y = 0; y < data[x].length; y++) {
				switch (y) {
					case 0:
						data[x][y]=lamps.getActual().getCode();
						break;
					case 1:
						data[x][y]=lamps.getActual().getWatts();
						break;
					case 2:
						data[x][y]=lamps.getActual().getKindOfLamp();
						break;
					case 3:
						data[x][y]=lamps.getActual().getQuantity();
						break;
					default:
						break;
				}
			}
		}
		new ShowLamps(columnNames, data, view);
	}
}

}
