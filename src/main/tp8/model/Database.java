package main.tp8.model;

import struct.impl.BinarySearchTree;
import struct.impl.List;

public class Database {
private BinarySearchTree<Lamp> lamps;
public Database() {
	lamps = new BinarySearchTree<>();
}
public void insertLamp(Lamp aLamp) {
	lamps.insert(aLamp);
}
public Lamp searchLamp(String code) {
	return lamps.search(new Lamp(code, 0, "", 0));
}
public void deleteLamp(String code) {
	Lamp lamp = searchLamp(code);
	lamps.delete(lamp);
}
public void modifyLamp(Lamp aLamp) {
deleteLamp(aLamp.getCode());
insertLamp(aLamp);
}
private List<Lamp> getLamps(BinarySearchTree<Lamp> tree, List<Lamp> lamps){
	try {
	tree.getRoot();
		 try {
			 getLamps(tree.getLeft(), lamps);
		 }catch (NullPointerException e) {
			
		}
		 try {
			 lamps.insertNext(tree.getRoot());
		 }catch (NullPointerException e) {
			
		}
		 try {
			 getLamps(tree.getRight(), lamps);
		 }catch (NullPointerException e) {
			
		}
		//if(tree.getLeft()!=null)getLamps(tree.getLeft(), lamps);
		// lamps.insertNext(tree.getRoot());
		 //if(tree.getRight()!=null)getLamps(tree.getRight(), lamps);
	}catch (Exception e) {
		
	}finally {
		return lamps;
	}
	}
public List<Lamp> getLamps(){
	return getLamps(lamps, new List<>());
	
}

}
