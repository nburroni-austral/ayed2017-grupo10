package main.tp8.model;

public class Lamp implements Comparable<Lamp>{
private String code;
private int watts;
private String kindOfLamp;
private int quantity;
public Lamp(String code, int watts, String kindOfLamp, int quantity) {
	this.code = code;
	this.watts = watts;
	this.kindOfLamp = kindOfLamp;
	this.quantity = quantity;
}
public String getCode() {
	return code;
}
public int getWatts() {
	return watts;
}
public String getKindOfLamp() {
	return kindOfLamp;
}
public int getQuantity() {
	return quantity;
}
@Override
public int compareTo(Lamp o) {
	return this.code.compareTo(o.getCode());
}
@Override
public boolean equals(Object anObject) {
    if ( this == anObject ) return true;
    if ( !(anObject instanceof Lamp) ) return false; //this also checks for null
    Lamp aLamp = (Lamp)anObject; //cast to Lamp is now safe
    return this.code.equals(aLamp.getCode());
}
}
