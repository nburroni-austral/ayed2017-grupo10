package main.tp8.view;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import javax.swing.JDialog;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class lampCodeInputWindow extends JFrame {
	private JButton btnDelete;
	private JPanel contentPane;
	private JTextField textField;
	private String returnName;
	/**
	 * Create the frame.
	 */
	public String showLampCodeInputWindow(JFrame parent) {
		JDialog dialog = new JDialog();
		dialog.setResizable(false);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setModal(true);
		dialog.setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		dialog.setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblEnterLampCode = new JLabel("Enter lamp code:");
		GridBagConstraints gbc_lblEnterLampCode = new GridBagConstraints();
		gbc_lblEnterLampCode.insets = new Insets(0, 0, 5, 5);
		gbc_lblEnterLampCode.gridx = 0;
		gbc_lblEnterLampCode.gridy = 0;
		contentPane.add(lblEnterLampCode, gbc_lblEnterLampCode);
		
		textField = new JTextField();
		textField.setDocument (new JTextFieldLimit(5));
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 0, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 1;
		contentPane.add(textField, gbc_textField);
		textField.setColumns(10);
		textField.setDocument (new JTextFieldLimit(5));
		btnDelete = new JButton("Ok");
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.gridx = 1;
		gbc_btnDelete.gridy = 1;
		contentPane.add(btnDelete, gbc_btnDelete);
		btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            	String tempReturnName=textField.getText();
                JButton button = (JButton)e.getSource();
                if(tempReturnName.equals("")) {
                	new ErrorWindow("Please fill all the fields", dialog);
                }
                else if(tempReturnName.length()!=5) new ErrorWindow("Code should be 5 characters long", dialog);
                else {
               returnName=tempReturnName;
               SwingUtilities.getWindowAncestor(button).dispose();
                	               	
            }
                }
        });
		dialog.getRootPane().setDefaultButton(btnDelete);
        dialog.pack();
        dialog.setLocationRelativeTo(parent);
        dialog.setVisible(true);
        return returnName;
	}
	/**
	 * private class to limit the amount of characters that the user can input
	 *
	 */

}
