package main.tp8.view;
import javax.swing.*;

/**
 * simple class for displaying errors or exceptions.
 *
 */
public class ErrorWindow extends JOptionPane {
	/**
	 * Create the dialog.
	 */
	public ErrorWindow(String message, JFrame parent) {
		showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	public ErrorWindow(String message, JDialog parent) {
		showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	}
