package main.tp8.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import main.tp8.model.Lamp;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
/**
 * window to allow the user to input a set of parameters in order to create a car.
 *
 */
public class CreateALampWindow {

        private String code = "";
        private String model = "";
        private String quantity = "";
        private String watts = "";
        Lamp returnLamp;
        private JTextField wattsInput;
        private JTextField modelInput;
        private JTextField quantityInput;
        private JTextField codeInput;
        /**
         * method for creating this window
         * @param parent
         * the JFrame that created this window (for centering purposes)
         * @return
         * a Lamp with the parameters entered by the user
         */
        public Lamp showCreateALampWindow(JFrame parent) {

            JPanel content = new JPanel();
            content.setBorder(new EmptyBorder(5, 5, 10, 10));

            JDialog dialog = new JDialog();
            dialog.setResizable(false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setModal(true);
            dialog.setTitle("Create a lamp");
            dialog.getContentPane().add(content);
            GridBagLayout gbl_content = new GridBagLayout();
            gbl_content.columnWidths = new int[]{113, 65, 45, 0};
            gbl_content.rowHeights = new int[]{14, 0, 0, 0, 23, 0};
            gbl_content.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
            gbl_content.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
            content.setLayout(gbl_content);
            
            JLabel lblCode = new JLabel("enter lamp code: ");
            GridBagConstraints gbc_lblCode = new GridBagConstraints();
            gbc_lblCode.anchor = GridBagConstraints.EAST;
            gbc_lblCode.insets = new Insets(0, 0, 5, 5);
            gbc_lblCode.gridx = 0;
            gbc_lblCode.gridy = 0;
            content.add(lblCode, gbc_lblCode);
            
            codeInput = new JTextField();
            codeInput.setColumns(10);
            GridBagConstraints gbc_quantityInput = new GridBagConstraints();
            gbc_quantityInput.gridwidth = 2;
            gbc_quantityInput.insets = new Insets(0, 0, 5, 0);
            gbc_quantityInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_quantityInput.gridx = 1;
            gbc_quantityInput.gridy = 0;
            content.add(codeInput, gbc_quantityInput);
            
            JLabel lblWatts = new JLabel("Enter lamp watts: ");
            GridBagConstraints gbc_lblWatts = new GridBagConstraints();
            gbc_lblWatts.anchor = GridBagConstraints.EAST;
            gbc_lblWatts.insets = new Insets(0, 0, 5, 5);
            gbc_lblWatts.gridx = 0;
            gbc_lblWatts.gridy = 1;
            content.add(lblWatts, gbc_lblWatts);
            
            wattsInput = new JTextField();
            wattsInput.setColumns(10);
            GridBagConstraints gbc_colorInput = new GridBagConstraints();
            gbc_colorInput.gridwidth = 2;
            gbc_colorInput.insets = new Insets(0, 0, 5, 0);
            gbc_colorInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_colorInput.gridx = 1;
            gbc_colorInput.gridy = 1;
            content.add(wattsInput, gbc_colorInput);
            
            JLabel lblEnterCarModel = new JLabel("Enter lamp model: ");
            GridBagConstraints gbc_lblEnterCarModel = new GridBagConstraints();
            gbc_lblEnterCarModel.anchor = GridBagConstraints.EAST;
            gbc_lblEnterCarModel.insets = new Insets(0, 0, 5, 5);
            gbc_lblEnterCarModel.gridx = 0;
            gbc_lblEnterCarModel.gridy = 2;
            content.add(lblEnterCarModel, gbc_lblEnterCarModel);
            
            modelInput = new JTextField();
            modelInput.setColumns(10);
            GridBagConstraints gbc_modelInput = new GridBagConstraints();
            gbc_modelInput.gridwidth = 2;
            gbc_modelInput.insets = new Insets(0, 0, 5, 0);
            gbc_modelInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_modelInput.gridx = 1;
            gbc_modelInput.gridy = 2;
            content.add(modelInput, gbc_modelInput);
            
            JLabel lblQuantity = new JLabel("Enter lamp quantity: ");
            GridBagConstraints gbc_lblEnterCarBrand = new GridBagConstraints();
            gbc_lblEnterCarBrand.anchor = GridBagConstraints.EAST;
            gbc_lblEnterCarBrand.insets = new Insets(0, 0, 5, 5);
            gbc_lblEnterCarBrand.gridx = 0;
            gbc_lblEnterCarBrand.gridy = 3;
            content.add(lblQuantity, gbc_lblEnterCarBrand);
            
            quantityInput = new JTextField();
            quantityInput.setColumns(10);
            GridBagConstraints gbc_brandInput = new GridBagConstraints();
            gbc_brandInput.gridwidth = 2;
            gbc_brandInput.insets = new Insets(0, 0, 5, 0);
            gbc_brandInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_brandInput.gridx = 1;
            gbc_brandInput.gridy = 3;
            content.add(quantityInput, gbc_brandInput);
            
            JButton cancelButton = new JButton("Cancel");
            GridBagConstraints gbc_cancelButton = new GridBagConstraints();
            gbc_cancelButton.insets = new Insets(0, 0, 0, 5);
            gbc_cancelButton.gridx = 1;
            gbc_cancelButton.gridy = 4;
            content.add(cancelButton, gbc_cancelButton);
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JButton button = (JButton)e.getSource();
                    returnLamp=null;
                    SwingUtilities.getWindowAncestor(button).dispose();
                }
            });
            JButton okButton = new JButton("Ok");
            GridBagConstraints gbc_okButton = new GridBagConstraints();
            gbc_okButton.gridx = 2;
            gbc_okButton.gridy = 4;
            content.add(okButton, gbc_okButton);
            codeInput.setDocument (new JTextFieldLimit(5));
            modelInput.setDocument (new JTextFieldLimit(10));
            quantityInput.setDocument (new JTextFieldLimit(9));
            wattsInput.setDocument (new JTextFieldLimit(9));
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                	code=codeInput.getText();
                	model=modelInput.getText();
                	quantity=quantityInput.getText();
                	watts=wattsInput.getText();
                    JButton button = (JButton)e.getSource();
                    if(code.equals("") || (model.equals("")) || (quantity.equals("")) || (watts.equals(""))) {
                    	new ErrorWindow("Please fill all the fields", dialog);
                    }else if(code.length()!=5) 	new ErrorWindow("Code should be 5 characters long", dialog);
                    else {
                    try {
                    	returnLamp = new Lamp(code, Integer.valueOf(watts), model, Integer.valueOf(quantity));
					} catch (Exception e2) {
						new ErrorWindow("Please enter valid values in all fields", dialog);
					}finally {
						SwingUtilities.getWindowAncestor(button).dispose();
					}
                    	               	
                }
                    }
            });
            dialog.getRootPane().setDefaultButton(okButton);
            dialog.pack();
            dialog.setLocationRelativeTo(parent);
            dialog.setVisible(true);
            return returnLamp;
        }
    }


//}