package main.tp8.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;

public class MainWindow extends JFrame {

	private JPanel contentPane;
	private JButton btnAddLamp;
	private JButton btnModifyLamp;
	private JButton btnRemoveLamp;
	JButton btnShowStock;
	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{87, 77, 89, 97, 0};
		gbl_contentPane.rowHeights = new int[]{23, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		btnShowStock = new JButton("Show stock");
		GridBagConstraints gbc_btnShowStock = new GridBagConstraints();
		gbc_btnShowStock.insets = new Insets(0, 0, 0, 5);
		gbc_btnShowStock.gridx = 0;
		gbc_btnShowStock.gridy = 0;
		contentPane.add(btnShowStock, gbc_btnShowStock);
		
		btnAddLamp = new JButton("Add lamp");
		GridBagConstraints gbc_btnAddLamp = new GridBagConstraints();
		gbc_btnAddLamp.insets = new Insets(0, 0, 0, 5);
		gbc_btnAddLamp.gridx = 1;
		gbc_btnAddLamp.gridy = 0;
		contentPane.add(btnAddLamp, gbc_btnAddLamp);
		
		btnModifyLamp = new JButton("Modify lamp");
		GridBagConstraints gbc_btnModifyLamp = new GridBagConstraints();
		gbc_btnModifyLamp.insets = new Insets(0, 0, 0, 5);
		gbc_btnModifyLamp.gridx = 2;
		gbc_btnModifyLamp.gridy = 0;
		contentPane.add(btnModifyLamp, gbc_btnModifyLamp);
		
		btnRemoveLamp = new JButton("Remove lamp");
		GridBagConstraints gbc_btnRemoveLamp = new GridBagConstraints();
		gbc_btnRemoveLamp.gridx = 3;
		gbc_btnRemoveLamp.gridy = 0;
		contentPane.add(btnRemoveLamp, gbc_btnRemoveLamp);
		setLocationRelativeTo(null);
		pack();
	}
	public void addRemoveLampListener(ActionListener e) {
		btnRemoveLamp.addActionListener(e);
	}
	public void addNewLampListener(ActionListener e) {
		btnAddLamp.addActionListener(e);
	}
	public void addModifyLampListener(ActionListener e) {
		btnModifyLamp.addActionListener(e);
	}
	public void addShowStockListener(ActionListener e) {
		btnShowStock.addActionListener(e);
	}
}
