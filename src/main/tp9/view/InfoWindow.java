package main.tp9.view;
import javax.swing.*;

/**
 * simple class for displaying information messages.
 *
 */
public class InfoWindow extends JOptionPane {
	/**
	 * Create the dialog.
	 */
	public InfoWindow(String message, JFrame parent) {
		showMessageDialog(parent, message, "Information", JOptionPane.INFORMATION_MESSAGE);
	}
	
	}
