package main.tp9.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;
import java.awt.GridBagConstraints;

/**
 * window to display loading text.
 *
 */
public class LoadingWindow extends JFrame {
	private JLabel lblAddingWordsTo;
	private JPanel contentPane;
	/**
	 * Create the frame.
	 *  @param isLoadingResults
	 *  true if the window is being used to display results, 
	 *  false if the window is being used to add words to dictionary
	 */

	public LoadingWindow(boolean isLoadingResults) {
		setResizable(false);
		setAlwaysOnTop(true);
		setTitle("Spelling checker");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 255, 77);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{221, 0};
		gbl_contentPane.rowHeights = new int[]{51, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		lblAddingWordsTo = new JLabel("Adding words to dictionary, please wait...");
		//lblAddingWordsTo = new JLabel("Adding words to dictionary, please wait...             ");
		GridBagConstraints gbc_lblAddingWordsTo = new GridBagConstraints();
		gbc_lblAddingWordsTo.anchor = GridBagConstraints.WEST;
		gbc_lblAddingWordsTo.gridx = 0;
		gbc_lblAddingWordsTo.gridy = 0;
		contentPane.add(lblAddingWordsTo, gbc_lblAddingWordsTo);
		setLocationRelativeTo(null);
		//pack();
		if(!isLoadingResults)updateLoadingText();
		else if(isLoadingResults)updateLoadingResultsText();
		setVisible(true);
	}

	/**
	 * swing's complicated way to give some movement to the loading screen
	 */
	protected void loadingWords() {
	        SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer>() {
	            @Override
	            protected Void doInBackground() throws Exception {
	                	int i=0;
	    				while(i>-1) {
	    					lblAddingWordsTo.setText("Adding words to dictionary, please wait.");
	    					//lblAddingWordsTo.setText("Adding words to dictionary, please wait.               ");
								Thread.sleep(500);
								lblAddingWordsTo.setText("Adding words to dictionary, please wait..");
								//lblAddingWordsTo.setText("Adding words to dictionary, please wait..              ");
								Thread.sleep(500);
								lblAddingWordsTo.setText("Adding words to dictionary, please wait...");
								//lblAddingWordsTo.setText("Adding words to dictionary, please wait...             ");
								Thread.sleep(500);
	                }
	                return null;
	            }

	            

	            @Override
	            protected void done() {
	                lblAddingWordsTo.setText("Done");
	                dispose();
	            }
	        };
	        worker.execute();
	    }
	 /**
	 * method to call if the window is being used to add words to dictionary
	 */
	public void updateLoadingText() {
		  SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	loadingWords();
		      }
		  });
		}
	/**
	 * swing's complicated way to give some movement to the loading screen
	 */
	protected void loadingResults() {
	        SwingWorker<Void, Integer> worker = new SwingWorker<Void, Integer>() {
	            @Override
	            protected Void doInBackground() throws Exception {
	                	int i=0;
	    				while(i>-1) {
	    					lblAddingWordsTo.setText("Parsing text, please wait.                             ");
								Thread.sleep(500);
	    					lblAddingWordsTo.setText("Parsing text, please wait..                            ");
								Thread.sleep(500);
	    					lblAddingWordsTo.setText("Parsing text, please wait...                           ");
								Thread.sleep(500);
	                }
	                return null;
	            }

	            

	            @Override
	            protected void done() {
	                lblAddingWordsTo.setText("Done");
	                dispose();
	            }
	        };
	        worker.execute();
	    }
	 /**
	 * method to call if the window is being used to parse text to show results
	 */
	public void updateLoadingResultsText() {
		  SwingUtilities.invokeLater(new Runnable() {
		    public void run() {
		    	loadingResults();
		      }
		  });
		}
}
