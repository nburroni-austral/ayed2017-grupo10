package main.tp9.view;
import javax.swing.*;

/**
 * simple class for displaying errors or exceptions.
 *
 */
public class ErrorWindow extends JOptionPane {
	/**
	 * Create the dialog.
	 */
	public ErrorWindow(String message, JFrame parent) {
		showMessageDialog(parent, message, "Error", JOptionPane.ERROR_MESSAGE);
	}
	}
