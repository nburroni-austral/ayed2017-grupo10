package main.tp9.view;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import javax.swing.JScrollPane;

public class MainWindow {
	private JTextArea textArea;
	private JPanel contentPane;
	private String text;
	private JButton btnCheckSpelling;
	/**
	 * Create the frame.
	 * @param displayText
	 * the text that will be the initial value displayed in the text field
	 * @param parent
	 * the JFrame from which this dialog was opened.
	 */
	public String getText(String displayText, JFrame parent) {
		JDialog dialog = new JDialog(parent);
		dialog.setTitle("Spelling checker");
        dialog.setResizable(false);
        dialog.setModal(true);
		dialog.setBounds(100, 100, 450, 300);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setLocationRelativeTo(parent);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		dialog.setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblWouldYouLike = new JLabel("Enter text to check for spelling mistakes: ");
		GridBagConstraints gbc_lblWouldYouLike = new GridBagConstraints();
		gbc_lblWouldYouLike.anchor = GridBagConstraints.WEST;
		gbc_lblWouldYouLike.gridwidth = 9;
		gbc_lblWouldYouLike.insets = new Insets(0, 0, 5, 0);
		gbc_lblWouldYouLike.gridx = 0;
		gbc_lblWouldYouLike.gridy = 0;
		contentPane.add(lblWouldYouLike, gbc_lblWouldYouLike);
		
		btnCheckSpelling = new JButton("                              Check!                               ");
		btnCheckSpelling.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton button = (JButton)e.getSource();
				text = textArea.getText();
				SwingUtilities.getWindowAncestor(button).dispose();
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 8;
		gbc_scrollPane.gridwidth = 9;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		textArea = new JTextArea();
		textArea.setRows(8);
		textArea.setLineWrap(true);
		textArea.setText(displayText);
		scrollPane.setViewportView(textArea);
		GridBagConstraints gbc_dictionaryStatusLabel = new GridBagConstraints();
		gbc_dictionaryStatusLabel.gridwidth = 5;
		gbc_dictionaryStatusLabel.insets = new Insets(0, 0, 0, 5);
		gbc_dictionaryStatusLabel.gridx = 1;
		gbc_dictionaryStatusLabel.gridy = 9;
		
		JButton btnClearText = new JButton("Clear text");
		btnClearText.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.setText("");
			}
		});
		GridBagConstraints gbc_btnClearText = new GridBagConstraints();
		gbc_btnClearText.insets = new Insets(0, 0, 0, 5);
		gbc_btnClearText.gridx = 0;
		gbc_btnClearText.gridy = 9;
		contentPane.add(btnClearText, gbc_btnClearText);
		GridBagConstraints gbc_btnCheckSpelling = new GridBagConstraints();
		gbc_btnCheckSpelling.gridwidth = 8;
		gbc_btnCheckSpelling.gridx = 1;
		gbc_btnCheckSpelling.gridy = 9;
		contentPane.add(btnCheckSpelling, gbc_btnCheckSpelling);
		dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
		
		return text;
	}
}
