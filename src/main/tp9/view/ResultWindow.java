package main.tp9.view;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.JList;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.event.ListSelectionListener;
import main.tp9.model.DictionaryEntry;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.AbstractAction;
import java.awt.event.ActionEvent;
import javax.swing.Action;

public class ResultWindow{
private DictionaryEntry wrongSpellingWords;
private DictionaryEntry[] suggestions;
private DefaultListModel<String>[] suggestionWordsListModel;
private DefaultListModel<String> wrongWordListModel;
	private JPanel contentPane;
	private JDialog dialog;
	private final Action action = new SwingAction();

	/**
	 * Create the frame.
	 * @param someWrongSpellingWords
	 * DictionaryEntry containing the words to be displayed
	 * @param someSuggestions
	 * DictionaryEntry array containing the suggestions for each word being displayed
	 * @param parent
	 * the JFrame from which this dialog was opened
	 */
	public ResultWindow(DictionaryEntry someWrongSpellingWords, DictionaryEntry[] someSuggestions, JFrame parent) {
		dialog = new JDialog(parent);
		dialog.setTitle("Results");
		dialog.setResizable(false);
		dialog.setModal(true);

		this.wrongSpellingWords=someWrongSpellingWords;
		this.suggestions=someSuggestions;
		suggestionWordsListModel=new DefaultListModel[suggestions.length];
		wrongWordListModel = new DefaultListModel<>();
		wrongSpellingWords.goTo(0);
		for (int i = 0; i < wrongSpellingWords.size(); i++) {
			wrongWordListModel.addElement(wrongSpellingWords.getActual());
			if(i<wrongSpellingWords.size()-1)wrongSpellingWords.goNext();
		}
		for (int i = 0; i < suggestions.length; i++) {
			suggestionWordsListModel[i]=new DefaultListModel<>();
			suggestions[i].goTo(0);
			for (int j = 0; j < suggestions[i].size(); j++) {
				suggestionWordsListModel[i].addElement(suggestions[i].getActual());
				if(j<suggestions[i].size()-1)suggestions[i].goNext();
			}
		}
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.setBounds(100, 100, 506, 352);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		dialog.setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{207, 212, 0};
		gbl_contentPane.rowHeights = new int[]{10, 251, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel lblWrongWords = new JLabel("Mispelled words: ");
		GridBagConstraints gbc_lblWrongWords = new GridBagConstraints();
		gbc_lblWrongWords.insets = new Insets(0, 0, 5, 5);
		gbc_lblWrongWords.gridx = 0;
		gbc_lblWrongWords.gridy = 0;
		contentPane.add(lblWrongWords, gbc_lblWrongWords);
		
		JLabel lblSuggestedWords = new JLabel("Suggested words:");
		GridBagConstraints gbc_lblSuggestedWords = new GridBagConstraints();
		gbc_lblSuggestedWords.insets = new Insets(0, 0, 5, 0);
		gbc_lblSuggestedWords.gridx = 1;
		gbc_lblSuggestedWords.gridy = 0;
		contentPane.add(lblSuggestedWords, gbc_lblSuggestedWords);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 1;
		contentPane.add(scrollPane_1, gbc_scrollPane_1);
		JList<String> wrongWordList = new JList<>(wrongWordListModel);
		JList<String> suggestionList = new JList<>(suggestionWordsListModel[0]);
		wrongWordList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPane_1.setViewportView(wrongWordList);
		wrongWordList.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent arg0) {
				suggestionList.setModel(suggestionWordsListModel[wrongWordList.getSelectedIndex()]);
			}
		});
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 1;
		contentPane.add(scrollPane, gbc_scrollPane);
		
		suggestionList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		suggestionList.setLayoutOrientation(JList.VERTICAL);
		scrollPane.setViewportView(suggestionList);
		
		JButton btnOk = new JButton("");
		btnOk.setAction(action);
		GridBagConstraints gbc_btnOk = new GridBagConstraints();
		gbc_btnOk.gridwidth = 2;
		gbc_btnOk.insets = new Insets(0, 0, 0, 5);
		gbc_btnOk.gridx = 0;
		gbc_btnOk.gridy = 2;
		contentPane.add(btnOk, gbc_btnOk);
		//dialog.pack();
		dialog.setLocationRelativeTo(parent);
		dialog.setVisible(true);
	}
	
	/**
	 * inner class to make the "ok" button close this window
	 *
	 */
	private class SwingAction extends AbstractAction {
		public SwingAction() {
			putValue(NAME, "                                                                       Ok                                                                       ");
			putValue(SHORT_DESCRIPTION, "Close this frame");
		}
		public void actionPerformed(ActionEvent e) {
			dialog.dispose();
		}
	}
}
