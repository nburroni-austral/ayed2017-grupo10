package main.tp9;

import javax.swing.UIManager;
import main.tp9.controller.MainController;

public class Main {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName()); //tell Swing to display GUI components according to the OS style
			new MainController();
		} catch (Exception e) { //need to handle .setLookAndFeel exceptions
			e.printStackTrace();
		}
    }
}
