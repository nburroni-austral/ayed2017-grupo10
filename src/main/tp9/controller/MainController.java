package main.tp9.controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.swing.JFrame;

import main.tp9.model.Dictionary;
import main.tp9.model.DictionaryEntry;
import main.tp9.view.ErrorWindow;
import main.tp9.view.InfoWindow;
import main.tp9.view.LoadingWindow;
import main.tp9.view.MainWindow;
import main.tp9.view.ResultWindow;

public class MainController {
	JFrame container;
	Dictionary dictionary;
	String text;
	public MainController() {
		container = new JFrame();
		container.setLocationRelativeTo(null);

		dictionary = new Dictionary();
	LoadingWindow loading = new LoadingWindow(false);
	String message = addWords()+" words added successfully";
	loading.dispose();
	container.setVisible(true);
	new InfoWindow(message, container);
	String displayText = "";
	while(true) {
		text=new MainWindow().getText(displayText, container);
		if (text==null) System.exit(0);
		displayText=text;
	while(text.trim().equals("")) {
		new ErrorWindow("Please enter at least a valid word", container);
		text = new MainWindow().getText(displayText, container);
		if (text==null) System.exit(0);
	}
	LoadingWindow secondLoading = new LoadingWindow(true);
	container.setVisible(false);
DictionaryEntry wordsToDisplay = checkSpelling(text);
	if(wordsToDisplay==null) {
		secondLoading.dispose();
		container.setVisible(true);
		new ErrorWindow("Please enter at least a valid word", container);
		continue;
	}
	DictionaryEntry[] suggestions = getSimilarWords(wordsToDisplay);
	secondLoading.dispose();
	container.setVisible(true);
	if(wordsToDisplay.size()==0) new InfoWindow("All words are correctly spelled.", container);
	else new ResultWindow(wordsToDisplay, suggestions, container);
	}
}
	/**
	 * add words to the dictionary
	 * @return
	 * the number of words that have been added.
	 */
	private int addWords() {
		  BufferedReader reader;
		try {
			reader = new BufferedReader(new FileReader("src/main/tp9/model/ReferenceText.txt"));
			reader.lines().forEach(o -> dictionary.addWordsFromText(o));
		} catch (FileNotFoundException e) {
			new ErrorWindow("Fatal error: "+e.getMessage(), container);
			System.exit(1);
		}
		return dictionary.getNumberOfWordsInDictionary();
	}
	/**
	 * method for getting all similar words in the dictionary, given a list of words
	 * @param words
	 * the list of words that need to be searched in the dictionary
	 * @return
	 * an array of DictionaryEntry with all the similar words to each word contained in "words"
	 */
	private DictionaryEntry[] getSimilarWords(DictionaryEntry words) {
		words.goTo(0);
		DictionaryEntry[] suggestions = new DictionaryEntry[words.size()];
		for (int i = 0; i < suggestions.length; i++) {
			if(words.getActual()!=null)suggestions[i]=dictionary.getSimilarWords(words.getActual());
			if(i<words.size()-1)words.goNext();
		}
		return suggestions;
	}
	/**
	 * checks a text searching for mispelled words (according to the words added to the dictionary)
	 * @param text
	 * a text that needs to be checked
	 * @return
	 * a DictionaryEntry containing all the mispelled words
	 */
	private DictionaryEntry checkSpelling(String text) {
		String[] words = text.split("\\W+");
		int invalidWords=0;
		DictionaryEntry wordsNotFound = new DictionaryEntry(); 
		for (int i = 0; i < words.length; i++) {
			if(dictionary.soundex(words[i])!=null){
			if(!dictionary.checkForWord(words[i]))wordsNotFound.insert(words[i]);
			}else invalidWords++;
		}
		if(invalidWords==words.length) return null;
		return wordsNotFound;
	}
}
