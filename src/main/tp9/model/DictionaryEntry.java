package main.tp9.model;

/**
 * class representing a group of similar sounding words
 *
 */
public class DictionaryEntry{
    private Node head, window, sentinel;
    private int size;
    public DictionaryEntry(){
        head = new Node();
        sentinel = new Node();
        head.next = sentinel;
        window = head;
        size = 0;
    }
    /**
     * get the word stored in the current index position
     * @return
     * the word stored in the current index position
     */
    public String getActual() {
        return window.obj;
    }

    /**
     * check if the entry is empty
     * @return
     * true if it is empty (has no words stored in it)
     */
    public boolean isVoid() {
        return head.next == sentinel;
    }

    /**
     * check if a word is added to this entry
     * @param word
     * the word to check if is added
     * @return
     * true if the word is added to this entry
     */
    public boolean isAdded(String word) {
    	if(size>0) {
    		goTo(0);
    	for (int i = 0; i < size; i++) {
    		if(window.obj.toLowerCase().equals(word.toLowerCase()))return true;
    		if(i!=size-1)goNext();
		}
    	}
    	return false;
    }

    /**
     * insert a word into this entry
     * @param word
     * @return
     * true if the word is added to this entry
     */
    public boolean insert(String word) {
    	if(!isAdded(word)) {
    	window.next = new Node(word, window.next);
        window = window.next;
        size++;
        return true;
        }return false;
    }

    /**
     * advance to the next position in the entry
     */
    public void goNext() {
        if(window.next == sentinel) throw new IndexOutOfBoundsException("Reached the end of this Entry");
        window = window.next;
    }

    /**
     * return to the previous position of the entry
     */
    public void goPrev() {
        if(window == head.next) throw new IndexOutOfBoundsException("Reached the beginning of this Entry");
        goBack(); }
    private void goBack(){
        Node aux = head;
        while(window != aux.next){
            aux = aux.next;
        }
        window = aux;
    }

    /**
     * go to a specific index position of the entry
     * @param index
     * the position to which to go
     */
    public void goTo(int index) {
        window = head.next;
        for(int i = 0; i < index; i++){
            window = window.next;
        }
    }

    /**
     * get the amount of words stored in this entry
     * @return
     * the amount of words stored in this entry
     */
    public int size() {
        return size;
    }

    private static class Node {
        String obj;
        Node next;
        Node() {
            obj = null;
            next = null; }
        Node(String o) {
            obj = o;
            next = null; }
        Node(String o, Node next) {
            this(o);
            this.next = next;
        }
    } }