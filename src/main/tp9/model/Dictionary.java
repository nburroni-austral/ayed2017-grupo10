package main.tp9.model;

public class Dictionary {
	/* Implements the mapping
	 * from: AEHIOUWYBFPVCGJKQSXZDTLMNR
	 * to:   00000000111122222222334556
	 */
	private static final char[] MAP = {
	  //A  B   C   D   E   F   G   H   I   J   K   L   M
	  '0','1','2','3','0','1','2','0','0','2','2','4','5',
	  //N  O   P   W   R   S   T   U   V   W   X   Y   Z
	  '5','0','1','2','6','2','3','0','1','0','2','0','2'
	};
	private DictionaryEntry[] words;
	private int tableSize;
	private double usedPlaces;
	private int numberOfWords;
public Dictionary() {
	words= new DictionaryEntry[13];
	tableSize = words.length;
	usedPlaces=0;
	numberOfWords = 0;
	for (int i = 0; i < words.length; i++) {
		words[i]=new DictionaryEntry();
	}
}



/**
 * add words to the dictionary from a given text
 * @param text
 * the text from which to add the words to the dictionary
 */
public void addWordsFromText(String text) {
	  String[] possibleWords = text.split("\\W+");
	 for (int i = 0; i < possibleWords.length; i++) {
		  if((usedPlaces/tableSize)>0.8) grow();
		  String code = soundex(possibleWords[i]);
		  if(code!=null) {
		  DictionaryEntry entry = words[hash(code)];
		  if(entry!=null);
			else {
				entry=new DictionaryEntry();
			}
		  if (entry.size()==0)usedPlaces++;
		  if(entry.insert(possibleWords[i].toLowerCase())) numberOfWords++;
					  }
		 	  }
	  }
/**
 * check if a given word exists in the dictionary
 * @param word
 * a word to check if it is added, it must be a valid "soundex" word
 * @return
 * true if the word exists in the dictionary
 */
public boolean checkForWord(String word) {
	if(soundex(word)==null) throw new IllegalArgumentException("Enter a valid word");
	DictionaryEntry entry = words[hash(soundex(word))];
	if (entry!=null)return entry.isAdded(word);
	return false;
}
/**
 * hash function to add words to the dictionary
 * @param soundex
 * the soundex code of the word being added to the dictionary
 * @return
 * the index in the table that that word should occuppy
 */
private int hash(String soundex) {
	StringBuffer result = new StringBuffer();
	result.append(Integer.toString((Character.getNumericValue(soundex.charAt(0))-10)*31));
	result.append(Integer.parseInt(soundex.substring(1)));
	return Integer.parseInt(result.toString())%tableSize;
}
/** Convert the given String to its Soundex code.
 * @return null If the given string can't be mapped to Soundex.
 */
public String soundex(String s) {

  // Algorithm works on uppercase (mainframe era).
  String t = s.toUpperCase();

  StringBuffer res = new StringBuffer();
  char c, prev = '?';

  // Main loop: find up to 4 chars that map.
  for (int i=0; i<t.length() && res.length() < 4 &&
    (c = t.charAt(i)) != ','; i++) {

    // Check to see if the given character is alphabetic.
    // Text is already converted to uppercase. Algorithm
    // only handles ASCII letters, do NOT use Character.isLetter()!
    // Also, skip double letters.
    if (c>='A' && c<='Z' && c != prev) {
      prev = c;

      // First char is installed unchanged, for sorting.
      if (i==0)
        res.append(c);
      else {
        char m = MAP[c-'A'];
        if (m != '0')
          res.append(m);
      }
    }
  }
  if (res.length() == 0)
    return null;
  for (int i=res.length(); i<4; i++)
    res.append('0');
  return res.toString();
}
/**
 * method to get the number of words that the dictionary has.
 * @return
 * the number of words currently in the dictionary
 */
public int getNumberOfWordsInDictionary() {
return numberOfWords;
}
public DictionaryEntry getSimilarWords(String word) {
	int wordLocation= hash(soundex(word));
	DictionaryEntry placeInTable = words[wordLocation];
	DictionaryEntry similarWords=new DictionaryEntry();
	placeInTable.goTo(0);
	for(int i=0; i<placeInTable.size(); i++) {
		if(soundex(word).equals(soundex(placeInTable.getActual()))) similarWords.insert(placeInTable.getActual());
		if(i!=placeInTable.size()-1)placeInTable.goNext();
	}return similarWords;
}
/**
 * method to check if a given number is prime
 * @param n
 * a number to be checked
 * @return
 * true if it is a prime number
 */
private boolean isPrime(int n) {
	 if (n == 1 || n == 2 || n == 3)
	 return true;
	 if (n % 2 == 0)
	 return false;
	 else {
	 int k = 3;
	 while (k <= Math.sqrt(n)) {
	 if (n % k == 0)
	 return false;
	 k = k +2;
	 }
	 }
	 return true;
	}
	/**
	 * method to get the next prime number given any number
	 * @param n
	 * a number
	 * @return
	 * the next prime number following that number
	 */
	private int nextPrime(int n) {
	 if (n % 2 == 0) n++; 
	 while (!isPrime(n)) n+=2;
	 return n;
	} 
	/**
	 * grow the size of the table
	 */
	private void grow() {
		tableSize = nextPrime(words.length+2);
		DictionaryEntry[] tempArray = new DictionaryEntry[tableSize];
		for (int i = 0; i < words.length; i++) {
			if(words[i]!=null) {
			for(int j=0; j<words[i].size(); j++) {
				words[i].goTo(j);
				String value = words[i].getActual(); 
				int place = hash(soundex(value));
			if(tempArray[place]!=null);
			else {
				tempArray[place]=new DictionaryEntry();
			}tempArray[place].insert(value);
			}
			}
			}
		words=tempArray;
	}
}
