package main.distancesbetweenwords;


public class LevenshteinDistance {
public static int calculateLevenshteinDistance(String aWord, String anotherWord) {
	int[][] distance = new int[aWord.length() + 1][anotherWord.length() + 1];
	for (int i = 0; i <= aWord.length(); i++) {                                 
    	distance[i][0] = i;
	}
	for (int j = 1; j <= anotherWord.length(); j++) {
    	distance[0][j] = j;
	}
    for (int i = 1; i <= aWord.length(); i++) {
    	for (int j = 1; j <= anotherWord.length(); j++)
        	distance[i][j] = Math.min(Math.min(distance[i - 1][j] + 1, distance[i][j - 1] + 1), distance[i - 1][j - 1] + ((aWord.charAt(i - 1) == anotherWord.charAt(j - 1)) ? 0 : 1));
    }
    	return distance[aWord.length()][anotherWord.length()];             
}
}
