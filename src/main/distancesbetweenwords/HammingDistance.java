package main.distancesbetweenwords;

public class HammingDistance {
public static int calculateHammingDistance (String aWord, String anotherWord) {
	String firstWord = aWord.toLowerCase().trim();
	String secondWord = anotherWord.toLowerCase().trim();
	if (firstWord.length() != secondWord.length()) throw new IllegalArgumentException("Both words need to have the same length");
	int distance = 0;
	for (int i=0; i<firstWord.length(); i++) {
		if (firstWord.charAt(i)!=secondWord.charAt(i)) distance++;
	}
	return distance;
}
}
