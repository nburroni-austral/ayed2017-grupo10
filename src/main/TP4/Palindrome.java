package main.TP4;

import struct.impl.Queue;
import struct.impl.Stack;

public class Palindrome {
    private Queue<Character> characterQueue;
    private Stack<Character> characterStack;

    public Palindrome(){
        characterStack = new Stack<>();
        characterQueue = new Queue<>();
    }

    public boolean checkPalindrome(String phrase){
        phrase = phrase.replaceAll("\\s","");
        phrase = phrase.toLowerCase().trim();
        for(int i = 0; i < phrase.length() ; i++){
            characterQueue.enqueue(phrase.charAt(i));
            characterStack.push(phrase.charAt(i));
        }

        while(!characterQueue.isEmpty() && !characterStack.isEmpty()){
            if(!characterQueue.dequeue().equals(characterStack.peek())){
                return false;
            }
            characterStack.pop();

        }
        return true;

    }

}
