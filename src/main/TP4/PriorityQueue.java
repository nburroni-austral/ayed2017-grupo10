package main.TP4;

import struct.impl.Queue;

public class PriorityQueue<T> {
    private Queue<T>[] queues;
    private int quantity;

    public PriorityQueue(int levelsOfPriority){
        queues = new Queue[levelsOfPriority];
        initializeQueues();
    }

    public void enqueue(T element, int priority){
        queues[priority-1].enqueue(element);
        quantity++;
    }

    public T dequeue(){
        for(int i=0; i<queues.length ; i++){
            if(!queues[i].isEmpty()){
                quantity--;
                return queues[i].dequeue();
            }
        }
        return null;
    }

    public boolean isEmpty(){
        return quantity == 0 ;
    }

    public int length(){
        return quantity;
    }

    public void empty(){
        for(int i = 0 ; i< queues.length ; i++){
            queues[i].empty();
        }
    }

    private void initializeQueues(){
        for(int i = 0; i<queues.length ; i++){
            queues[i] = new Queue<T>();
        }
    }


}
