package main.TP4.bank;

public interface Strategy {
public void enqueue(Client client);
public Client dequeue(int aCashier);
public int currentPeople();

}
