package main.TP4.bank;

import java.util.ArrayList;
import java.util.Random;

import org.hamcrest.core.Is;

public class Main {
	/**
	 * creates 3 cashiers and adds them to two banks with different strategies
	 * it then starts the simulation
	 * the unit of time in this simulation is seconds
	 *
	 * @throws InterruptedException
	 */

	public static void main(String[] args) throws InterruptedException {
		int minimumCustomers = 1;
		int maximumCustomers = 5;
		long elapsedSeconds = 0;
		Cashier firstCashier = new Cashier(30, 90);
		Cashier secondCashier = new Cashier(30, 120);
		Cashier thirdCashier = new Cashier(60, 150);
		ArrayList<Cashier> cashiers = new ArrayList<>();
		cashiers.add(firstCashier);
		cashiers.add(secondCashier);
		cashiers.add(thirdCashier);
		Bank firstBank = new Bank(new StrategyA(), cashiers);
		Bank secondBank = new Bank(new StrategyB(cashiers.size()), cashiers);
		while(elapsedSeconds<18000) {
			firstBank.updateTime();
			secondBank.updateTime();
			if(elapsedSeconds%90==0) {
				Random rand = new Random();
			    int newCustomers = rand.nextInt((maximumCustomers - minimumCustomers) + 1) + minimumCustomers;
			    for (int i=0; i<newCustomers; i++) {
			    	firstBank.enqueueClient(new Client());
			    	secondBank.enqueueClient(new Client());
			    }
			}
			firstBank.dequeueClient();
			secondBank.dequeueClient();
			elapsedSeconds++;
		}
		firstBank.closeBank();
		secondBank.closeBank();
		System.out.println("The bank implementing a single queue served "+firstBank.getServedClients()+" customers");
		System.out.println("The bank implementing multiple queues served "+secondBank.getServedClients()+" customers");
		System.out.println();
		if(firstBank.getServedClients()<secondBank.getServedClients()) System.out.println("The bank implementing multiple queues, in other words strategy B, is more effective.");
		else if(firstBank.getServedClients()>secondBank.getServedClients()) System.out.println("The bank implementing a single queue, in other words strategy A, is more effective.");
		else if(Math.abs(firstBank.getServedClients()-secondBank.getServedClients())<10) System.out.println("both strategies are equally effective"); //give them a margin of +- 10 people because of random numbers.
	}

}
