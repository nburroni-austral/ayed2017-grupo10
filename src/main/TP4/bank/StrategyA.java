package main.TP4.bank;

import java.util.Random;

import struct.impl.Queue;

public class StrategyA implements Strategy {
private Queue<Client> queue;

	public StrategyA() {
	queue = new Queue<>();
}

	/**
	 * if the queue is not too long it has a chance to enqueue the client
	 * @param client
	 * the client that you want to enqueue
	 */
	@Override

	public void enqueue(Client client) {
		Random randomGenerator = new Random();
		int isLeaving = 0;
		if (queue.length()<4) {
			isLeaving = 2;
		}else if(queue.length()>3 && queue.length()<9) {
			isLeaving = randomGenerator.nextInt(4 - 1 + 1) + 1;
		}else if(queue.length()>8) {
			isLeaving = randomGenerator.nextInt(2 - 1 + 1) + 1;
		}
		if(isLeaving!=1)queue.enqueue(client);
	}

	/**
	 * removes a client from the queue to be attended
	 * @param aCashier
	 * the cashier that will take care of the client
	 * @return
	 * the client that was removed from the queue
	 */

	@Override
	public Client dequeue(int aCashier) {
		return queue.dequeue();
	}

	/**
	 * Method to get the current people on the queue
	 * @return
	 * current people on the queue
	 */
	@Override
	public int currentPeople() {
		return queue.size();
	}

}
