package main.TP4.bank;

import java.util.Random;

public class Cashier {
	private long currentTime;
	private int minimumTime;
	private int maximumTime;
	private long nextAvailableTime;

	public Cashier(int minimumTime, int maximumTime) {
		currentTime=0;
		nextAvailableTime=0;
		this.minimumTime=minimumTime;
		this.maximumTime=maximumTime;
	}

	/**
	 * checks if the client was served
	 * @param client
	 * the client that  you want to check
	 * @return
	 * true if he was served, false if he was not served
	 */
	public boolean serveClient(Client client) {
		if(nextAvailableTime<=currentTime) {
			Random rand = new Random();
		    int waitTime = rand.nextInt((maximumTime - minimumTime) + 1) + minimumTime;
			nextAvailableTime=currentTime+waitTime;
			return true;
		}
		return false;
	}

	/**
	 * checks if the cashier is occupied with a client
	 * @return
	 * true if its occupied, false if its not occupied
	 */
	public boolean isOccupied() {
		 return nextAvailableTime>currentTime;
	}

	/**
	 * adds one unit of time
	 */
	public void tick() {
		currentTime++;
	}
}
