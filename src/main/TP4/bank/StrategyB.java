package main.TP4.bank;

import java.util.ArrayList;
import java.util.Random;

import struct.impl.Queue;

public class StrategyB implements Strategy {
	private ArrayList<Queue<Client>> queues;
	
	
	public StrategyB(int cashiers) {
		queues=new ArrayList<>();
		for(int i=0; i<cashiers; i++) {
			queues.add(new Queue<>());
		}
	}

	/**
	 * selects the shorter queue and if it's not long enough it has a chance to enqueue the client
	 * @param client
	 * the client that you want to enqueue
	 */
	@Override
	public void enqueue(Client client) {
		ArrayList<Queue<Client>> shortestQueues = new ArrayList<>();
		shortestQueues.add(queues.get(0));
		for (Queue<Client> queue : queues) {
			if(queue.length()==shortestQueues.get(0).length())shortestQueues.add(queue);
			else if (queue.length()<shortestQueues.get(0).length()) {
				shortestQueues.clear();
				shortestQueues.add(queue);
			}
		}
		Random randomGenerator = new Random();
		int selectedQueue = randomGenerator.nextInt(shortestQueues.size());
		int isLeaving = 0;
		if (shortestQueues.get(selectedQueue).length()<4) {
			isLeaving = 2;
		}else if(shortestQueues.get(selectedQueue).length()>3 && shortestQueues.get(selectedQueue).length()<9) {
			isLeaving = randomGenerator.nextInt(4 - 1 + 1) + 1;
		}else if(shortestQueues.get(selectedQueue).length()>8) {
			isLeaving = randomGenerator.nextInt(2 - 1 + 1) + 1;
		}
		if(isLeaving!=1)shortestQueues.get(selectedQueue).enqueue(client);
	}

	/**
	 *removes a client from the queue
	 * @param aCashier
	 * the cashier that will take care of the client
	 * @return
	 * the client that is removed from the queue
	 */
	@Override
	public Client dequeue(int aCashier) {
		if(aCashier<1 || aCashier>queues.size()) throw new IllegalArgumentException("invalid cashier");
		return queues.get(aCashier-1).dequeue();
	}

	/**
	 * method to get the current people on the queues
	 * @return
	 * current people on the queues
	 */
	@Override
	public int currentPeople() {
		int people =0;
		for (Queue<Client> queue : queues) {
			people+=queue.length();
		}
		return people;
	}

}
