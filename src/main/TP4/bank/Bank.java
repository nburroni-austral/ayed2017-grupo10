package main.TP4.bank;

import java.util.ArrayList;

public class Bank {
	private int servedClients;
	private ArrayList<Cashier> cashiers;
	private Strategy strategy;
	public Bank(Strategy aStrategy, ArrayList<Cashier> cashiers) {
		this.cashiers = cashiers;
		this.strategy = aStrategy;
		servedClients = 0;
	}

	/**
	 * Updates the time.
	 * Adds 1 unit of time to the current time.
	 */
	public void updateTime() {
		for (Cashier cashier : cashiers) cashier.tick();
	}
	public void enqueueClient(Client client) {
		strategy.enqueue(client);
	}
	public void dequeueClient() {
		if(getCurrentPeople()>0) {
		for (Cashier cashier : cashiers) {
			if(!cashier.isOccupied()){
				cashier.serveClient(strategy.dequeue(cashiers.indexOf(cashier)+1));
				servedClients++;
				break;
			}
		}
		}
	}

	/**
	 *closes the bank
	 * @return
	 * true if there is no more clients inside, false if there still are clients being attended
	 */
	public boolean closeBank(){
		if(getCurrentPeople()==0) return true;
		dequeueClient();
		updateTime();
		return closeBank();
	}

	/**
	 * method for getting the amount of served clients
	 * @return
	 * the amount of served clients
	 */
	public int getServedClients () {
		return servedClients;
	}

	/**
	 * method for getting the amount of people in the bank
	 * @return
	 * current people on the bank
	 */
	public int getCurrentPeople() {
		return strategy.currentPeople();
	}
}
