package main.tp5;

import struct.impl.List;

public class Cashier {
	private long currentTime;
	private static int waitTime = 10; //this is the time it takes for a cashier to serve a customer, all cashiers take the same time.
	//private long nextAvailableTime;
	private int servedCustomers;
	private long emptyTime;
	private boolean isOccupied;
	private Long occupiedTime;
	private List<Long> allTimes;
	public Cashier() {
		emptyTime=0;
		servedCustomers = 0;
		currentTime=0;
		//nextAvailableTime=0;
		isOccupied=false;
		occupiedTime = Long.valueOf(0);
		allTimes=new List<>();
	}

	/**
	 * attempts to serve a client
	 * @param client
	 * the client that  you want to serve
	 * @return
	 * true if he was served, false if he was not served
	 */
	public boolean serveClient(Client client) {
		 if(!isOccupied) {
					isOccupied=true;
					return true;
				}
		return false;
	}

	/**
	 * checks if the cashier is occupied with a client
	 * @return
	 * true if its occupied, false if its not occupied
	 */
	public boolean isOccupied() {
		 return isOccupied;
	}

	public void checkIfCustomerWasFinishedGettingserved() {
		if(isOccupied) {
			 double chanceOfCustomerBeingServed = Math.random();
				if(chanceOfCustomerBeingServed<=0.3) {
					isOccupied=false;
					allTimes.insertNext(occupiedTime);
					occupiedTime=(long) 0;
					servedCustomers++;
				}
				}
	}
	/**
	 * adds one unit of time, also checks if customer has finished being served
	 */
	public void tick() {
		if(currentTime%waitTime==0)	checkIfCustomerWasFinishedGettingserved();
		if(isOccupied)occupiedTime++;
		else emptyTime++;
		currentTime++;
	}
	public long getMeanCustomerTime() {
		if(allTimes.size()==0) return 0;
		long result = 0;
		for(int i=0; i<allTimes.size(); i++) {
			allTimes.goTo(i);
			result+=allTimes.getActual();
		}
		result = result/allTimes.size();
		return result;
	}
	public int getServedCustomers() {
		return servedCustomers;
	}
	public long getEmptyTime() {
		return emptyTime;
	}
}