package main.tp5;

import java.util.Scanner;

public class Main {
	/**
	 * creates 3 cashiers and adds them to a train system
	 * it then starts the simulation
	 * the unit of time in this simulation is seconds
	 *
	 */

	public static void main(String[] args) {
		
		int numberOfCashiers = 0;
		while(numberOfCashiers<3 || numberOfCashiers>10){
			
			try {
			System.out.print("Enter number of cashiers(between 3 and 10): ");
			Scanner sc = new Scanner(System.in);
			numberOfCashiers = sc.nextInt();
		} catch (Exception e) {
			System.out.println("enter a valid number.");
		}
			}
		int numberOfCustomers = 10;
		long elapsedSeconds = 0;
		TrainSystem system = new TrainSystem(numberOfCashiers);
		while(elapsedSeconds<57600) {
			system.updateTime();
			if(elapsedSeconds%10==0 && elapsedSeconds<57571) {
			   for (int i=0; i<numberOfCustomers; i++) {
			    	system.enqueueClient(new Client());
			    }
			}
			system.dequeueClient();
			elapsedSeconds++;
		}
		system.closeTrainSystem();
		system.printStatistics();
	}

}
