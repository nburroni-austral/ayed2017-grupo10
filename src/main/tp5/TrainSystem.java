package main.tp5;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import struct.impl.*;

public class TrainSystem {
	private double ticketPrice;
private List<Cashier> cashiers;
private List<Queue<Client>> queues;
private List<Stack<Long>> waitTimes;
private long[] actualWaitTimes;
private Random rand;
public TrainSystem(int numberOfCashiers) {
	ticketPrice=0.7;
	rand = new Random();
	cashiers = new List<>();
	queues = new List<>();
	waitTimes=new List<>();
	actualWaitTimes= new long[numberOfCashiers];
	for(int i=0; i< numberOfCashiers; i++) {
		cashiers.insertNext(new Cashier());
		queues.insertNext(new Queue<>());
		waitTimes.insertNext(new Stack<>());
	}
}
public void enqueueClient(Client client) {
	int selectedQueue = rand.nextInt((queues.size()));
	queues.goTo(selectedQueue);
	queues.getActual().enqueue(client);
}
public void dequeueClient() {
    for (int i=0; i<cashiers.size(); i++) {
    	actualWaitTimes[i]++;
    cashiers.goTo(i);
    if(!cashiers.getActual().isOccupied()) {
    	queues.goTo(i);
    Client client = queues.getActual().dequeue();
    boolean wasServed = cashiers.getActual().serveClient(client);
    if (!wasServed) queues.getActual().enqueue(client);
    else {
    	waitTimes.goTo(i);
    	waitTimes.getActual().push(actualWaitTimes[i]);
    	actualWaitTimes[i]=0;
    }
    }
    }
}
public void closeTrainSystem() {
	for(int i=0; i<queues.size(); i++) {
		queues.goTo(i);

	while(!queues.getActual().isEmpty()) {
		for (int j = 0; j < cashiers.size(); j++) {
			cashiers.goTo(j);
			cashiers.getActual().checkIfCustomerWasFinishedGettingserved();
		}
		dequeueClient();
	}
	}
}
public void updateTime() {
	for(int i=0; i<cashiers.size(); i++) {
		cashiers.goTo(i);
		cashiers.getActual().tick();
	}
	}
	private int getServedClients(){
		int servedClients=0;
		for(int i=0; i<cashiers.size(); i++) {
			cashiers.goTo(i);
			servedClients+=cashiers.getActual().getServedCustomers();
		}
		return servedClients;
	}
	private double round(double value, int places) {
	    if (places < 0) throw new IllegalArgumentException();

	    BigDecimal bd = new BigDecimal(value);
	    bd = bd.setScale(places, RoundingMode.DOWN);
	    return bd.doubleValue();
	}
	private String getPrintableTime(long time) {
		Double aux = (double) 0;
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		aux = (double) (time/3600);
		hours=aux.intValue();
		aux = (double) ((time-hours*3600)/60);
		minutes = aux.intValue();
		aux=(double) (time-(hours*3600)-(minutes*60));
		seconds=aux.intValue();
		return (hours+" hours, "+minutes+" minutes, "+seconds+" seconds");
	}
	public long getAverageWaitTimeInQueue(int queue){
		waitTimes.goTo(queue);
		long totalTimes = waitTimes.getActual().size();
		if(waitTimes.getActual().isEmpty()) return 0;
		long result = 0;
		while(!waitTimes.getActual().isEmpty()) {
			result+=waitTimes.getActual().peek();
			waitTimes.getActual().pop();
		}
		result = result/totalTimes;
		return result;
	}
	public void printStatistics() {
		System.out.println("The system served a total of "+getServedClients()+" customers");
		for(int i=0; i<cashiers.size(); i++) {
			cashiers.goTo(i);
			System.out.println("Cashier number "+(i+1)+":");
			System.out.println("               served a total of "+cashiers.getActual().getServedCustomers()+" customers");
			System.out.println("               raised a total of $"+round(cashiers.getActual().getServedCustomers()*ticketPrice, 2));
			System.out.println("               was empty for "+getPrintableTime(cashiers.getActual().getEmptyTime()));
			System.out.println("               took an average time of "+getPrintableTime(cashiers.getActual().getMeanCustomerTime())+" serving each customer");
			System.out.println("               each customer waited an average of "+getPrintableTime(getAverageWaitTimeInQueue(i))+" to get served");
		}
	}
}
