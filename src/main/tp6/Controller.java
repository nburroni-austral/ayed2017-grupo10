package main.tp6;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import struct.impl.List;

public class Controller {
	Scanner sc;
private BusSystem buses;

public Controller() {
	buses = new BusSystem();
}
public void displayMenu() {
	int selectedOption = -1;
	while(selectedOption<0 || selectedOption>8){
		try {
	        System.out.println("\033[H\033[2J"); //clears console in windows powershell but not in cmd
	        System.out.flush();
		System.out.println("1. Add bus");
		System.out.println("2. Remove bus");
		System.out.println("3. Get report from all buses");
		System.out.println("4. Get buses for handicapped people in each line");
		System.out.println("5. Get buses with more than 27 seats in each line");
		System.out.println("6. Save data to a file");
		System.out.println("7. Get data from file");
		System.out.println("0. Exit");
		System.out.print("Enter option: ");
		sc = new Scanner(System.in);
		selectedOption = sc.nextInt();
	} catch (Exception e) {
		System.out.println("enter a valid number.");
	}
		}
	switch (selectedOption) {
	case 1:
		addBus();
		break;
	case 2:
		removeBus();
		break;
	case 3:
		getReport();
		break;
	case 4:
		getHandicappedBuses();
		break;
	case 5:
		getLargeBuses();
		break;
	case 6:
		saveToFile();
		break;
	case 7:
		readFromFile();
		break;
	case 0:
		System.exit(0);
	default:
		break;
	}displayMenu();
}
private void addBus() {
	Integer busLine=null;
	Integer busInternal=null;
	Integer seatQuantity=null;
	boolean isHandicapped=false;
	while (busLine==null || busLine<1) {
	System.out.print("Enter bus line (or -1 to return to main menu): ");
	try {
		sc = new Scanner(System.in);
		busLine = sc.nextInt();
		if(busLine==-1) return;
	} catch (Exception e) {
		System.out.println("Just positive numbers please");
continue;
	}
	}
	while (busInternal==null || busInternal<1) {
		System.out.print("Enter bus internal number: ");
		try {
			sc = new Scanner(System.in);
			busInternal = sc.nextInt();
		} catch (Exception e) {
			System.out.println("Just positive numbers please");
	continue;
		}
		}
	while (seatQuantity==null || seatQuantity<1) {
		System.out.print("Enter bus seat quantity: ");
		try {
			sc = new Scanner(System.in);
			seatQuantity = sc.nextInt();
		} catch (Exception e) {
			System.out.println("Just positive numbers please");
	continue;
		}
		}

	int rawInput=12;
	while(rawInput!=0 && rawInput!=1) {
	System.out.print("Can the bus carry handicapped people? (0 or 1)");
	try {
		sc = new Scanner(System.in);
		rawInput = sc.nextInt();
	} catch (Exception e) {
		System.out.println("Just numbers please");
continue;
	}
	}
	isHandicapped = 1==rawInput;
	buses.addBus(new Bus(busLine, busInternal, seatQuantity, isHandicapped));
	System.out.println("Bus added successfully");
	System.out.println("Press enter key to continue...");
    try
    { System.in.read();}  
    catch(Exception e){}
}
private void removeBus() {
	Integer busLine=null;
	Integer busInternal=null;
	while (busLine==null || busLine<1) {
	System.out.print("Enter bus line (or -1 to return to main menu): ");
	try {
		sc = new Scanner(System.in);
		busLine = sc.nextInt();
		if(busLine==-1) return;
	} catch (Exception e) {
		System.out.println("Just positive numbers please");
continue;
	}
	}
	while (busInternal==null|| busInternal<1) {
		System.out.print("Enter bus internal number: ");
		try {
			sc = new Scanner(System.in);
			busInternal = sc.nextInt();
		} catch (Exception e) {
			System.out.println("Just positive numbers please");
	continue;
		}
		}
	if(!buses.deleteBus(new Bus(busLine, busInternal, 0, false))) {
		System.out.println("Bus not found");
		removeBus();
	}else {
		System.out.println("Bus removed successfully");
		System.out.println("Press enter key to continue...");
	    try
	    { System.in.read();}  
	    catch(Exception e){}
	}
}
private void getReport() {
	buses.printReport();
	System.out.println("Press enter key to continue...");
    try
    { System.in.read();}  
    catch(Exception e){}  
}
private void getHandicappedBuses() {
	buses.printHandicappedBusNumber();
	System.out.println("Press enter key to continue...");
    try
    { System.in.read();}  
    catch(Exception e){}
}
private void getLargeBuses() {
	buses.printLargeBuses();
	System.out.println("Press enter key to continue...");
    try
    { System.in.read();}  
    catch(Exception e){}
}
private void readFromFile() {
	List<String> readLines = new List<>();
	BusSystem newBuses = new BusSystem();
	try {
		System.out.print("enter file name (leave blank to return to main menu): ");
		sc = new Scanner(System.in);
		String path = sc.nextLine();
		if(path.equals("")) return;
	BufferedReader reader = new BufferedReader(new FileReader(path+".bus"));
		reader.lines().forEach(o -> readLines.insertNext(o));
		 reader.close();
	}catch (FileNotFoundException e) {
		System.out.println("file not found.");
		return;
	}
	catch (Exception e) {
		return;
	}
	try {
		for(int i=0; i<readLines.size(); i++) {
			readLines.goTo(i);
			String[] fields = readLines.getActual().split(",\\s+");
			newBuses.addBus(new Bus(Integer.valueOf(fields[0].substring(1)), Integer.valueOf(fields[1]), Integer.valueOf(fields[2]), Boolean.valueOf(fields[3].substring(0, fields[3].length() - 1))));
		}
	} catch (Exception e) {
		System.out.println("invalid file.");
		return;
	}
	buses=newBuses;
	System.out.println("Retrieved data from file successfully");
	System.out.println("Press enter key to continue...");
    try
    { System.in.read();}  
    catch(Exception e){}
}
private void saveToFile() {
	BufferedWriter outputWriter = null;
	String[][] info = buses.getReport();
	  try {
			System.out.print("enter file name (leave blank to return to main menu): ");
			sc = new Scanner(System.in);
			String path = sc.nextLine();
			if(path.equals("")) return;
		outputWriter = new BufferedWriter(new FileWriter(path+".bus"));
		  for (int i = 1; i < info.length; i++) {
		    outputWriter.write(Arrays.toString(info[i]));
		    outputWriter.newLine();
		  }
		  outputWriter.flush();  
		  outputWriter.close();
	} catch (IOException e) {
		System.out.println("Error while writing to file.");
		return;
	} 
		System.out.println("Saved data to file successfully");
		System.out.println("Press enter key to continue...");
	    try
	    { System.in.read();}  
	    catch(Exception e){}
}
}
