package main.tp6;

import java.io.Serializable;

import struct.impl.SortedList;
import sun.net.www.content.text.plain;

public class BusSystem implements Serializable{
private SortedList<Bus> buses;
public BusSystem() {
	buses = new SortedList<>();
}
public void addBus (Bus aBus) {
	if(!containsBus(aBus))buses.insert(aBus);
}
public boolean deleteBus(Bus aBus) {
	for(int i=0; i<buses.size(); i++) {
		buses.goTo(i);
		if(buses.getActual().compareTo(aBus)>0) break;
		if(buses.getActual().equals(aBus)) {
			buses.remove();
			return true;
		}
	}
	return false;
}

public boolean containsBus(Bus aBus) {
	for(int i=0; i<buses.size(); i++) {
		buses.goTo(i);
		if(buses.getActual().equals(aBus)) return true;
		if(buses.getActual().compareTo(aBus)>0) return false;
	}
	return false;
}
public void printLargeBuses() {
	printSomething("large buses");
}
private void printSomething(String whatToPrint) {
	int i=0;
	buses.goTo(0);
	while (i<buses.size()) {
		int numberOfLargeBuses=0;
		int numberOfHandicappedBuses=0;
		int lineNumber=buses.getActual().getLineNumber();
		while((buses.getActual().getLineNumber()==lineNumber)) {
		if(buses.getActual().getSeatQuantity()>27) {
			numberOfLargeBuses++;
		}
		if(buses.getActual().isForHandicapped()) {
			numberOfHandicappedBuses++;
		}i++;
		try {
			buses.goNext();
		} catch (IndexOutOfBoundsException e) {
break;
		}
	}
		if(whatToPrint.equals("handicapped buses")) System.out.println("Bus line "+lineNumber+" has "+numberOfHandicappedBuses+" buses for handicapped people.");
		if(whatToPrint.equals("large buses")) System.out.println("Bus line "+lineNumber+" has "+numberOfLargeBuses+" buses whith more than 27 seats.");
		}
}



public void printHandicappedBusNumber() {
	printSomething("handicapped buses");
	
}
public void printReport() {
	getReport(true);
}
public String[][] getReport(){
	return getReport(false);
}
private String[][] getReport(boolean print) {
	String[][] table = new String[buses.size()+1][4];
	table[0]= new String[]{"Line number", "Internal Number", "Number of seats", "Admits handicapped people"};
	for(int i=0; i<buses.size(); i++) {
		buses.goTo(i);
		for(int j=0; j<4; j++) {
			switch (j) {
				case 0:
					table[i+1][j]=String.valueOf(buses.getActual().getLineNumber());
					break;
				case 1:
					table[i+1][j]=String.valueOf(buses.getActual().getInternalNumber());
					break;
				case 2:
					table[i+1][j]=String.valueOf(buses.getActual().getSeatQuantity());
					break;
				case 3:
					table[i+1][j]=String.valueOf(buses.getActual().isForHandicapped());
					break;
				default:
					break;
			}
		}
	}
	
	if(print)for (Object[] row : table) System.out.format("%-25s%-25s%-25s%-25s\n", row);
	return table;
}
}
