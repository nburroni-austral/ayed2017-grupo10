package main.tp6;

public class Bus implements Comparable<Bus> {
private int lineNumber;
private int internalNumber;
private int seatQuantity;
private boolean isForHandicapped;
public Bus(int lineNumber, int internalNumber, int seatQuantity, boolean isForHandicapped) {
	this.lineNumber = lineNumber;
	this.internalNumber = internalNumber;
	this.seatQuantity = seatQuantity;
	this.isForHandicapped = isForHandicapped;
}
public int getLineNumber() {
	return lineNumber;
}
public int getInternalNumber() {
	return internalNumber;
}
public int getSeatQuantity() {
	return seatQuantity;
}
public boolean isForHandicapped() {
	return isForHandicapped;
}

@Override
public int compareTo(Bus o) {
	if(lineNumber==o.getLineNumber()) return Integer.valueOf(internalNumber).compareTo(o.getInternalNumber());
	else return Integer.valueOf(lineNumber).compareTo(o.lineNumber);
}

@Override
public boolean equals(Object o) {
    if ( this == o ) return true;
    if (!(o instanceof Bus)) return false; //this also checks for null
    Bus aBus = (Bus)o; //cast to Bus is now safe
    return (this.lineNumber==aBus.getLineNumber() && this.internalNumber==aBus.getInternalNumber());
    
}

}
