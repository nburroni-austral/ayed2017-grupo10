package main.tp3.LexicographicAnalyzer;
import struct.impl.Stack;
import java.io.*;
import java.util.Iterator;



public class LexicographicAnalyzer {
    private static Stack<String> linesOfCode;
    int numberOfKeys;
    int numberOfParenthesis;
    int numberOfBrackets;

    public LexicographicAnalyzer() {
        linesOfCode = new Stack<>();
        numberOfKeys = 0;
        numberOfParenthesis = 0;
        numberOfBrackets = 0;

    }

    /**
     * Separates the file given into a String for each line and then stores each line in the stack
     *
     * @throws IOException
     */
    public void storeTextFile() throws IOException {
    	/*linesOfCode.push("{{{{{{{{");
    	linesOfCode.push("sdfsd");
    	linesOfCode.push("[)(9{");
    	linesOfCode.push("{");
    	linesOfCode.push("{");
    	linesOfCode.push("*(*(((");
    	linesOfCode.push("ascdasc");
    	*/

        BufferedReader reader = new BufferedReader(new FileReader("src/main/tp3/LexicographicAnalyzer/file.txt"));
        try {
        	reader.lines().forEach(o -> linesOfCode.push(o));
        } finally {
            reader.close();
        }
    }

    /**
     * analyzes if the keys are balanced
     * @return
     * 0 if the keys are balanced
     * <0 if there are opening keys missing
     * >0 if there are closing keys missing
     */
    public int analyzeKeys() {
    	int size = linesOfCode.size();
    	Stack<String> tempLinesOfCode=new Stack<>();
        for (int i = 0; i < size; i++) {
        	char[] line = linesOfCode.peek().toCharArray();
            for (int j = 0; j < line.length; j++) {
            	if (line[j]=='{') {
                    numberOfKeys += 1;
                }
                if (line[j]=='}') {
                    numberOfKeys -= 1;
                }
			}
        	tempLinesOfCode.push(linesOfCode.peek());
            linesOfCode.pop();

        }
        for(int i= 0; i<size; i++) {
        	linesOfCode.push(tempLinesOfCode.peek());
        	tempLinesOfCode.pop();
        }
        return numberOfKeys;
    }

    /**
     * analyzes if the parenthesis are balanced
     * @return
     * 0 if the parenthesis are balanced
     * <0 if there are opening parenthesis missing
     * >0 if there are closing parenthesis missing
     */
    public int analyzeParenthesis(){
    	int size = linesOfCode.size();
    	Stack<String> tempLinesOfCode=new Stack<>();
        for (int i = 0; i < size; i++) {
        	char[] line = linesOfCode.peek().toCharArray();
            for (int j = 0; j < line.length; j++) {
            	if (line[j]=='(') {
                    numberOfParenthesis += 1;
                }
                if (line[j]==')') {
                    numberOfParenthesis -= 1;
                }
			}
        	tempLinesOfCode.push(linesOfCode.peek());
            linesOfCode.pop();

        }
        for(int i= 0; i<size; i++) {
        	linesOfCode.push(tempLinesOfCode.peek());
        	tempLinesOfCode.pop();
        }
        return numberOfParenthesis;
    }

    /**
     * analyzes if the brackets are balanced
     * @return
     * 0 if the brackets are balanced
     * <0 if there are opening brackets missing
     * >0 if there are closing brackets missing
     */
    public int analyzeBrackets(){
    	int size = linesOfCode.size();
    	Stack<String> tempLinesOfCode=new Stack<>();
    	for (int i = 0; i < size; i++) {
    	char[] line = linesOfCode.peek().toCharArray();
        for (int j = 0; j < line.length; j++) {
        	if (line[j]=='[') {
                numberOfBrackets += 1;
            }
            if (line[j]==']') {
                numberOfBrackets -= 1;
            }
		}
    	tempLinesOfCode.push(linesOfCode.peek());
        linesOfCode.pop();

    }
    	for(int i= 0; i<size; i++) {
        	linesOfCode.push(tempLinesOfCode.peek());
        	tempLinesOfCode.pop();
        }
    return numberOfBrackets;
    }




}
