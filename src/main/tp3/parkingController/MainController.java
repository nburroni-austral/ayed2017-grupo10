package main.tp3.parkingController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.tp3.parking.Car;
import main.tp3.parkingModel.Model;
import main.tp3.parkingView.CreateACarWindow;
import main.tp3.parkingView.EnterPlateWindow;
import main.tp3.parkingView.ErrorWindow;
import main.tp3.parkingView.MainWindow;

public class MainController {
		private Model actions;
		private MainWindow display;
	public MainController() {
		actions =new Model();
		display = new MainWindow();
		display.addParkCarListener(new ParkCarListener());
		display.addRemoveCarListener(new RemoveCarListener());
		display.addExitListener(new ExitListener());
		display.setVisible(true);
	}
	/**
	 * Listener class for exit button
	 *
	 */
	class ExitListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent buttonPressed) {
			new ErrorWindow("today's total earnings are: $"+actions.getEarnings(), display);
			System.exit(0);
		}
	}
	/**
	 * Listener class for park car button
	 *
	 */
	class ParkCarListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent buttonPressed) {
			Car aCar = new CreateACarWindow().showCreateACarWindow(display);
			if(aCar!=null) {
				if(actions.parkCar(aCar)==false) new ErrorWindow("Parking lot is full", display);
				display.updateNumberOfParkedCars(actions.getNumberOfParkedCars());
			}
		}
	}
	/**
	 * Listener class for remove car button
	 *
	 */
	class RemoveCarListener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent buttonPressed) {
			String parkCarPlate = new EnterPlateWindow().showEnterPlateWindow(display);
			if(!parkCarPlate.equals("")) {
				try {
					actions.removeCar(parkCarPlate);
					display.updateNumberOfParkedCars(actions.getNumberOfParkedCars());
				} catch (NullPointerException e) {
					new ErrorWindow(e.getMessage(), display);
				}
			}
		}
	}
}
