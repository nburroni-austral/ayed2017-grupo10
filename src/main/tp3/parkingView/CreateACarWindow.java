package main.tp3.parkingView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import main.tp3.parking.Car;

import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;
/**
 * window to allow the user to input a set of parameters in order to create a car.
 *
 */
public class CreateACarWindow {

        private String license = "";
        private String color = "";
        private String model = "";
        private String brand = "";
        Car returnCar;
        private JTextField colorInput;
        private JTextField modelInput;
        private JTextField brandInput;
        private JTextField licenseInput;
        /**
         * method for creating this window
         * @param parent
         * the JFrame that created this window (for centering purposes)
         * @return
         * a Car with the parameters entered by the user
         */
        public Car showCreateACarWindow(JFrame parent) {

            JPanel content = new JPanel();
            content.setBorder(new EmptyBorder(5, 5, 10, 10));

            JDialog dialog = new JDialog();
            dialog.setAlwaysOnTop(true);
            dialog.setResizable(false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setModal(true);
            dialog.setTitle("Parking lot");
            dialog.getContentPane().add(content);
            GridBagLayout gbl_content = new GridBagLayout();
            gbl_content.columnWidths = new int[]{113, 65, 45, 0};
            gbl_content.rowHeights = new int[]{14, 0, 0, 0, 23, 0};
            gbl_content.columnWeights = new double[]{0.0, 1.0, 0.0, Double.MIN_VALUE};
            gbl_content.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
            content.setLayout(gbl_content);
            
            JLabel lblEnterCarLicense = new JLabel("enter car license plate: ");
            GridBagConstraints gbc_lblEnterCarLicense = new GridBagConstraints();
            gbc_lblEnterCarLicense.anchor = GridBagConstraints.EAST;
            gbc_lblEnterCarLicense.insets = new Insets(0, 0, 5, 5);
            gbc_lblEnterCarLicense.gridx = 0;
            gbc_lblEnterCarLicense.gridy = 0;
            content.add(lblEnterCarLicense, gbc_lblEnterCarLicense);
            
            licenseInput = new JTextField();
            licenseInput.setColumns(10);
            GridBagConstraints gbc_licenseInput = new GridBagConstraints();
            gbc_licenseInput.gridwidth = 2;
            gbc_licenseInput.insets = new Insets(0, 0, 5, 0);
            gbc_licenseInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_licenseInput.gridx = 1;
            gbc_licenseInput.gridy = 0;
            content.add(licenseInput, gbc_licenseInput);
            
            JLabel lblEnterCarColor = new JLabel("Enter car color: ");
            GridBagConstraints gbc_lblEnterCarColor = new GridBagConstraints();
            gbc_lblEnterCarColor.anchor = GridBagConstraints.EAST;
            gbc_lblEnterCarColor.insets = new Insets(0, 0, 5, 5);
            gbc_lblEnterCarColor.gridx = 0;
            gbc_lblEnterCarColor.gridy = 1;
            content.add(lblEnterCarColor, gbc_lblEnterCarColor);
            
            colorInput = new JTextField();
            colorInput.setColumns(10);
            GridBagConstraints gbc_colorInput = new GridBagConstraints();
            gbc_colorInput.gridwidth = 2;
            gbc_colorInput.insets = new Insets(0, 0, 5, 0);
            gbc_colorInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_colorInput.gridx = 1;
            gbc_colorInput.gridy = 1;
            content.add(colorInput, gbc_colorInput);
            
            JLabel lblEnterCarModel = new JLabel("Enter car model: ");
            GridBagConstraints gbc_lblEnterCarModel = new GridBagConstraints();
            gbc_lblEnterCarModel.anchor = GridBagConstraints.EAST;
            gbc_lblEnterCarModel.insets = new Insets(0, 0, 5, 5);
            gbc_lblEnterCarModel.gridx = 0;
            gbc_lblEnterCarModel.gridy = 2;
            content.add(lblEnterCarModel, gbc_lblEnterCarModel);
            
            modelInput = new JTextField();
            modelInput.setColumns(10);
            GridBagConstraints gbc_modelInput = new GridBagConstraints();
            gbc_modelInput.gridwidth = 2;
            gbc_modelInput.insets = new Insets(0, 0, 5, 0);
            gbc_modelInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_modelInput.gridx = 1;
            gbc_modelInput.gridy = 2;
            content.add(modelInput, gbc_modelInput);
            
            JLabel lblEnterCarBrand = new JLabel("Enter car brand: ");
            GridBagConstraints gbc_lblEnterCarBrand = new GridBagConstraints();
            gbc_lblEnterCarBrand.anchor = GridBagConstraints.EAST;
            gbc_lblEnterCarBrand.insets = new Insets(0, 0, 5, 5);
            gbc_lblEnterCarBrand.gridx = 0;
            gbc_lblEnterCarBrand.gridy = 3;
            content.add(lblEnterCarBrand, gbc_lblEnterCarBrand);
            
            brandInput = new JTextField();
            brandInput.setColumns(10);
            GridBagConstraints gbc_brandInput = new GridBagConstraints();
            gbc_brandInput.gridwidth = 2;
            gbc_brandInput.insets = new Insets(0, 0, 5, 0);
            gbc_brandInput.fill = GridBagConstraints.HORIZONTAL;
            gbc_brandInput.gridx = 1;
            gbc_brandInput.gridy = 3;
            content.add(brandInput, gbc_brandInput);
            
            JButton cancelButton = new JButton("Cancel");
            GridBagConstraints gbc_cancelButton = new GridBagConstraints();
            gbc_cancelButton.insets = new Insets(0, 0, 0, 5);
            gbc_cancelButton.gridx = 1;
            gbc_cancelButton.gridy = 4;
            content.add(cancelButton, gbc_cancelButton);
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JButton button = (JButton)e.getSource();
                    returnCar=null;
                    SwingUtilities.getWindowAncestor(button).dispose();
                }
            });
            JButton okButton = new JButton("Ok");
            GridBagConstraints gbc_okButton = new GridBagConstraints();
            gbc_okButton.gridx = 2;
            gbc_okButton.gridy = 4;
            content.add(okButton, gbc_okButton);
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                	license=licenseInput.getText();
                	model=modelInput.getText();
                	brand=brandInput.getText();
                	color=colorInput.getText();
                    JButton button = (JButton)e.getSource();
                    if(license.equals("") || (brand.equals("")) || (color.equals("")) || (model.equals(""))) {
                    	new ErrorWindow("Please fill all the fields", parent);
                    }
                    else {
                    returnCar = new Car(model, color, license, brand);
                	SwingUtilities.getWindowAncestor(button).dispose();
                }
                    }
            });
            dialog.getRootPane().setDefaultButton(okButton);
            dialog.pack();
            dialog.setLocationRelativeTo(parent);
            dialog.setVisible(true);
            return returnCar;
        }
    }


//}