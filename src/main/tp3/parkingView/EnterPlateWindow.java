package main.tp3.parkingView;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;
import java.awt.Insets;

/**
 * window to allow the user to input a license plate number
 *
 */
public class EnterPlateWindow {

        private String license = "";
        private JTextField carLicense;
        
        /**
         * method for creating this window
         * @param parent
         * the JFrame that created this window (for centering purposes)
         * @return
         * a String with the license plate number entered by the user
         */
        public String showEnterPlateWindow(JFrame parent) {

            JPanel content = new JPanel();
            content.setBorder(new EmptyBorder(5, 5, 10, 10));

            JDialog dialog = new JDialog();
            dialog.setAlwaysOnTop(true);
            dialog.setResizable(false);
            dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
            dialog.setModal(true);
            dialog.setTitle("Parking lot");
            dialog.getContentPane().add(content);
            GridBagLayout gbl_content = new GridBagLayout();
            gbl_content.columnWidths = new int[]{113, 65, 45, 0};
            gbl_content.rowHeights = new int[]{14, 23, 0};
            gbl_content.columnWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
            gbl_content.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
            content.setLayout(gbl_content);
            
            JLabel lblEnterCarLicense = new JLabel("enter car license plate: ");
            GridBagConstraints gbc_lblEnterCarLicense = new GridBagConstraints();
            gbc_lblEnterCarLicense.insets = new Insets(0, 0, 5, 5);
            gbc_lblEnterCarLicense.gridx = 0;
            gbc_lblEnterCarLicense.gridy = 0;
            content.add(lblEnterCarLicense, gbc_lblEnterCarLicense);
            
            carLicense = new JTextField();
            GridBagConstraints gbc_carLicense = new GridBagConstraints();
            gbc_carLicense.fill = GridBagConstraints.HORIZONTAL;
            gbc_carLicense.insets = new Insets(0, 0, 0, 5);
            gbc_carLicense.gridx = 0;
            gbc_carLicense.gridy = 1;
            content.add(carLicense, gbc_carLicense);
            carLicense.setColumns(10);
            
            JButton cancelButton = new JButton("Cancel");
            GridBagConstraints gbc_cancelButton = new GridBagConstraints();
            gbc_cancelButton.insets = new Insets(0, 0, 0, 5);
            gbc_cancelButton.gridx = 1;
            gbc_cancelButton.gridy = 1;
            content.add(cancelButton, gbc_cancelButton);
            cancelButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JButton button = (JButton)e.getSource();
                    SwingUtilities.getWindowAncestor(button).dispose();
                }
            });
            JButton okButton = new JButton("Ok");
            GridBagConstraints gbc_okButton = new GridBagConstraints();
            gbc_okButton.gridx = 2;
            gbc_okButton.gridy = 1;
            content.add(okButton, gbc_okButton);
            okButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                	license=carLicense.getText();
                    JButton button = (JButton)e.getSource();
                    if(!license.equals("")) SwingUtilities.getWindowAncestor(button).dispose();
                    else new ErrorWindow("Please enter a license", parent);
                }
            });
            dialog.getRootPane().setDefaultButton(okButton);
            dialog.pack();
            dialog.setLocationRelativeTo(parent);
            dialog.setVisible(true);
            return license;
        }
    }


//}