package main.tp3.parkingView;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JProgressBar;

/**
 * main view class for the parking lot application
 *
 */
public class MainWindow extends JFrame {
	private JPanel contentPane;
	JButton exitButton;
	private JButton parkCarButton;
	private JButton removeCarButton;
	private JLabel lblCarsParked;
	private JLabel numberOfParkedCars;
	private JProgressBar occupationVisual;
	/**
	 * Create the frame.
	 */
	public MainWindow() {
		setTitle("Parking lot");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1074, 341);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10,10, 10, 10));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{67, 67, 67, 67, 67, 0};
		gbl_panel.rowHeights = new int[]{6, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel lblParkingOccupati = new JLabel("Parking occupation:");
		lblParkingOccupati.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblParkingOccupati = new GridBagConstraints();
		gbc_lblParkingOccupati.anchor = GridBagConstraints.WEST;
		gbc_lblParkingOccupati.insets = new Insets(0, 5, 5, 5);
		gbc_lblParkingOccupati.gridwidth = 2;
		gbc_lblParkingOccupati.gridx = 0;
		gbc_lblParkingOccupati.gridy = 0;
		panel.add(lblParkingOccupati, gbc_lblParkingOccupati);
		
		occupationVisual = new JProgressBar();
		occupationVisual.setStringPainted(true);
		GridBagConstraints gbc_occupationVisual = new GridBagConstraints();
		gbc_occupationVisual.fill = GridBagConstraints.HORIZONTAL;
		gbc_occupationVisual.gridwidth = 3;
		gbc_occupationVisual.insets = new Insets(0, 0, 5, 0);
		gbc_occupationVisual.gridx = 2;
		gbc_occupationVisual.gridy = 0;
		panel.add(occupationVisual, gbc_occupationVisual);
		
		lblCarsParked = new JLabel("Cars parked: ");
		GridBagConstraints gbc_lblCarsParked = new GridBagConstraints();
		gbc_lblCarsParked.anchor = GridBagConstraints.EAST;
		gbc_lblCarsParked.insets = new Insets(0, 0, 0, 5);
		gbc_lblCarsParked.gridx = 0;
		gbc_lblCarsParked.gridy = 1;
		panel.add(lblCarsParked, gbc_lblCarsParked);
		
		numberOfParkedCars = new JLabel("0");
		GridBagConstraints gbc_numberOfParkedCars = new GridBagConstraints();
		gbc_numberOfParkedCars.anchor = GridBagConstraints.WEST;
		gbc_numberOfParkedCars.insets = new Insets(0, 0, 0, 5);
		gbc_numberOfParkedCars.gridx = 1;
		gbc_numberOfParkedCars.gridy = 1;
		panel.add(numberOfParkedCars, gbc_numberOfParkedCars);
		
		removeCarButton = new JButton("remove car");
		GridBagConstraints gbc_removeCarButton = new GridBagConstraints();
		gbc_removeCarButton.insets = new Insets(0, 0, 0, 5);
		gbc_removeCarButton.gridx = 2;
		gbc_removeCarButton.gridy = 1;
		panel.add(removeCarButton, gbc_removeCarButton);
		
		parkCarButton = new JButton("Park car");
		GridBagConstraints gbc_parkCarButton = new GridBagConstraints();
		gbc_parkCarButton.insets = new Insets(0, 0, 0, 5);
		gbc_parkCarButton.gridx = 3;
		gbc_parkCarButton.gridy = 1;
		panel.add(parkCarButton, gbc_parkCarButton);
		
		exitButton = new JButton("Exit");
		GridBagConstraints gbc_exitButton = new GridBagConstraints();
		gbc_exitButton.gridx = 4;
		gbc_exitButton.gridy = 1;
		panel.add(exitButton, gbc_exitButton);
		pack();
		setLocationRelativeTo(null);
	}
	/**Method to add a listener to the exit button
	 * @param exitButtonAction
	 * an ActionListener to link to this button
	 */
	public void addExitListener(ActionListener exitButtonAction) {
		exitButton.addActionListener(exitButtonAction);
	}
	/**Method to add a listener to the park car button
	 * @param exitButtonAction
	 * an ActionListener to link to this button
	 */
	public void addParkCarListener(ActionListener parkCarButtonAction) {
		parkCarButton.addActionListener(parkCarButtonAction);
	}
	/**Method to add a listener to the remove car button
	 * @param exitButtonAction
	 * an ActionListener to link to this button
	 */
	public void addRemoveCarListener(ActionListener removeCarButtonAction) {
		removeCarButton.addActionListener(removeCarButtonAction);
	}

	/**Method to display the number of currently parked cars in the parking lot
	 * @param cars
	 * the number of cars currently parked
	 */
	public void updateNumberOfParkedCars(int cars) {
		numberOfParkedCars.setText(Integer.toString(cars));
		occupationVisual.setValue(cars*100/50);
	}
}
