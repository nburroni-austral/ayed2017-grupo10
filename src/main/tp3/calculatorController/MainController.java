package main.tp3.calculatorController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import main.tp3.calculatorModel.*;
import main.tp3.calculatorView.*;

/**
 * Controller class for the main menu window
 *
 */
public class MainController {
	private Model actions;
	private MainWindow display;
public MainController() {
	actions =new Model();
	display = new MainWindow();
	display.addExitListener(new ExitListener());
	display.addCalculateListener(new CalculateListener());
	display.setVisible(true);
}

/**
 * Listener class for exit button
 *
 */
class ExitListener implements ActionListener{
	@Override
	public void actionPerformed(ActionEvent buttonPressed) {
		System.exit(0);
	}
}

/**
 * Listener class for calcualte buttton 
 *
 */
class CalculateListener implements ActionListener{
	@Override
	public void actionPerformed(ActionEvent e) {
		String input = display.getUserInput();
		try {
		new ResultWindow(actions.calculate(input), display);
		} catch (Exception error) {
			new ErrorWindow(error.getMessage(), display);
		}
	}
	
}
}
