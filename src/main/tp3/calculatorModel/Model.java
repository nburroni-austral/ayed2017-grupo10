package main.tp3.calculatorModel;

public class Model {
public Model() {

}
/** 
 * does the mathemtical operation
 * @param input
 * a String containing the input from the user
 * @return
 * the result of the operation
 */
public double calculate(String input) throws IllegalArgumentException, ArithmeticException{
	double result = new Double(0);
	if(checkInput(input)){
		String[] sumParts = input.split("(?=-)|(?=\\+)");
		for (int i = 0; i < sumParts.length; i++) {
			if(sumParts[i].contains("*") || sumParts[i].contains("/")) {
				String[] multiplyParts = sumParts[i].split("(?=/)|(?=\\*)");
				double tempResult = new Double(multiplyParts[0]);
				for (int j = 1; j < multiplyParts.length; j++) {
					if (multiplyParts[j].contains("*")) {
						String multiply = multiplyParts[j].replace("*", "");
						try {
						tempResult *= new Double(multiply);
						}catch (Exception e) {
							throw new IllegalArgumentException("Consecutively repeating symbols is not allowed");
						}
					}else if (multiplyParts[j].contains("/")) {
						String divide = multiplyParts[j].replace("/", "");
						try {
							tempResult /= new Double(divide);
							}catch (Exception e) {
								throw new IllegalArgumentException("Consecutively repeating symbols is not allowed");
							}
					}
					
				} result+=tempResult;
			} else
				try {
					result += new Double(sumParts[i]);
				} catch (Exception e) {
					throw new IllegalArgumentException("Consecutively repeating symbols is not allowed");
				}
		}
		} if (Double.isFinite(result)) return result;
			throw new ArithmeticException("You can't divide by 0!");
		
}
/**
 * Private method for checking if a string contains any illegal characters that will make the calculator crash
 * @param input
 * String to be checked
 * @return
 * true if input is a valid String
 * @throws IllegalArgumentException
 * if input contains something that is illegal (mainly letters, parenthesis and symbols)
 */
private boolean checkInput (String input) throws IllegalArgumentException{
	
	if (input.matches(".*([\\+/*\\-])\\1+.*")) throw new IllegalArgumentException("Consecutively repeating symbols is not allowed");
	else if (input.contains(",")) throw new IllegalArgumentException("Please use '.' as decimal point");
	else if (input.matches(".*[()].*")) throw new IllegalArgumentException("Please don't use parenthesis");
	else if(!input.matches("^([\\+/*\\-]|[0-9])+$"))  throw new IllegalArgumentException("Please just use numbers and valid operands ('+', '-', '*', '/')");
	return true;
}
}
