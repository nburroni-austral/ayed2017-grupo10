package main.tp3.parking;

import struct.impl.Stack;

public class Parking {
	private static int fee = 5; //it is static so it can be raised without having to create all the parking lots again.
	private int earnings;
	private int maximumNumberOfCars;
	private Stack<Car> parkedCars;
	/**
	 * Creates an empty parking lot
	 * @param maximumNumberOfCars
	 * the maximum amount of cars to be parked in this parking lot
	 */
	public Parking(int maximumNumberOfCars) {
		this.maximumNumberOfCars = maximumNumberOfCars;
		parkedCars = new Stack<>();
		earnings=0;
	}
	/**
	 * method for parking a car in this parking lot.
	 * @param aCar
	 * The car to be parked.
	 * @return
	 * returns true if it was parked, false if the parking lot was full.
	 */
	public boolean parkCar (Car aCar) {
		if(parkedCars.size()<maximumNumberOfCars) {
			earnings+=fee;
			parkedCars.push(aCar);
			return true;
		}return false;
	}
	/**
	 * remove a parked car from the parking lot
	 * @param aPlate
	 * the license plate number of the car that is attempting to leave the premises
	 * @throws NullPointerException
	 * if no car was found in the parking lot with that license plate number
	 */
	public void removeCar (String aPlate) throws NullPointerException{
		Stack<Car> curve = new Stack<>();
    boolean rightCar = false;
    int numberOfCars = parkedCars.size();
    for (int i = 0; i < numberOfCars; i++){
        curve.push(parkedCars.peek());
        parkedCars.pop();
        if(curve.peek().getPlate().equals(aPlate)) {
        	curve.pop();
        	rightCar=true;
        	break;
        }
    }int removedCars=curve.size();
    for(int i=0; i<removedCars; i++) {
        parkedCars.push(curve.peek());
        curve.pop();
    }if (!rightCar) throw new NullPointerException("That car is not parked here.");
	}
	/**
	 * @return
	 * the total earnings of the parking lot today
	 */
	public int getTotalEarnings() {
		return earnings;
	}
	/**
	 * @return
	 * the current amount of cars parked in this parking lot.
	 */
	public int getNumberOfParkedCars() {
		return parkedCars.size();
	}
}
