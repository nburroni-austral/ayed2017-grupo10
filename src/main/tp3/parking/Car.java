package main.tp3.parking;

/**
 * a class representing a Car
 *
 */
public class Car {
private String model;
private	String color;
private String plate;
private String brand;
/**
 * Creates a car with a model, a color, a license plate and a brand
 * @param model
 * The model of the car being created.
 * @param color
 * The color of the car being created.
 * @param plate
 * The license plate of the car being created.
 * @param brand
 * The brand of the car being created.
 */
public Car(String model, String color, String plate, String brand) {
	this.model = model;
	this.color = color;
	this.plate = plate;
	this.brand = brand;
}
/**
 * @return
 * The model of the car.
 */
public String getModel() {
	return model;
}
/**
 * @return
 * The color of the car.
 */
public String getColor() {
	return color;
}
/**
 * @return
 * The license plate of the car.
 */
public String getPlate() {
	return plate;
}
/**
 * @return
 * The brand of the car.
 */
public String getBrand() {
	return brand;
}

}
