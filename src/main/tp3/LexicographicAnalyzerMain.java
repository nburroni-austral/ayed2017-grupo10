package main.tp3;

import java.io.IOException;
import java.util.Scanner;

import main.tp3.LexicographicAnalyzer.LexicographicAnalyzer;

public class
LexicographicAnalyzerMain {
    /**
     * Main method that initializes the Lexicographic analyzer and analyzes the file given
     *
     */
     public static void main(String args[]) throws IOException {
         System.out.println("Welcome to the lexicographic analyzer!");
         System.out.println("The file to be read is located in this directory, under the name 'file.txt'");
         System.out.println("Press enter key when you are ready");
         Scanner sc = new Scanner(System.in);
         sc.nextLine();
         sc.close();
         System.out.println("--------------------->");
    	 LexicographicAnalyzer analyzer = new LexicographicAnalyzer();
         analyzer.storeTextFile();

         int brackets = analyzer.analyzeBrackets();
         if(brackets == 0) {
             System.out.println("brackets are properly balanced");
         }
             else if(brackets < 0){
             System.out.println("You are missing " + Math.abs(brackets) + " opening brackets");

         }
         else {
             System.out.println("You have "+ Math.abs(brackets) + " extra opening brackets");
         }

         int parenthesis = analyzer.analyzeParenthesis();

         if(parenthesis == 0) {
             System.out.println("Parenthesis are properly balanced");
         }
         else if(parenthesis < 0){
             System.out.println("You are missing " + Math.abs(parenthesis)+ " opening parenthesis");

         }
         else {
             System.out.println("You have "+ Math.abs(parenthesis)+ " extra opening parenthesis");

         }

         int keys = analyzer.analyzeKeys();

         if(keys == 0) {
             System.out.println("Keys are properly balanced");
         }
         else if(analyzer.analyzeKeys() < 0){
             System.out.println("You are missing " + Math.abs(keys)+ " opening keys");

         }
         else {
             System.out.println("You have "+ Math.abs(keys) + " extra opening keys");
         }

     }


}
