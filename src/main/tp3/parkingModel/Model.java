package main.tp3.parkingModel;


import main.tp3.parking.Car;
import main.tp3.parking.Parking;


/**
 * model class for the parking lot application
 *
 */
public class Model {
private Parking parking;
public Model () {
	parking = new Parking(50);
}
/**
 * method for parking a car in this parking lot.
 * @param aCar
 * The car to be parked.
 * @return
 * returns true if it was parked, false if the parking lot was full.
 */
public boolean parkCar(Car aCar) {
	return parking.parkCar(aCar);
}
/**
 * remove a parked car from the parking lot
 * @param aPlate
 * the license plate number of the car that is attempting to leave the premises
 * @throws NullPointerException
 * if no car was found in the parking lot with that license plate number
 */
public void removeCar (String aPlate) throws NullPointerException{
		parking.removeCar(aPlate);
	
}
/**
 * @return
 * the total earnings of the parking lot today
 */
public int getEarnings() {
	return parking.getTotalEarnings();
}
/**
 * @return
 * the current amount of cars parked in this parking lot.
 */
public int getNumberOfParkedCars() {
	return parking.getNumberOfParkedCars();
}
}
