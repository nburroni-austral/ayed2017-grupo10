package main.tp3.calculatorView;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ErrorWindow extends JDialog {
	private final Action action = new OkButtonAction();/**
	 * Create the dialog.
	 */
	public ErrorWindow(String message, JFrame parent) {
	setAlwaysOnTop(true);
	setResizable(false);
		setTitle("Error");
		setModal(true);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 266, 113);
		getContentPane().setLayout(new BorderLayout());
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setBorder(new EmptyBorder(10, 10, 10, 10));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			GridBagLayout gbl_buttonPane = new GridBagLayout();
			gbl_buttonPane.columnWidths = new int[]{89, 88, 0};
			gbl_buttonPane.rowHeights = new int[]{14, 23, 0};
			gbl_buttonPane.columnWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			gbl_buttonPane.rowWeights = new double[]{0.0, 0.0, Double.MIN_VALUE};
			buttonPane.setLayout(gbl_buttonPane);
			{
				JLabel resultLabel = new JLabel(message);
				GridBagConstraints gbc_resultLabel = new GridBagConstraints();
				gbc_resultLabel.gridwidth = 2;
				gbc_resultLabel.insets = new Insets(0, 0, 5, 0);
				gbc_resultLabel.gridx = 0;
				gbc_resultLabel.gridy = 0;
				buttonPane.add(resultLabel, gbc_resultLabel);
			}
			{
				JButton okButton = new JButton("Ok");
				okButton.setAction(action);

				GridBagConstraints gbc_okButton = new GridBagConstraints();
				gbc_okButton.gridwidth = 2;
				gbc_okButton.anchor = GridBagConstraints.NORTH;
				gbc_okButton.gridx = 0;
				gbc_okButton.gridy = 1;
				buttonPane.add(okButton, gbc_okButton);
			}
		}

	pack();
	setLocationRelativeTo(parent);
	setVisible(true);
	}

	/**
	 * private class for setting the ok button action
	 *
	 */
	private class OkButtonAction extends AbstractAction {
		public OkButtonAction() {
			putValue(NAME, "Ok");
		}
		public void actionPerformed(ActionEvent e) {
			dispose();
		}
	}
}
