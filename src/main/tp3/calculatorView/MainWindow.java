package main.tp3.calculatorView;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.*;

/**
 * View class for the main menu window
 *
 */
public class MainWindow extends JFrame{
	private JPanel contentPane;
	private JButton calculateButton;
	private JButton exitButton;
	private JTextField userInputField;
	private JLabel lblIngreseUnCalculo;
	public MainWindow() {
		setResizable(false);
		setTitle("Calculator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 548, 411);
		setLocationRelativeTo(null);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 14, 23, 23, 23, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{0.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JLabel titleLabel = new JLabel("Calculator");
		titleLabel.setHorizontalAlignment(SwingConstants.CENTER);
		titleLabel.setFont(new Font("Times New Roman", Font.BOLD, 20));
		GridBagConstraints gbc_titleLabel = new GridBagConstraints();
		gbc_titleLabel.gridwidth = 2;
		gbc_titleLabel.insets = new Insets(0, 0, 5, 0);
		gbc_titleLabel.gridx = 0;
		gbc_titleLabel.gridy = 0;
		contentPane.add(titleLabel, gbc_titleLabel);
		
		lblIngreseUnCalculo = new JLabel("Insert calculation, no parenthesis allowed:");
		GridBagConstraints gbc_lblIngreseUnCalculo = new GridBagConstraints();
		gbc_lblIngreseUnCalculo.insets = new Insets(0, 0, 5, 5);
		gbc_lblIngreseUnCalculo.gridx = 0;
		gbc_lblIngreseUnCalculo.gridy = 1;
		contentPane.add(lblIngreseUnCalculo, gbc_lblIngreseUnCalculo);
		
		userInputField = new JTextField();
		userInputField.setDocument (new JTextFieldLimit(32));
		GridBagConstraints gbc_userInputField = new GridBagConstraints();
		gbc_userInputField.insets = new Insets(0, 0, 5, 5);
		gbc_userInputField.fill = GridBagConstraints.HORIZONTAL;
		gbc_userInputField.gridx = 0;
		gbc_userInputField.gridy = 2;
		contentPane.add(userInputField, gbc_userInputField);
		userInputField.setColumns(10);
		
		calculateButton = new JButton("Calculate");
		GridBagConstraints gbc_calculateButton = new GridBagConstraints();
		gbc_calculateButton.fill = MAXIMIZED_HORIZ;
		gbc_calculateButton.insets = new Insets(0, 10, 5, 0);
		gbc_calculateButton.gridx = 1;
		gbc_calculateButton.gridy = 2;
		contentPane.add(calculateButton, gbc_calculateButton);
		
		exitButton = new JButton("Exit");
		GridBagConstraints gbc_exitButton = new GridBagConstraints();
		gbc_exitButton.gridwidth = 2;
		gbc_exitButton.anchor = GridBagConstraints.NORTH;
		gbc_exitButton.fill = MAXIMIZED_HORIZ;
		gbc_exitButton.insets = new Insets(0,0,0,0);
		gbc_exitButton.gridx = 0;
		gbc_exitButton.gridy = 4;
		contentPane.add(exitButton, gbc_exitButton);
		getRootPane().setDefaultButton(calculateButton);
		pack();
	}
	/**Method to add a listener to the exit button
	 * @param exitButtonAction
	 * an ActionListener to link to this button
	 */
	public void addExitListener(ActionListener exitButtonAction) {
		exitButton.addActionListener(exitButtonAction);
	}
	/** Method to add a listener to the calculate button
	 * @param calculateButtonAction
	 * an ActionListener to link to this button
	 */
	public void addCalculateListener(ActionListener calculateButtonAction) {
		calculateButton.addActionListener(calculateButtonAction);
	}
	/** Method to get the text that the user entered
	 * @return
	 * A String representing the mathematical operation that the user wants to do 
	 */
	public String getUserInput() {
		return userInputField.getText();
	}
	
	/**
	 * private class to limit the amount of characters that the user can input
	 *
	 */
	private class JTextFieldLimit extends PlainDocument {
		  private int limit;

		  JTextFieldLimit(int limit) {
		   super();
		   this.limit = limit;
		   }

		  public void insertString( int offset, String  str, AttributeSet attr ) throws BadLocationException {
		    if (str == null) return;

		    if ((getLength() + str.length()) <= limit) {
		      super.insertString(offset, str, attr);
		    }
		  }
		}
}
