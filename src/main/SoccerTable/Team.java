package main.SoccerTable;

public class Team {
private String name;

public Team(String name) {
	this.name=name.trim();
}

public String getName() {
	return name;
}

@Override 
public boolean equals(Object anObject) {
    if ( this == anObject ) return true;
    if ( !(anObject instanceof Team) ) return false; //this also checks for null
    Team aTeam = (Team)anObject; //cast to Team is now safe
    return this.name.toLowerCase().equals(aTeam.getName().toLowerCase());
  }


}
