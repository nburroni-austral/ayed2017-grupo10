package main.SoccerTable;

import java.io.IOException;

public class Main {

	public static void main(String[] args) {
		Controller controller = new Controller();
		try {
			controller.readFromFile();
		} catch (IOException e) {
			System.out.println("error while reading from file");
			System.exit(1);
		}catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}
		controller.guessMatchesResults();
		controller.displayResults();
	}

}
