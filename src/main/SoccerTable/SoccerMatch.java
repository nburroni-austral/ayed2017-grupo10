package main.SoccerTable;

public class SoccerMatch {
private Team localTeam;
private Team visitorTeam;
private Team winnerTeam;
private SoccerLeague league;
private boolean finished;
public SoccerMatch(Team localTeam, Team visitorTeam, Team winnerTeam, SoccerLeague league) {
	if(!(winnerTeam.equals(visitorTeam) || winnerTeam.equals(localTeam))) throw new IllegalArgumentException("The winning team must have played the match");
	this.league=league;
	this.localTeam = localTeam;
	this.visitorTeam = visitorTeam;
	this.winnerTeam = winnerTeam;
	finished=true;
}
public SoccerMatch(Team localTeam, Team visitorTeam, SoccerLeague league) {
	this.league = league;
	this.localTeam = localTeam;
	this.visitorTeam = visitorTeam;
	finished=false;
	this.winnerTeam = null;
}
public Team localTeamWon() {
	if(!finished) {
	winnerTeam = localTeam;
	finished=true;
	}return winnerTeam;
	
}
public Team visitorTeamWon() {
	if(!finished) {
	winnerTeam=visitorTeam;
	finished=true;
	}
	return winnerTeam;
}
public void tiedMatch() {
	if(!finished) {
	winnerTeam=null;
	finished=true;
	}
}
public boolean isFinished() {
	return finished;
}
public void resetScore() {
	finished=false;
	winnerTeam = null;
}
public Team getWinnerTeam() throws NullPointerException{
	if(!finished) throw new NullPointerException("the match hasn't been played yet");
	return winnerTeam;
}
public SoccerLeague getLeague() {
	return league;
}
public Team getLocalTeam() {
	return localTeam;
}
public Team getVisitorTeam() {
	return visitorTeam;
}
}
