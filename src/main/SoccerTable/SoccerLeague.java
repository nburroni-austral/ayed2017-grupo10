package main.SoccerTable;

import java.util.ArrayList;
import java.util.HashMap;

public class SoccerLeague {
private HashMap<Team, Integer> teamScores;
private ArrayList<SoccerMatch> playedMatches;

public SoccerLeague() {
	teamScores = new HashMap<>();
	playedMatches = new ArrayList<>();
}
/**
 * adds a team to this league
 * @param team
 * the team to be added to the league
 */
public void addTeam(Team team) {
addTeam(team, 0);
}
public void addMatch(SoccerMatch aMatch) throws IllegalArgumentException {
	if(!aMatch.getLeague().equals(this))throw new IllegalArgumentException("This match didn't take place in this league");
	if(playedMatches.contains(aMatch))throw new IllegalArgumentException("This match is already added to this league");
	playedMatches.add(aMatch);
}
/**
 * adds a team to this league, but specifying the amount of points it has
 * @param team
 * the team to be added to the league
 * @param points
 * the points the team has scored in the matches it played in this league
 */
public void addTeam(Team team, int points) {
	if(!isAdded(team)) teamScores.put(team, points);
	else throw new IllegalArgumentException(team.getName()+" is already playing in this league");
}

/**
* get the points that a particular team has in this league
* @param team
* the team whose points need to be known
* @return
* the amount of points the team has in this league 
 * @throws IllegalArgumentException
 * when the team is not playing in this league
 */
public int getPoints(Team team) throws IllegalArgumentException {
	if(isAdded(team))return teamScores.get(searchTeam(team.getName()));
	else throw new IllegalArgumentException(team.getName()+" is not playing in this league");
}
/**
 * method to specify that a specific team won a match, it adds 3 points to that team
 * @param team
 * the team that won a match
 * @throws IllegalArgumentException
 * if the team is not playing in this league
 */
public void wonMatch(Team team) throws IllegalArgumentException {
	if(isAdded(team)) teamScores.replace(team, teamScores.get(searchTeam(team.getName()))+3);
	else throw new IllegalArgumentException(team.getName()+" is not playing in this league");
}
/**
 * specifies that two teams tied during a match, it adds 1 point to each team
 * @param firstTeam
 * the first team that tied
 * @param SecondTeam
 * the other team that tied in that match
 */
public void tiedMatch(Team firstTeam, Team secondTeam) {
	if(isAdded(firstTeam) && isAdded(secondTeam)) {
		teamScores.replace(firstTeam, teamScores.get(searchTeam(firstTeam.getName()))+1);
		teamScores.replace(secondTeam, teamScores.get(searchTeam(secondTeam.getName()))+1);
	}
	else if(!(isAdded(firstTeam)))throw new IllegalArgumentException(firstTeam.getName()+" is not playing in this league");
	else throw new IllegalArgumentException(secondTeam.getName()+" is not playing in this league");
}
public int getRemainingMatches(Team team) {
	if(!(isAdded(team)))throw new IllegalArgumentException(team.getName()+" is not playing in this league");
	int returnMatches = 0;
	for (SoccerMatch soccerMatch : playedMatches) {
		if((soccerMatch.getLocalTeam().equals(team) || soccerMatch.getVisitorTeam().equals(team)) && !(soccerMatch.isFinished())) returnMatches++;
	}return returnMatches;
}
/**
 * method to know how many teams are playing in this league
 * @return
 * the amount of teams playing in this league
 */
public int getNumberOfPlayingTeams() {
	return teamScores.size();
}
/**
 * private method to check if a certain team is playing in this league
 * @param team
 * the team to check whether is in this league
 * @return
 * whether the specific team is playing in this league or not
 */
private boolean isAdded(Team team) {
	boolean isAdded = false;
	for (Team aTeam : teamScores.keySet()) {
		if (aTeam.equals(team)) isAdded=true;
	}
	return isAdded;
}
private Team searchTeam(String teamName) {
	for (Team aTeam : teamScores.keySet()) {
		if (aTeam.getName().equals(teamName)) return aTeam;
	}
	return null;
}
public ArrayList<SoccerMatch> getPlayedMatches() {
	return playedMatches;
}
}
