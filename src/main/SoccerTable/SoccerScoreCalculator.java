package main.SoccerTable;

import java.util.*;

public class SoccerScoreCalculator {
	private boolean solutionFound;
	private HashMap<Team, Integer> tempTeamPoints;
private ArrayList<SoccerMatch> soccerMatches;
private SoccerLeague league;
public SoccerScoreCalculator(SoccerLeague league) {
	solutionFound=false;
	tempTeamPoints=new HashMap<>();
	this.league=league;
	soccerMatches=league.getPlayedMatches();
}
public boolean guessMatchesResults() {
	for (SoccerMatch soccerMatch : soccerMatches) {
		tempTeamPoints.put(soccerMatch.getLocalTeam(), 0);
		tempTeamPoints.put(soccerMatch.getVisitorTeam(), 0);
	}
		return guessMatchesResults(soccerMatches.get(0));
}
private boolean guessMatchesResults(SoccerMatch aMatch) {
	if(aMatch==null) {
		solutionFound=true;
		return true;
	}

	for (int i = 1; i < 4; i++) { 
		
		   boolean valid = isValidResult(aMatch, i);
		   if (!valid) 
			    continue;
			   Team winningTeam = null;
		   if(i==1) {
			   aMatch.localTeamWon();
			   winningTeam=aMatch.getLocalTeam();
			   updateTeamPoints(winningTeam, tempTeamPoints.get(winningTeam)+3);
		   }
		  if(i==3) {
			  aMatch.visitorTeamWon();
			  winningTeam=aMatch.getVisitorTeam();
			  updateTeamPoints(winningTeam, tempTeamPoints.get(winningTeam)+3);
		  }
		   //tempMatchResults.put(aMatch, i);
		   if(i==2) {
			   aMatch.tiedMatch();
			   updateTeamPoints(aMatch.getLocalTeam(), tempTeamPoints.get(aMatch.getLocalTeam())+1);
			   updateTeamPoints(aMatch.getVisitorTeam(), tempTeamPoints.get(aMatch.getVisitorTeam())+1);
		   }
		   boolean solved = guessMatchesResults(getNextMatch(aMatch));
		   if (solved) {
			   solutionFound=true;		   
			   return true;
		   }
		   else {
			   if(aMatch.getWinnerTeam()==null) {
				   updateTeamPoints(aMatch.getLocalTeam(), tempTeamPoints.get(aMatch.getLocalTeam())-1);
				   updateTeamPoints(aMatch.getVisitorTeam(), tempTeamPoints.get(aMatch.getVisitorTeam())-1);
			   }
			   else updateTeamPoints(aMatch.getWinnerTeam(), tempTeamPoints.get(aMatch.getWinnerTeam())-3);
		   		aMatch.resetScore();
		   		}
		  }
return false;
}
private void updateTeamPoints(Team team, int points) {
	tempTeamPoints.replace(team, points);
}
private boolean isValidResult(SoccerMatch match, int result) {
	if(tempTeamPoints.get(match.getLocalTeam())+league.getRemainingMatches(match.getLocalTeam())*3<league.getPoints(match.getLocalTeam()) ||
			tempTeamPoints.get(match.getVisitorTeam())+(league.getRemainingMatches(match.getVisitorTeam())*3)<league.getPoints(match.getVisitorTeam()))return false;
	switch (result) {
		case 1:
			return tempTeamPoints.get(match.getLocalTeam())+3<=league.getPoints(match.getLocalTeam());
		case 2:
			return tempTeamPoints.get(match.getLocalTeam())+1<=league.getPoints(match.getLocalTeam()) &&
					tempTeamPoints.get(match.getVisitorTeam())+1<=league.getPoints(match.getVisitorTeam());
		case 3:
			return tempTeamPoints.get(match.getVisitorTeam())+3<=league.getPoints(match.getVisitorTeam());
		default:
			return false;
	}
}
public SoccerMatch getNextMatch(SoccerMatch aSoccerMatch) throws IllegalArgumentException, IndexOutOfBoundsException {
if(!soccerMatches.contains(aSoccerMatch)) throw new IllegalArgumentException("this match never took place");
	try {
		return soccerMatches.get(soccerMatches.indexOf(aSoccerMatch)+1);
	} catch (IndexOutOfBoundsException e) {
		return null;
	}
}
public ArrayList<SoccerMatch> getMatchResults(){
return soccerMatches;	
}
public boolean hasSolution() {
		return solutionFound;
}
	
}
