package main.SoccerTable;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


public class Controller {
private ArrayList<SoccerLeague> leagues;
private ArrayList<SoccerScoreCalculator> calculators;
public Controller() {
	leagues = new ArrayList<>();
	calculators = new ArrayList<>();
}
public void readFromFile() throws IOException {
	ArrayList<String> readLines = new ArrayList<>();
	BufferedReader reader = new BufferedReader(new FileReader("src/main/SoccerTable/file.txt"));
    
	try {
    	reader.lines().forEach(o -> readLines.add(o));
    } finally {
        reader.close();
    }
	//long totalLeagues = readLines.stream().filter(o-> o.equals("")).count();
	int line = 0;
	if(!readLines.contains("-1")) throw new IllegalArgumentException("please append -1 to the end of the file");
	while(!readLines.get(line).equals("-1")) {
		if(readLines.get(line).equals("") || line==0) {
			if(readLines.get(line).equals("")) line++;
			if(readLines.get(line).equals("-1")) break;
			SoccerLeague newLeague = new SoccerLeague();
			SoccerScoreCalculator newCalculator = new SoccerScoreCalculator(newLeague);
			calculators.add(newCalculator);
			leagues.add(newLeague);
	String[] teamsAndMatches = readLines.get(line).split("\\s+");
	if(teamsAndMatches.length!=2) throw new IllegalArgumentException("check number of teams and number of matches");
	int numberOfTeams = line;
	int numberOfMatches = line;
	try {
		numberOfTeams += Integer.valueOf(teamsAndMatches[0]);
		numberOfMatches += Integer.valueOf(teamsAndMatches[1]);
	} catch (NumberFormatException e) {
		throw new IllegalArgumentException("invalid number of teams or matches");
	}
	int i = line;
	while (i<=numberOfTeams) {
		String[] teamAndPoints = readLines.get(i).split("\\s+");
		if(teamAndPoints.length!=2) throw new IllegalArgumentException("check all teams have proper number of points");
	int teamPoints = 0;
	try { teamPoints=Integer.valueOf(teamAndPoints[1]); } catch (NumberFormatException e) { throw new IllegalArgumentException("you are messing with me");}
	newLeague.addTeam(new Team(teamAndPoints[0]), teamPoints);
	i++;
	}
	for (int j=line; j<numberOfMatches; j++) {
		String[] teamsInMatch = readLines.get(i).split("\\s+");
		if(teamsInMatch.length!=2) throw new IllegalArgumentException("check number of teams in a match");
		newLeague.addMatch(new SoccerMatch(new Team(teamsInMatch[0]), new Team(teamsInMatch[1]), newLeague));
		i++;
	}
		if( line!=0) line--;
		}line++;
	}
}
public boolean guessMatchesResults() {
	boolean result = true;
	for (SoccerScoreCalculator calculator : calculators) {
		if(!calculator.guessMatchesResults()) result = false;
	}
	return result;
}
public void displayResults() {
	for (SoccerScoreCalculator calculator : calculators) {
		if(calculator.hasSolution()) {
		ArrayList<SoccerMatch> matches = calculator.getMatchResults();
		for (SoccerMatch soccerMatch : matches) {
			if(soccerMatch.getWinnerTeam()==null)System.out.print("X ");
			else if(soccerMatch.getWinnerTeam().equals(soccerMatch.getLocalTeam()))System.out.print("1 ");
			else System.out.print("2 ");
			}
		} else System.out.print("there are no solutions");
		System.out.println();
	}
	
}

}
