package main.TP7;

import struct.impl.BinaryTree;
import struct.impl.BinaryTreeApi;

public class Main {
public static void main(String[] args) {
	BinaryTree<Integer> testLeftSecondTree = new BinaryTree<>(45);
	BinaryTree<Integer> testLeftaSecondTree = new BinaryTree<>(32);
	BinaryTree<Integer> testLeftTree = new BinaryTree<>(15, testLeftaSecondTree, testLeftSecondTree);
	BinaryTree<Integer> testRightSecondTree = new BinaryTree<>(27);
	BinaryTree<Integer> testRightTree = new BinaryTree<>(20, null, testRightSecondTree);
	BinaryTree<Integer> testRootTree = new BinaryTree<>(88, testLeftTree, testRightTree);
	BinaryTreeApi<Integer> api = new BinaryTreeApi<>();
	BinaryTreeUtil<Integer> util = new BinaryTreeUtil<>();
	System.out.println("inorder");
	api.inorder(testRootTree);
	System.out.println("----------------------------------");
	System.out.println("postorder");
	api.postorder(testRootTree);
	System.out.println("----------------------------------");
	System.out.println("preorder");
	api.preorder(testRootTree);
	System.out.println("----------------------------------");
	System.out.println("weight");
	System.out.println(util.weight(testRootTree));
	System.out.println("----------------------------------");
	System.out.println("leaf amount");
	System.out.println(util.leafAmount(testRootTree));
	System.out.println("----------------------------------");
	System.out.println("height");
	System.out.println(util.getHeight(testRootTree));
}

}
