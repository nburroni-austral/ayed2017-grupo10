package main.TP7;


import struct.impl.BinaryTree;

public class BinaryTreeUtil<T> {



    public int weight(BinaryTree<T> tree){
        if(tree.isEmpty()){
            return 0;
        }
        else return 1 + weight(tree.getLeft()) + weight(tree.getRight());
    }

    public int leafAmount(BinaryTree<T> tree){
        if(tree.isEmpty()){
            return 0;
        }
        else if(tree.getLeft().isEmpty() && tree.getRight().isEmpty()){
            return 1;
        }
        else return leafAmount(tree.getLeft())  + leafAmount(tree.getRight());
    }
    public int getHeight(BinaryTree<T> tree){
        if(tree.isEmpty()){
            return -1;
        }
        if(getHeight(tree.getLeft()) > getHeight(tree.getRight())){
            return getHeight(tree.getLeft()) + 1;
        }
        else{
            return getHeight(tree.getRight()) + 1;
        }
    }
    //public int elementsOnLevel(BinaryTree tree, int level){

   // }
}
