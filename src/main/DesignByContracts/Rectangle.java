package main.DesignByContracts;

public class Rectangle {
    private int  sideA;
    private int /*@ non_null @*/ sideB;
    /*@
        invariant sideA != null

     @*/

    /*@
        requires sideA > 0;
        requires sideB > 0;

    @*/

    public Rectangle(int sideA, int sideB){
        this.sideA = sideA;
        this.sideB = sideB;
    }
    /*@
        ensures \result > 0;
        signals (ArithmeticException e);
         \result <= 0;

    @*/
    public /*@ pure @*/int perimeter(){
        return sideA*2 + sideB*2;
    }


}
