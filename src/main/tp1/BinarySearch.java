package main.tp1;

public class BinarySearch {
/**
 * method for searching a value in an array using binary search algorithm
 * @param data
 * the array in which to search for a value
 * @param key
 * the value to search for in the array
 * @return
 * returns an int representing the index of the array in which the key is found, 
 * returns -1 if the key was not found
 */
public static <T extends Comparable<T>> int search (T[] data, T key) {
	int low = 0;
    int high = data.length - 1;
        while(high >= low) {
                int middle = (low + high) / 2;
                int comparison = key.compareTo(data[middle]);
                if(comparison<0) {
                        low = middle + 1;
                    }
                else if(comparison>0) {
                        high = middle - 1;
                   }else return middle;
           }
       return -1;
}
}
