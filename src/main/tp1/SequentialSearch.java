package main.tp1;

public class SequentialSearch {
	public static <T> int search(T[] data, T key){
        for(int i= 0; i<data.length; i++){
            if(data[i] == key){
                return  i;
            }
        }
        return -1;
    }
}
