package main.tp1;

public class InsertionSort {
    public static <T extends Comparable<T>> T[] sort (T[] data){

        T temp;
        for (int i = 1; i < data.length; i++) {
            for(int j = i ; j > 0 ; j--){
                if(data[j].compareTo(data[j-1]) < 0){
                    temp = data[j];
                    data[j] = data[j-1];
                    data[j-1] = temp;
                }
            }
        }
        return data;
    }

}
