package main.tp1;

public class SelectionSort {
public static <T extends Comparable<T>> T[] sort (T[] data){
	T[] sortedArray = data;
	T smallest = data[0];
	for(int i=0; i<data.length;i++){
		smallest = data [i];
		for (int j=i; j<data.length; j++){
			if (smallest.compareTo(data[j])>0) {
				T tmpNumber = smallest;
				smallest = data[j];
				data[j]=tmpNumber;
			}
		}
		sortedArray[i] = smallest;
	}
	return sortedArray;
}
public static <T extends Comparable<T>> T[] recursiveSort (T[] data){
	return recursiveSort(data, 0);
}
private static <T extends Comparable<T>> T[] recursiveSort (T[] data, int index){
	if (index>=data.length) 	return data;
	T smallest = data[index];
		for (int j=index; j<data.length; j++){
			if (smallest.compareTo(data[j])>0) {
				T tmpNumber = smallest;
				smallest = data[j];
				data[j]=tmpNumber;
			}
		}
		data[index] = smallest;
		index++;
		return recursiveSort(data, index);
}

}
