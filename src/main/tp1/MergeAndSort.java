package main.tp1;

import java.util.*;

public class MergeAndSort {
	public static <T extends Comparable<T>> T[] merge (T[] firstData, T[] secondData){
		T[] firstSortedArray = BubbleSort.sort(firstData);
		T[] secondSortedArray = BubbleSort.sort(secondData);
		//List<T> result = new ArrayList<T>();
		T[] result = (T[]) new Comparable[firstData.length+secondData.length];

		int i = 0;
		int j = 0;
		int k = 0;
		while (i < result.length) {
			if (j >= firstSortedArray.length || k >= secondSortedArray.length) break;
			if (firstSortedArray[j].compareTo(secondSortedArray[k])<0) {
				result[i]=firstSortedArray[j];
			j++;
			}else {
				result[i]=secondSortedArray[k];
				k++;
			}
			i++;
		}
		if (j >= firstSortedArray.length) {
			while(k<secondSortedArray.length) {
				result[i]=secondSortedArray[k];
				k++;
				i++;
			}
		}  else {
			while(j<firstSortedArray.length) {
				result[i]= firstSortedArray[j];
				j++;
				i++;
			}
		}
		return result;
	}
}
