package main.tp1;

public class BubbleSort {
	public static <T extends Comparable<T>> T[] sort (T[] data){
		boolean changed = true;
		while (changed) {
			changed = false;
		for(int i=0; i<data.length-1;i++){
			if (data[i].compareTo(data[i+1])>0) {
					T tmpElement = data[i];
					data[i] = data[i+1];
					data[i+1]=tmpElement;
					changed = true;
		}
			}
		
		}
		return data;
	}
}
