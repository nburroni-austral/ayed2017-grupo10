package main.tp10;

import java.io.*;
import java.util.Scanner;

public class Ejercicio1 {

    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the location of the file you want to read: ");
        String location = scanner.nextLine();
        System.out.println("Enter the operation you want to do");
        System.out.println("c. if you want to count the amount of characters of the file");
        System.out.println("l. if you want to count the amount of lines of the file");
        char operation = scanner.nextLine().charAt(0);
        try {

            BufferedReader br = new BufferedReader(new FileReader(location));
            String thisLine;
            if(operation == 'c') {
                int result =0;
                while ((thisLine = br.readLine()) != null) {
                    thisLine.trim();
                    char[] charArray = thisLine.toCharArray();
                    result+= charArray.length;
                }
                System.out.println("The amount of character on this file is: " + result);
            }
            if(operation == 'l'){
                int result=0;
                while((thisLine = br.readLine()) != null){
                    result++;
                }
                System.out.println("The amount of lines on this file is: " + result);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
