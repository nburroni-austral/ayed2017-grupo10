package main.tp10;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Ejercicio4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the location of the file you want to read");
        String location = scanner.nextLine();
        System.out.println("Enter the location you want the new file to be");
        String location2 = scanner.nextLine();
        try {

            BufferedReader br = new BufferedReader(new FileReader(location));
            BufferedWriter bw = new BufferedWriter(new FileWriter(location2+"/moreThan30M.txt"));
            BufferedWriter bw2 = new BufferedWriter(new FileWriter(location2+"/lessThan30M.txt"));

            String thisLine;

            while ((thisLine = br.readLine()) != null) {
            	if(thisLine.equals("")) throw new IllegalArgumentException("Please do not insert blank lines");
                String[] splitted = thisLine.split("\\s+");
                Integer population;
                population = Integer.parseInt(splitted[1]);
                if(population > 30000000){
                    bw.write(thisLine);
                    bw.newLine();
                }
                else{
                    bw2.write(thisLine);
                    bw2.newLine();
                }
            }
            bw2.close();
            bw.close();






        } catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		}
        catch(Exception e) {
            System.out.println("please enter a valid file format.");
        }
    }
}
