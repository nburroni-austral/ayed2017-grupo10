package main.tp10;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class Ejercicio2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the location of the file you want to read");
        String location = scanner.nextLine();
        System.out.println("Enter the character you want to know the occurrences of");
        char characterToFind = scanner.nextLine().charAt(0);
        try {

            BufferedReader br = new BufferedReader(new FileReader(location));
            String thisLine;

            int result =0;
            while ((thisLine = br.readLine()) != null) {
                thisLine.trim();
                char[] charArray = thisLine.toCharArray();
                for(int i=0; i<charArray.length; i++){
                    if(charArray[i] ==characterToFind){
                        result++;
                    }
                }
            }
            System.out.println("The amount of times the character '"+characterToFind+ "' appears is: " + result);


        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
