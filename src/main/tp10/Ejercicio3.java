package main.tp10;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Ejercicio3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the location of the file you want to read");
        String location = scanner.nextLine();
        System.out.println("Enter the location you want the new file to be");
        String tempLocation = scanner.nextLine();
        System.out.println("Enter the name of the new file");
        String location2 = tempLocation + "/" + scanner.nextLine() + ".txt";
        System.out.println("Enter the operation you want to run: ");
        System.out.println("1. Change all letters to uppercase");
        System.out.println("2. Change all letters to lowercase");
        char operation = scanner.nextLine().charAt(0);

        try {

            BufferedReader br = new BufferedReader(new FileReader(location));
            BufferedWriter bw = new BufferedWriter(new FileWriter(location2));
            String thisLine;

            if(operation == '1'){
                while ((thisLine = br.readLine()) != null) {
                    String inUppercase = thisLine.toUpperCase();
                    bw.write(inUppercase);
                    bw.newLine();


                }
                bw.close();
            }
            if(operation == '2'){
                while ((thisLine = br.readLine()) != null) {
                    String inLowercase = thisLine.toLowerCase();
                    bw.write(inLowercase);
                    bw.newLine();


                }
                bw.close();
            }


        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
