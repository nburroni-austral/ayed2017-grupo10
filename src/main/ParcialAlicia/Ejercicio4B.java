package main.ParcialAlicia;

import struct.impl.BinaryTree;

public class Ejercicio4B <T> {


    private boolean isLeaf(BinaryTree<T> tree){
        if(tree.getLeft().isEmpty() && tree.getRight().isEmpty()){
            return true;
        }
        else return false;
    }

    public boolean certainNumberIsLeaf(BinaryTree<T> tree, T o){
        if(tree.isEmpty()){
            return false;
        }
        if(tree.getRoot().equals(o)){
            return isLeaf(tree);
        }

        else{
           return certainNumberIsLeaf(tree.getRight(), o) || certainNumberIsLeaf(tree.getLeft(), o);
        }
    }

    public int occurrences(BinaryTree<T> tree, T o ){
        if(tree.isEmpty()){
            return 0;
        }
        if(tree.getRoot().equals(o)){
            return 1 + occurrences(tree.getLeft(), o ) + occurrences(tree.getRight(), o);
        }
        else{
            return occurrences(tree.getLeft(), o ) + occurrences(tree.getRight(), o);
        }
    }

    public static void main(String[] args) {

        Ejercicio4B prueba = new Ejercicio4B();

        BinaryTree<Integer> tree1 = new BinaryTree<>(1);
        BinaryTree<Integer> tree2 = new BinaryTree<>(2);
        BinaryTree<Integer> tree3 = new BinaryTree<>(3);
        BinaryTree<Integer> tree4 = new BinaryTree<>(4, tree1, tree2);
        BinaryTree<Integer> tree5 = new BinaryTree<>(5, tree3, new BinaryTree<>(6));
        BinaryTree<Integer> completed = new BinaryTree<>(7, tree4, tree5);


        if(prueba.certainNumberIsLeaf(completed, 2)){
            System.out.println("your object is a leaf");
        }
    else System.out.println("Your object is NOT a leaf");
        int ocurrencias = prueba.occurrences(completed, 5);

        System.out.println("your number has " + ocurrencias + " occurrences");
    }
}
