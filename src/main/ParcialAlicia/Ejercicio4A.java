package main.ParcialAlicia;


import struct.impl.BinaryTree;

public class Ejercicio4A {

    public Ejercicio4A() {
    }

    public Integer integerTreeSum(BinaryTree<Integer> tree){
        if (tree.isEmpty()) return 0;
        return integerTreeSumAux(tree,0);
    }

    private int integerTreeSumAux(BinaryTree<Integer> tree, Integer aux){
        if (tree.getRight().isEmpty() && tree.getLeft().isEmpty()) return tree.getRoot();
        return tree.getRoot()
                + integerTreeSumAux(tree.getLeft(),aux + tree.getLeft().getRoot())
                + integerTreeSumAux(tree.getRight(),aux + tree.getRight().getRoot());
    }

    public void inOrder(BinaryTree<Integer> tree){
        if (tree.isEmpty()) return;
        inOrder(tree.getLeft());
        System.out.print(tree.getRoot() + "  ");
        inOrder(tree.getRight());
    }

    public static void main(String[] args) {

        Ejercicio4A ejercicio4A = new Ejercicio4A();

        BinaryTree<Integer> tree1 = new BinaryTree<>(1);
        BinaryTree<Integer> tree2 = new BinaryTree<>(2);
        BinaryTree<Integer> tree3 = new BinaryTree<>(3);
        BinaryTree<Integer> tree4 = new BinaryTree<>(4, tree1, tree2);
        BinaryTree<Integer> tree5 = new BinaryTree<>(5, tree3, new BinaryTree<>(0));
        BinaryTree<Integer> treeCompleted = new BinaryTree<>(7, tree4, tree5);

        System.out.print("The In-Order Traversal of the original tree: ");
        ejercicio4A.inOrder(treeCompleted);
        System.out.println("\n");

        BinaryTree<Integer> newTree1 = new BinaryTree<>(ejercicio4A.integerTreeSum(tree1));
        BinaryTree<Integer> newTree2 = new BinaryTree<>(ejercicio4A.integerTreeSum(tree2));
        BinaryTree<Integer> newTree3 = new BinaryTree<>(ejercicio4A.integerTreeSum(tree3));
        BinaryTree<Integer> newTree4 = new BinaryTree<>(ejercicio4A.integerTreeSum(tree4), newTree1, newTree2);
        BinaryTree<Integer> newTree5 = new BinaryTree<>(ejercicio4A.integerTreeSum(tree5), newTree3, new BinaryTree<>(0));
        BinaryTree<Integer> newTreeCompleted = new BinaryTree<>(ejercicio4A.integerTreeSum(treeCompleted), newTree4, newTree5);

        System.out.print("The In-Order Traversal of the new tree: ");
        ejercicio4A.inOrder(newTreeCompleted);
        System.out.println("\n");
    }
}
