package main.tp1;

import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

import static org.junit.Assert.assertArrayEquals;

public class InsertionSortTest {
    public Integer[] fillInt (int numberOfElements){
        ArrayList<Integer> unsortedInt = new ArrayList<>();
        for (int i=1; i<=numberOfElements; i++){
            unsortedInt.add(i);
        }
        Integer[] intArray = new Integer[numberOfElements];
        unsortedInt.toArray(intArray);
        Collections.shuffle(unsortedInt);
        return intArray;
    }
    Integer[] millionElements = fillInt(1000000);
    Integer[] hundredElements = fillInt(100);
    Integer[] thousandElements = fillInt(1000);
    Integer[] tenThousandElements = fillInt(10000);
    Integer[] hundredThousandElements = fillInt(100000);
    @Test
    public void insertionSortTest() {
        Integer[] array = {7, 3, 8, 9, 4, 1, 6, 5, 2};
        Integer[] sortedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        assertArrayEquals(sortedArray, BubbleSort.sort(array));
    }
    @Test
    public void insertionSortHundredElementsTest() {
        BubbleSort.sort(hundredElements);
    }
    @Test
    public void insertionSortThousandElementsTest() {
        BubbleSort.sort(thousandElements);
    }
    @Test
    public void insertionSortTenThousandElementsTest() {
        BubbleSort.sort(tenThousandElements);
    }
    @Test
    public void insertionSortHundredThousandElementsTest() {
        BubbleSort.sort(hundredThousandElements);
    }
    @Ignore //one million was pushing it too far
    @Test
    public void insertionSortMillionElementsTest() {

        BubbleSort.sort(millionElements);
    }
}
