package main.tp1;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Ignore;
import org.junit.Test;

public class SelectionSortTest {
	public Integer[] fillInt (int numberOfElements){
		ArrayList<Integer> unsortedInt = new ArrayList<>();
		for (int i=1; i<=numberOfElements; i++){
			unsortedInt.add(i);
		}
		Integer[] intArray = new Integer[numberOfElements];
		unsortedInt.toArray(intArray);
		Collections.shuffle(unsortedInt);
		return intArray;
	}
Integer[] millionElements = fillInt(1000000);
Integer[] hundredElements = fillInt(100);
Integer[] thousandElements = fillInt(1000);
Integer[] tenThousandElements = fillInt(10000);
Integer[] hundredThousandElements = fillInt(100000);
	@Test
	public void selectionSortTest() {
		Integer[] array = {7, 3, 8, 9, 4, 1, 6, 5, 2};
		Integer[] sortedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		assertArrayEquals(sortedArray, SelectionSort.sort(array));
	}
	@Test
	public void selectionSortHundredElementsTest() {
		SelectionSort.sort(hundredElements);
	}
	@Test
	public void selectionSortThousandElementsTest() {
		SelectionSort.sort(thousandElements);
	}
	@Test
	public void selectionSortTenThousandElementsTest() {
		SelectionSort.sort(tenThousandElements);
	}
	@Ignore //we know this works, no point in wasting time
	@Test
	public void selectionSortHundredThousandElementsTest() {
		SelectionSort.sort(hundredThousandElements);
	}
	@Ignore //one million was pushing it too far
	@Test
	public void selectionSortMillionElementsTest() {

		SelectionSort.sort(millionElements);
	}
	@Test
	public void recursiveSelectionSortTest() {
		Integer[] array = {7, 3, 8, 9, 4, 1, 6, 5, 2};
		Integer[] sortedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		assertArrayEquals(sortedArray, SelectionSort.recursiveSort(array));
	}
	@Test
	public void recursiveSelectionSortHundredElementsTest() {
		SelectionSort.recursiveSort(hundredElements);
	}
	@Test
	public void recursiveSelectionSortThousandElementsTest() {
		SelectionSort.recursiveSort(thousandElements);
	}
	@Test
	public void recursiveSelectionSortTenThousandElementsTest() {
		SelectionSort.recursiveSort(tenThousandElements);
	}
	@Ignore //recursive method crashes with this number
	@Test
	public void recursiveSelectionSortHundredThousandElementsTest() {
		SelectionSort.recursiveSort(hundredThousandElements);
	}
	@Ignore //one million is a very big number
	@Test
	public void recursiveSelectionSortMillionElementsTest() {

		SelectionSort.recursiveSort(millionElements);
	}
}
