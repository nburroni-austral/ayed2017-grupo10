package main.tp1;

import static org.junit.Assert.*;

import org.junit.Test;

public class SequentialSearchTests {

	@Test
	public void sequentialSearchTest() {
		String aString = "hola";
		String[] stringArray = {"hola", "chau", "hello"};
		assertEquals(0, SequentialSearch.search(stringArray, aString));
		}
	@Test
	public void sequentialSearchNotFoundTest() {
		String aString = "holaaa";
		String[] stringArray = {"hola", "chau", "hello"};
		assertEquals(-1, SequentialSearch.search(stringArray, aString));
		}

}
