package main.tp1;

import static org.junit.Assert.*;

import org.junit.Test;

public class BinarySearchTests {
	@Test
	public void BinarySearchTest() {
		String aString = "hola";
		String[] stringArray = {"hola", "chau", "hello"};
		assertEquals(0, BinarySearch.search(stringArray, aString));
		}
	@Test
	public void BinarySearchNotFoundTest() {
		String aString = "holaaaa";
		String[] stringArray = {"hola", "chau", "hello"};
		assertEquals(-1, BinarySearch.search(stringArray, aString));
		}
}
