package main.tp1;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collections;

import org.junit.Ignore;
import org.junit.Test;

public class BubbleSortTests {
	public Integer[] fillInt (int numberOfElements){
		ArrayList<Integer> unsortedInt = new ArrayList<>();
		for (int i=1; i<=numberOfElements; i++){
			unsortedInt.add(i);
		}
		Integer[] intArray = new Integer[numberOfElements];
		unsortedInt.toArray(intArray);
		Collections.shuffle(unsortedInt);
		return intArray;
	}
Integer[] millionElements = fillInt(1000000);
Integer[] hundredElements = fillInt(100);
Integer[] thousandElements = fillInt(1000);
Integer[] tenThousandElements = fillInt(10000);
Integer[] hundredThousandElements = fillInt(100000);
	@Test
	public void bubbleSortTest() {
		Integer[] array = {7, 3, 8, 9, 4, 1, 6, 5, 2};
		Integer[] sortedArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		assertArrayEquals(sortedArray, BubbleSort.sort(array));
	}
	@Test
	public void bubbleSortHundredElementsTest() {
		BubbleSort.sort(hundredElements);
	}
	@Test
	public void bubbleSortThousandElementsTest() {
		BubbleSort.sort(thousandElements);
	}
	@Test
	public void bubbleSortTenThousandElementsTest() {
		BubbleSort.sort(tenThousandElements);
	}
	@Test
	public void bubbleSortHundredThousandElementsTest() {
		BubbleSort.sort(hundredThousandElements);
	}
	@Ignore //one million was pushing it too far
	@Test
	public void bubbleSortMillionElementsTest() {

		BubbleSort.sort(millionElements);
	}

}
