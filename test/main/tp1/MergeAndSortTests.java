package main.tp1;

import static org.junit.Assert.*;

import org.junit.Test;

public class MergeAndSortTests {

	@Test
	public void test() {
		Integer[] firstArray = {1, 3, 5, 7, 9};
		Integer[] secondArray = {2, 4, 6, 8};
		Integer[] expected = {1, 2, 3, 4, 5, 6, 7, 8, 9};
		assertArrayEquals(expected, MergeAndSort.merge(firstArray, secondArray));
	}

}
