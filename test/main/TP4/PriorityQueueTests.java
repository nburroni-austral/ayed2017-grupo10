package main.TP4;

import org.junit.*;

import static junit.framework.TestCase.assertEquals;

public class PriorityQueueTests {
    @Test
    public void priorityQueueTests(){
        PriorityQueue<Integer> test = new PriorityQueue<>(5);
        test.enqueue(10, 4);
        test.enqueue(5,3);
        test.enqueue(7,1);
        test.enqueue(8,2);
        test.enqueue(9,5);
        Integer result = 7;
        Integer result2 = 8;
        Integer result3 = 5;

        assertEquals(test.dequeue() , result);
        assertEquals(test.dequeue() , result2);
        assertEquals(test.dequeue() , result3);




    }
}
