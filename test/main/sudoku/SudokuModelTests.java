package main.sudoku;

import static org.junit.Assert.*;

import org.junit.Test;

import main.sudoku.controller.MainController;
import main.sudoku.model.Model;

public class SudokuModelTests {

	@Test
	public void testSameBoxValidMove() {
		Model testModel = new Model(null);
		String[][] testBoard = new String[9][9];
		for (int y = 0; y < testBoard.length; y++) {
			for (int x = 0; x < testBoard.length; x++) {
				testBoard[x][y]="0";
			}
		}
		testBoard[0][0]="1";
		testBoard[0][2]="3";
		testModel.fillPuzzleGrid(testBoard);
		int[] position = {2, 2};
		assertTrue(testModel.isLegalMove(position, 2));
	}
	@Test
	public void testSameBoxInvalidMove() {
		Model testModel = new Model(null);
		String[][] testBoard = new String[9][9];
		for (int y = 0; y < testBoard.length; y++) {
			for (int x = 0; x < testBoard.length; x++) {
				testBoard[x][y]="0";
			}
		}
		testBoard[3][3]="1";
		testBoard[3][5]="3";
		testModel.fillPuzzleGrid(testBoard);
		int[] position = {5, 4};
		assertTrue(!testModel.isLegalMove(position, 3));
	}
	@Test
	public void testSameColumnInvalidMove() {
		Model testModel = new Model(null);
		String[][] testBoard = new String[9][9];
		for (int y = 0; y < testBoard.length; y++) {
			for (int x = 0; x < testBoard.length; x++) {
				testBoard[x][y]="0";
			}
		}
		testBoard[3][1]="1";
		testBoard[3][2]="3";
		testModel.fillPuzzleGrid(testBoard);
		int[] position = {3, 8};
		assertTrue(!testModel.isLegalMove(position, 3));
	}
	@Test
	public void testSameColumnValidMove() {
		Model testModel = new Model(null);
		String[][] testBoard = new String[9][9];
		for (int y = 0; y < testBoard.length; y++) {
			for (int x = 0; x < testBoard.length; x++) {
				testBoard[x][y]="0";
			}
		}
		testBoard[3][1]="1";
		testBoard[3][2]="3";
		testModel.fillPuzzleGrid(testBoard);
		int[] position = {3, 8};
		assertTrue(!testModel.isLegalMove(position, 3));
	}
	@Test
	public void testSameRowValidMove() {
		Model testModel = new Model(null);
		String[][] testBoard = new String[9][9];
		for (int y = 0; y < testBoard.length; y++) {
			for (int x = 0; x < testBoard.length; x++) {
				testBoard[x][y]="0";
			}
		}
		testBoard[3][1]="1";
		testBoard[8][1]="3";
		testModel.fillPuzzleGrid(testBoard);
		int[] position = {5, 1};
		assertTrue(!testModel.isLegalMove(position, 3));
	}
	@Test
	public void testSameRowInvalidMove() {
		Model testModel = new Model(null);
		String[][] testBoard = new String[9][9];
		for (int y = 0; y < testBoard.length; y++) {
			for (int x = 0; x < testBoard.length; x++) {
				testBoard[x][y]="0";
			}
		}
		testBoard[3][1]="1";
		testBoard[8][1]="3";
		testModel.fillPuzzleGrid(testBoard);
		int[] position = {5, 1};
		assertTrue(!testModel.isLegalMove(position, 3));
	}
}
