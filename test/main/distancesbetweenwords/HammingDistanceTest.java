package main.distancesbetweenwords;

import static org.junit.Assert.*;

import org.junit.Test;

public class HammingDistanceTest {

	@Test
	public void calculateHammingDistanceTest() {
			String testWord = "hello";
			String secondTestWord = "hwcco";
		assertEquals(3, HammingDistance.calculateHammingDistance(testWord, secondTestWord));
	}

/*	@Test
	public void calculateHammingDistanceTestFail() {
			String testWord = "hello";
			String secondTestWord = "hwcco";
		assertEquals(4, HammingDistance.calculateHammingDistance(testWord, secondTestWord));
	}*/

}
