package main.distancesbetweenwords;

import static org.junit.Assert.*;

import org.junit.*;

public class LevenshteinDistanceTest {
	@Test
	public void calculateLevenshteinDistanceTest() {
			String testWord = "hellaseouuu";
			String secondTestWord = "hello";
		assertEquals(6, LevenshteinDistance.calculateLevenshteinDistance(testWord, secondTestWord));
	}

}
