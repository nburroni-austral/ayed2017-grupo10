package main.SoccerTable;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SoccerLeagueTests {
	@Rule
	public ExpectedException exception = ExpectedException.none();
	@Test
	public void addTeamAndGetPointsTest() { //there was no direct way of checking that the team was correctly added, 
		Team boca = new Team("boca");		//but checking its points should work
		SoccerLeague league = new SoccerLeague();
		league.addTeam(boca);
		
	assertEquals(0, league.getPoints(boca));
	}
	@Test
	public void addTeamAlreadyAddedTest() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Boca is already playing in this league");
		Team boca = new Team("boca");
		Team secondBoca = new Team("Boca ");
		SoccerLeague league = new SoccerLeague();
		league.addTeam(boca);
		league.addTeam(secondBoca);
	}
	@Test
	public void wonMatchTest() {
		Team boca = new Team("boca");
		SoccerLeague league = new SoccerLeague();
		league.addTeam(boca);
		league.wonMatch(boca);
		assertEquals(3, league.getPoints(boca));
	}
	@Test
	public void wonMatchFailTest() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("boca is not playing in this league");
		Team boca = new Team("boca");
		SoccerLeague league = new SoccerLeague();
		league.wonMatch(boca);
	}
	@Test
	public void tiedMatchTest() {
		Team boca = new Team("boca");
		Team river = new Team("river");
		SoccerLeague league = new SoccerLeague();
		league.addTeam(boca);
		league.addTeam(river);
		league.tiedMatch(boca, river);
		assertEquals(1, league.getPoints(boca));
		assertEquals(1, league.getPoints(river));
	}
	@Test
	public void getNumberOfPlayingTeamsTest() {
		Team boca = new Team("boca");
		Team river = new Team("river");
		Team velez = new Team("velez");
		Team racing = new Team("racing");
		SoccerLeague league = new SoccerLeague();
		league.addTeam(boca);
		league.addTeam(river);
		league.addTeam(velez);
		league.addTeam(racing);
		assertEquals(4, league.getNumberOfPlayingTeams());
	}
}
