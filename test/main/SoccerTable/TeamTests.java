package main.SoccerTable;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * simple tests to check that the implementation of the equals method is working
 *
 */
public class TeamTests {
	@Test
	public void equalsToSameNameCapitalLettersTest() {
		Team boca = new Team("BocA");
		Team anotherBoca = new Team("boca");
		assertEquals(boca, anotherBoca);
	}
	@Test
	public void equalsToSameNameSpaceAtTheEndTest() {
		Team boca = new Team("boca");
		Team anotherBoca = new Team("  boca   ");
		assertEquals(boca, anotherBoca);
	}
	@Test
	public void equalsToSameNameTest() {
		Team boca = new Team("boca");
		Team anotherBoca = new Team("boca");
		assertEquals(boca, anotherBoca);
	}
	public void equalsFailsTest() {
		Team boca = new Team("boca");
		Team river = new Team("river");
		assertNotEquals(boca, river);
	}
	public void equalsToNullFailsTest() {
		Team boca = new Team("boca");
		assertNotEquals(boca, null);
	}
	@Test
	public void equalsToItselfTest() {
		Team boca = new Team("boca");
		assertEquals(boca, boca);
	}
}
