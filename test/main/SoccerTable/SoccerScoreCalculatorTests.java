package main.SoccerTable;
import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SoccerScoreCalculatorTests {
	@Rule
	public ExpectedException exception = ExpectedException.none();
	@Test
	public void testNextMatch() {
		SoccerLeague firstDivision = new SoccerLeague();
		Team boca = new Team("boca");
		Team river = new Team("river");
		Team velez = new Team("velez");
		firstDivision.addTeam(boca);
		firstDivision.addTeam(river);
		firstDivision.addTeam(velez);
		SoccerScoreCalculator soccerCalculator = new SoccerScoreCalculator(firstDivision);
		SoccerMatch firstMatch = new SoccerMatch(boca, river, boca, firstDivision);
		SoccerMatch secondMatch = new SoccerMatch(velez, river, river, firstDivision);
		firstDivision.addMatch(firstMatch);
		firstDivision.addMatch(secondMatch);
		SoccerMatch result = soccerCalculator.getNextMatch(firstMatch);
		assertEquals(secondMatch, result);
	}
	@Test
	public void testNextMatchException() {
		SoccerLeague firstDivision = new SoccerLeague();
		Team boca = new Team("boca");
		Team river = new Team("river");
		Team velez = new Team("velez");
		firstDivision.addTeam(boca);
		firstDivision.addTeam(river);
		firstDivision.addTeam(velez);
		SoccerScoreCalculator soccerCalculator = new SoccerScoreCalculator(firstDivision);
		SoccerMatch firstMatch = new SoccerMatch(boca, river, river, firstDivision);
		SoccerMatch secondMatch = new SoccerMatch(velez, river, velez, firstDivision);
		firstDivision.addMatch(firstMatch);
		firstDivision.addMatch(secondMatch);
		assertEquals(null, soccerCalculator.getNextMatch(secondMatch));
		
	}
}
