package main.SoccerTable;
import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class SoccerMatchTests {
	@Rule
	public ExpectedException exception = ExpectedException.none();
	@Test
	public void testInvalidMatchConstructor() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("The winning team must have played the match");
		SoccerLeague firstDivision = new SoccerLeague();
		Team boca = new Team("boca");
		Team river = new Team("river");
		Team velez = new Team("velez");
		firstDivision.addTeam(boca);
		firstDivision.addTeam(river);
		firstDivision.addTeam(velez);
		new SoccerMatch(river, boca, velez, firstDivision);
	}
	@Test
	public void testMatchResult() {
		SoccerLeague firstDivision = new SoccerLeague();
		Team boca = new Team("boca");
		Team river = new Team("river");
		firstDivision.addTeam(boca);
		firstDivision.addTeam(river);
		SoccerMatch match = new SoccerMatch(river, boca, river, firstDivision);
		assertEquals(river, match.getWinnerTeam());
	}
}
