package main.tp8;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import main.tp8.model.Database;
import main.tp8.model.Lamp;
import struct.impl.List;

public class DatabaseTests {
	@Rule
	public ExpectedException exception = ExpectedException.none();
	@Test
	public void searchTest() {
		Lamp aLamp = new Lamp("sedf", 100, "blue", 10);
		Lamp anotherLamp = new Lamp("s3edf", 100, "blue", 10);
		Lamp moreLamps = new Lamp("sdsedf", 100, "blue", 10);
		Database database = new Database();
		database.insertLamp(aLamp);
		database.insertLamp(anotherLamp);
		database.insertLamp(moreLamps);
		assertEquals(aLamp, database.searchLamp("sedf"));
	}
	@Test
	public void getLampsTest() {
		Lamp aLamp = new Lamp("sedf", 100, "blue", 10);
		Lamp anotherLamp = new Lamp("s3edf", 100, "blue", 10);
		Lamp moreLamps = new Lamp("sdsedf", 100, "blue", 10);
		Database database = new Database();
		database.insertLamp(aLamp);
		database.insertLamp(anotherLamp);
		database.insertLamp(moreLamps);
		List<Lamp>  lamps = database.getLamps();
		lamps.goTo(0);
		assertEquals(lamps.getActual(), anotherLamp);
		lamps.goTo(1);
		assertEquals(lamps.getActual(), moreLamps);
		lamps.goTo(2);
		assertEquals(lamps.getActual(), aLamp);
	}
	@Test
	public void addTwoTimesSameLampTest() {
		exception.expect(IllegalArgumentException.class);
		exception.expectMessage("Lamp already added");
		Lamp aLamp = new Lamp("sedf", 100, "blue", 10);
		Lamp anotherLamp = new Lamp("s3edf", 100, "blue", 10);
		Database database = new Database();
		database.insertLamp(aLamp);
		database.insertLamp(anotherLamp);
		database.insertLamp(new Lamp("sedf", 100, "blue", 10));
	}
	@Test
	public void modifyLampTest() {
		Lamp aLamp = new Lamp("sedf", 100, "blue", 10);
		Database database = new Database();
		database.insertLamp(aLamp);
		database.modifyLamp(new Lamp("sedf", 100, "blue", 20));
		assertEquals(20, database.searchLamp("sedf").getQuantity());
	}
	@Test
	public void deleteTest() {
		exception.expect(NullPointerException.class);
		exception.expectMessage("item not found");
		Lamp aLamp = new Lamp("sedf", 100, "blue", 10);
		Lamp anotherLamp = new Lamp("s3edf", 100, "blue", 10);
		Database database = new Database();
		database.insertLamp(aLamp);
		database.insertLamp(anotherLamp);
		database.deleteLamp("sedf");
		assertEquals(aLamp, database.searchLamp("sedf"));
	}
	@Test
	public void deleteFailTest() {
		exception.expect(NullPointerException.class);
		exception.expectMessage("item not found");
		Lamp aLamp = new Lamp("sedf", 100, "blue", 10);
		Lamp anotherLamp = new Lamp("s3edf", 100, "blue", 10);
		Lamp moreLamps = new Lamp("sdsedf", 100, "blue", 10);
		Database database = new Database();
		database.insertLamp(aLamp);
		database.insertLamp(anotherLamp);
		database.insertLamp(moreLamps);
		database.deleteLamp("ssdsedf");
	}

}
