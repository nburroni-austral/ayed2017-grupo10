package struct.impl;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class QueueTests {
	@Test
	public void enqueueTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		assertEquals(4, testQueue.length());
	}
	
	@Test
	public void dequeueTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		int actual = testQueue.dequeue();
		assertEquals(3, testQueue.length());
		assertEquals(1, actual);
	}
	@Test
	public void anotherDequeueTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		int result = testQueue.dequeue();
		assertEquals(1, result);
	}
	@Test
	public void emptyDequeueTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.dequeue();
		Integer result = testQueue.dequeue();
		assertEquals(null, result);
	}
	@Test
	public void nullDequeueTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.dequeue();
		Integer result = testQueue.dequeue();
		assertEquals(null, result);
	}
	@Test
	public void emptyTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		int actual = testQueue.dequeue();
		assertEquals(3, testQueue.length());
		assertEquals(1, actual);
		testQueue.empty();
		assertEquals(0, testQueue.length());
	}
	@Test
	public void isEmptyTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		testQueue.dequeue();
		testQueue.enqueue(22);
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.dequeue();
		assertEquals(0, testQueue.length());
		assertTrue(testQueue.isEmpty());
	}
	
	@Test
	public void growTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		int actual = testQueue.dequeue();
		assertEquals(3, testQueue.length());
		assertEquals(1, actual);
	}
	/**
	 * test for checking that the queue doesn't grow if the max size is reached several times
	 */
	@Test
	public void dontGrowTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.enqueue(22);
		testQueue.enqueue(22);
		assertEquals(4, testQueue.length());
		assertEquals(new Integer(145), testQueue.dequeue());
	}
	// we have to fix this

	@Test
	public void complexTest() {
		Queue<Integer> testQueue = new Queue<>();
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.dequeue();
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.enqueue(22);
		testQueue.enqueue(22);
		assertEquals(3, testQueue.length());
		int result = testQueue.dequeue();
		assertEquals(1340, result);
	}
	
}
