package struct.impl;

import static org.junit.Assert.*;

import org.junit.Test;

public class StackTest {

	@Test
	public void pushTest() {
		Stack<Integer> testStack = new Stack<>();
		testStack.push(1);
		testStack.push(123123);
		testStack.push(23);
		Integer a = 23;
		assertEquals(a, testStack.peek());
	}
	@Test
	public void popTest() {
		Stack<Integer> testStack = new Stack<>();
		testStack.push(1);
		testStack.push(123123);
		testStack.push(23);
		testStack.push(24324);
		testStack.pop();
		Integer a = 23;
		assertEquals(a, testStack.peek());
		testStack.pop();
		testStack.pop();
		testStack.pop();
		testStack.pop();
		testStack.pop();
		assertTrue(testStack.isEmpty());
	}
	@Test
	public void peekTest() {
		Stack<Integer> testStack = new Stack<>();
		testStack.push(23);
		Integer a = 23;
		assertEquals(a, testStack.peek());
	}
	@Test
	public void isEmptyTest() {
		Stack<Integer> testStack = new Stack<>();
		assertTrue(testStack.isEmpty());
		testStack.push(122);
		assertTrue(!testStack.isEmpty());
	}
	@Test
	public void emptyTest() {
		Stack<Integer> testStack = new Stack<>();
		testStack.push(1);
		testStack.push(123123);
		testStack.push(23);
		testStack.push(24324);
		testStack.empty();
		assertTrue(testStack.isEmpty());
	}
	@Test
	public void sizeTest() {
		Stack<Integer> testStack = new Stack<>();
		testStack.push(1);
		testStack.push(123123);
		testStack.push(23);
		testStack.push(24324);
		assertEquals(4, testStack.size());
	}
	@Test
	public void combinationTest() {
		Stack<Integer> testStack = new Stack<>();
		testStack.push(1);
		testStack.push(123123);
		testStack.push(23);
		testStack.push(24324);
		testStack.pop();
		testStack.empty();
		testStack.pop();
		testStack.push(2324324);
		Integer a = 2324324;
		assertEquals(a, testStack.peek());
		assertTrue(!testStack.isEmpty());
		assertEquals(1, testStack.size());
	}
}
