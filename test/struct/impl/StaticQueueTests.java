package struct.impl;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

public class StaticQueueTests {
	@Test
	public void enqueueTest() {
		StaticQueue<Integer> testQueue = new StaticQueue<>(5);
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		assertEquals(4, testQueue.length());
	}
	
	@Test
	public void dequeueTest() {
		StaticQueue<Integer> testQueue = new StaticQueue<>(5);
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		int actual = testQueue.dequeue();
		assertEquals(3, testQueue.length());
		assertEquals(1, actual);
	}
	@Test
	public void emptyTest() {
		StaticQueue<Integer> testQueue = new StaticQueue<>(5);
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		int actual = testQueue.dequeue();
		assertEquals(3, testQueue.length());
		assertEquals(1, actual);
		testQueue.empty();
		assertEquals(0, testQueue.length());
	}
	@Test
	public void isEmptyTest() {
		StaticQueue<Integer> testQueue = new StaticQueue<>(2);
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		testQueue.dequeue();
		testQueue.enqueue(22);
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.dequeue();
		assertEquals(0, testQueue.length());
		assertTrue(testQueue.isEmpty());
	}
	
	@Test
	public void growTest() {
		StaticQueue<Integer> testQueue = new StaticQueue<>(2);
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		int actual = testQueue.dequeue();
		assertEquals(3, testQueue.length());
		assertEquals(1, actual);
	}
	/**
	 * test for checking that the queue doesn't grow if the max size is reached several times
	 */
	@Test
	public void dontGrowTest() {
		StaticQueue<Integer> testQueue = new StaticQueue<>(5);
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.enqueue(22);
		testQueue.enqueue(22);
		assertEquals(5, testQueue.size());
		assertEquals(4, testQueue.length());
		assertEquals(new Integer(145), testQueue.dequeue());
	}
	// we have to fix this

	@Test
	public void complexTest() {
		StaticQueue<Integer> testQueue = new StaticQueue<>(3);
		testQueue.enqueue(1);
		testQueue.enqueue(10);
		testQueue.dequeue();
		testQueue.enqueue(145);
		testQueue.enqueue(1340);
		testQueue.dequeue();
		testQueue.dequeue();
		testQueue.enqueue(22);
		testQueue.enqueue(22);
		assertEquals(3, testQueue.length());
		int result = testQueue.dequeue();
		assertEquals(1340, result);
	}
	
}
