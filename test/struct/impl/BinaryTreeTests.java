package struct.impl;

import static org.junit.Assert.*;

import org.junit.Test;

public class BinaryTreeTests {
	@Test
	public void printTest() {
		BinaryTree<Integer> testLeftSecondTree = new BinaryTree<>(45);
		BinaryTree<Integer> testLeftaSecondTree = new BinaryTree<>(32);
		BinaryTree<Integer> testLeftTree = new BinaryTree<>(15, testLeftaSecondTree, testLeftSecondTree);
		BinaryTree<Integer> testRightSecondTree = new BinaryTree<>(27);
		BinaryTree<Integer> testRightTree = new BinaryTree<>(20, null, testRightSecondTree);
		BinaryTree<Integer> testRootTree = new BinaryTree<>(88, testLeftTree, testRightTree);
		BinaryTreeApi<Integer> api = new BinaryTreeApi<>();
		System.out.println("inorder");
		api.inorder(testRootTree);
		System.out.println("----------------------------------");
		System.out.println("postorder");
		api.postorder(testRootTree);
		System.out.println("----------------------------------");
		System.out.println("preorder");
		api.preorder(testRootTree);
		System.out.println("----------------------------------");
	}
	
}
